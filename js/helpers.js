// Function to Sort the Data by given Property
export const sortByProperty = (property, order = "asc") => {
  return function (a, b) {
    var sortStatus = 0,
      aProp = a[property],
      bProp = b[property];
    if ((order == "asc" && aProp < bProp) || (order == "desc" && aProp > bProp)) {
      sortStatus = -1;
    } else if (aProp > bProp) {
      sortStatus = 1;
    }
    return sortStatus;
  };
};

export const updateObjectInArray = ({ arr, obj, i, prop }) => {
  let clone = [...arr]  
  clone.forEach((item, j) => item[prop] == obj[prop] ? clone[j] = obj : null)
  return clone
}

export const numberWithCommas = (n) => {
  if (n) return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  else return n
}

export const getPathByRoleName = ({ role = [0], is_superuser = false }) => {
  
  if (is_superuser) return "/"

  role = role[0]
  switch(role.role) {
      case "User Management":
          return "/user-management"  

      case "Inventory":
          return "/inventory/dashboard"         

      case "Resource Allocation":
          return "/resource-allocation"         

      case "Result":
          return "/election-results/polling-scheme"          

      default:
          return "/register/login"
  }  

}




// DANIAL FUNCTIONS...
export const inventoryCalculation = (totalBooths, totalCandidates) => {
  return [
    totalBooths,
    parseInt(totalBooths) * Math.ceil(parseInt(totalCandidates) / 20),
    totalBooths,
    totalBooths,
  ];
};

const arrayToCsv = (data) => {
  return data
    .map(
      (row) =>
        row
          .map(String) // convert every value to String
          .map((v) => v.replaceAll('"', '""')) // escape double colons
          .map((v) => `"${v}"`) // quote it
          .join(",") // comma-separated
    )
    .join("\r\n"); // rows starting on new lines
};

const downloadBlob = (content, filename, contentType) => {
  // Create a blob
  var blob = new Blob([content], { type: contentType });
  var url = URL.createObjectURL(blob);

  // Create a link to download it
  var pom = document.createElement("a");
  pom.href = url;
  pom.setAttribute("download", filename);
  pom.click();
};

export const exportCsv = (data) => {
  let csv = arrayToCsv(data);
  console.log(csv, "csvv");
  downloadBlob(csv, "export.csv", "text/csv;charset=utf-8;");
};

export const printScreen = (window) => {
  window.print();
};
