export const SET_NAME = "SET_NAME"
export const GET_USERS = "GET_USERS"
export const SET_USER = "SET_USER"
export const GET_PERMISSIONS = "GET_PERMISSIONS"
export const SET_PERMISSIONS = "SET_PERMISSIONS"
export const GET_ROLES = "GET_ROLES"
export const SET_ROLES = "SET_ROLES"

export const SET_POLLING_STATION_DATA = "SET_POLLING_STATION_DATA"
export const SET_POLLING_STATION_LIST = "SET_POLLING_STATION_LIST"

export const GET_CONSTITUENCY_LIST = "GET_CONSTITUENCY_LIST"
export const SET_CONSTITUENCY_GEOJSON = "SET_CONSTITUENCY_GEOJSON"
export const SET_PARTIES = "SET_PARTIES"

// SITE
export const SET_ERRORS = "SET_ERRORS"
export const SET_LOADING = "SET_LOADING"
export const SET_LOADING2 = "SET_LOADING2"
export const SET_MSG = "SET_MSG"
export const SET_LATLNG = "SET_LATLNG"
export const SET_REDIRECT_PATH = "SET_REDIRECT_PATH"

// CANDIDATES
export const SET_CANDIDATES_LIST = "SET_CANDIDATES_LIST"