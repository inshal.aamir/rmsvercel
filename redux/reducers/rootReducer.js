import userReducer from "./user";
import { combineReducers } from "redux";
import pollingStationReducer from "./pollingstation";
import constituencyReducer from "./constituency";
import candidateReducer from "./candidate";
import siteReducer from "./site";
import authReducer from "./auth";


const rootReducer = combineReducers({
    user: userReducer,
    pollingStation: pollingStationReducer,
    constituency: constituencyReducer,
    candidate: candidateReducer,
    site: siteReducer,
    auth: authReducer
})

export default rootReducer