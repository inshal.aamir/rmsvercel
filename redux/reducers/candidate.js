import * as t from '../types'

const candidateReducer = (state = { list: [] }, action) => {
    switch (action.type) {
        case t.SET_CANDIDATES_LIST:
            return {
                ...state,
                list: action.payload
            }
        default:
            return state
    
    }
}

export default candidateReducer
