import * as t from '../types'

const pollingStationReducer = (state = { data: {}, list: [] }, action) => {
    switch (action.type) {
        case t.SET_POLLING_STATION_DATA:
            return { ...state, data: action.payload }

        case t.SET_POLLING_STATION_LIST:
            return { ...state, list: action.payload }
            
        default:
            return state
    
    }
}

export default pollingStationReducer


// ACTIONS
