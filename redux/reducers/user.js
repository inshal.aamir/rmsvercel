import * as t from '../types'

const userReducer = (state = { 
    data: null, 
    list: [], 
    permissions: [], 
    roles: [], 
    errors: null,
    loading: false
}, action) => {
    switch (action.type) {
        case t.GET_USERS:
            return { ...state, list: action.payload }

        case t.SET_USER:
            return { ...state, data: action.payload }

        case t.GET_ROLES:
            // state.data ? action.payload.forEach(role => {
            //     if (state.data.role.filter(userRole => userRole.role == role.role).length > 0) {
            //         role["checked"] = true
            //     }
            // }) : null
            return { ...state, roles: action.payload }

        case t.SET_ROLES:
            return { ...state, roles: action.payload }

        case t.SET_PERMISSIONS: 
            // console.log(action.payload, "action-payload")
            // return
            // state.data ? action.payload.forEach(permission => {
            //     if (state.data.user_permissions.filter(userPermission => userPermission.codename == permission.codename).length > 0) {
            //         permission["checked"] = true
            //     } 
            // }) : null
            return { ...state, permissions: action.payload }

        case t.SET_USER_ERRORS:
            return { ...state, errors: action.payload }

        case t.SET_NAME:
            return {
                ...state,
                name: action.payload
            }

        case 'CREATE_USER':
            // let response = createUser(action.payload)
            // return response

        default:
            return state
    
    }
}

export default userReducer


// ACTIONS
