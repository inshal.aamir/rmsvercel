import * as t from '../types'

const siteReducer = (state = {  
            errors: null,
            loading: false,
            loading2: false, // this loader is used for authentication
            msg: null,
            latlng: null,
            redirect: null,

        }, action) => {

        switch (action.type) {
            case t.SET_REDIRECT_PATH:
                return { ...state, redirect: action.payload }

            case t.SET_ERRORS:
                return { ...state, errors: action.payload }

            case t.SET_LOADING:
                return { ...state, loading: action.payload }

            case t.SET_LOADING2:
                return { ...state, loading2: action.payload }

            case t.SET_MSG:
                return { ...state, msg: action.payload }

            case t.SET_LATLNG:
                return { ...state, latlng: action.payload }

            default:
                return state

        }
}

export default siteReducer
 