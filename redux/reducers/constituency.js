import * as t from '../types'

const constituencyReducer = (state = { list: [], parties: [], geojson: null }, action) => {
    switch (action.type) {
        case t.GET_CONSTITUENCY_LIST:
            return { ...state, list: action.payload }
            
        case t.SET_CONSTITUENCY_GEOJSON:
            return { ...state, geojson: action.payload }

        case t.SET_PARTIES:
            return { ...state, parties: action.payload }
        default:
            return state
    
    }
}

export default constituencyReducer
