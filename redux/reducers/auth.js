import * as t from '../types'

const authReducer = (state = { list: [] }, action) => {
    switch (action.type) {
        case t.SET_CANDIDATES_LIST:
            return {
                ...state,
                list: action.payload
            }
        default:
            return state
    
    }
}

export default authReducer
