import * as t from "../types"
import axios from "axios"
import { getPathByRoleName } from "../../js/helpers"

export const getAccessTokenByRefreshToken = (refreshToken) => dispatch => {

    var config = {
        method: "post",
        url: `${process.env.BASE_URL}api/token/refresh/`,
        headers: {
            "Content-Type": "application/json",
        },
        data: { refresh: refreshToken },
    }
    axios(config)
    .then(async ({ data, status }) => {
        if (status == 200) localStorage.setItem("access_token", `ems ${data.access}`)        
        setTimeout(() => dispatch({ type: t.SET_LOADING2, payload: false }), 999)
    })
    .catch((error) => {
        console.log(error, error.response, "GET_ACCESS_TOKEN_REFRESH_TOKEN")
        setTimeout(() => dispatch({ type: t.SET_LOADING2, payload: false }), 999)

        if (error.response && error.response.status == 401) {
            localStorage.removeItem("access_user")
            localStorage.removeItem("refresh_token")
            localStorage.removeItem("access_token")
            dispatch({ type: t.SET_REDIRECT_PATH, payload: "/register/login" })
        }        
    })

}

export const authenticateUser = () => dispatch => {

    dispatch({ type: t.SET_LOADING2, payload: true })
    
    let accessUser = localStorage.getItem("access_user"),
        refreshToken = localStorage.getItem("refresh_token"),
        openRoutes = ["/register/login", "/register/otp", "/register/reset"],
        isOpenRoute = openRoutes.filter(r => r == window.location.pathname).length > 0

    if (accessUser && refreshToken) {
        if (isOpenRoute) {
            dispatch({ type: t.SET_REDIRECT_PATH, payload: getPathByRoleName(JSON.parse(accessUser)) })
            setTimeout(() => dispatch({ type: t.SET_LOADING2, payload: false }), 999)            
            
        } else {
            dispatch(getAccessTokenByRefreshToken(refreshToken))
        }

    }  else {
        localStorage.removeItem("access_user")
        localStorage.removeItem("refresh_token")
        localStorage.removeItem("access_token")
        
        if (!isOpenRoute) dispatch({ type: t.SET_REDIRECT_PATH, payload: "/register/login" })
        setTimeout(() => dispatch({ type: t.SET_LOADING2, payload: false }), 999)      
    }

}

export const getUserByAccessToken = (accessToken) => dispatch => {

    var config = {
        method: "get",
        url: `${process.env.BASE_URL}user/me/`,
        headers: { Authorization: accessToken },
      };
  
    axios(config)
    .then(({ status, data }) => {

        console.log(data, status, "GET_USER_BY_ACCESS_TOKEN_RESPONSE")

        if (status == 200) {
            localStorage.setItem("access_user", JSON.stringify(data))
            
            if (data && data.role) {
                dispatch({ type: t.SET_REDIRECT_PATH, payload: getPathByRoleName(data) })

            } else {         
                
                localStorage.removeItem("access_user")    
                localStorage.removeItem("access_token")
                localStorage.removeItem("refresh_token")  
                
                dispatch({ type: t.SET_MSG, payload: {
                    title: "No Roles Found",
                    description: "Sorry! We are unable to identify the user role",
                    onclose: { pathname: "/register/login" }
                }})
            }
        }

        dispatch({ type: t.SET_LOADING, payload: false })
        dispatch({ type: t.SET_ERRORS, payload: null }) 
    })
    .catch((error) => {
        alert("Please Check Logs")
        console.log(error.response, error);
        
        localStorage.removeItem("access_user")    
        localStorage.removeItem("access_token")
        localStorage.removeItem("refresh_token")  

        dispatch({ type: t.SET_LOADING, payload: false })
        dispatch({ type: t.SET_ERRORS, payload: null }) 

    })
}

export const sendOTP = (body) => dispatch => {

    dispatch({ type: t.SET_LOADING, payload: true })
    dispatch({ type: t.SET_ERRORS, payload: null }) 

    var config = {
      method: "post",
      url: `${process.env.BASE_URL}api/token/`,
      headers: {},
      data: body,
    };

    axios(config)
    .then(async ({ status, data }) => {
        console.log(status, data, "SEND_OTP_RESPONSE")
        
        if (status == 200) {
            await localStorage.setItem("access_token", `ems ${data.access}`)
            await localStorage.setItem("refresh_token", data.refresh)
            dispatch(getUserByAccessToken(localStorage.getItem("access_token")))            

        } else {
            dispatch({ type: t.SET_LOADING, payload: false })
            dispatch({ type: t.SET_ERRORS, payload: null }) 
        }

    })
    .catch(async (error) => {
        
        dispatch({ type: t.SET_LOADING, payload: false })        
        console.log(error.response, error, "SEND_OTP_ERROR");

        if (error.response) {
            if (error.response.status == 400) dispatch({ type: t.SET_ERRORS, payload: error.response.data })
            else {
                dispatch({ type: t.SET_ERRORS, payload: null })
                alert("Please Check Logs")
            }
        }
        // if (error.response.data.detail) {
        // alert(error.response.data.detail);
        // } else if (error.response.status == 403) {
        // await localStorage.setItem("username", username);
        // router.push({ pathname: "/register/reset" });
        // } else if (error.response.status == 401) {
        // alert(error.response.data.detail);
        // }
        // setLoading(false);
    })
}

export const login = (body) => dispatch => {

    dispatch({ type: t.SET_LOADING, payload: true })
    dispatch({ type: t.SET_ERRORS, payload: null }) 

    var config = {
        method: "post",
        url: `${process.env.BASE_URL}api/token/`,
        headers: {},
        data: body,
    }

    axios(config)
    .then(async ({ status, data }) => {

        if (status == 202) {            
            dispatch({ type: t.SET_MSG, payload: {
                title: "OTP Sent Successfully",
                description: data && data.message,
                onclose: { pathname: "/register/otp", query: body.username }
            }})
        }
        console.log(data, status, "AUTH_LOIGN_RESPONSE")
        // router.push({ pathname: "/register/otp", query: username })

        dispatch({ type: t.SET_LOADING, payload: false })
        dispatch({ type: t.SET_ERRORS, payload: null }) 

    })
    .catch(async (error) => {

        console.log(error.response, error, "AUTH_LOGIN_ERROR")

        dispatch({ type: t.SET_LOADING, payload: false })
        
        if (error && error.response) {
            if (error.response.status == 401) dispatch({ type: t.SET_ERRORS, payload: error.response.data })
            else {
                dispatch({ type: t.SET_ERRORS, payload: null })
                alert("Please Check Logs")
            }
            // else if (error.response.status == 403) router.push({ pathname: "/register/reset" })
        }

        // if (error && error.response && error.response.data && error.response.data.detail) {
        //     alert(error.response.data.detail);
        // } else if (error && error.response && error.response.status == 403) {
        //     await localStorage.setItem("username", username);
        //     router.push({ pathname: "/register/reset" });
        // } else if (error.response && error.response.status == 401) {
        //     alert(error.response.data.detail);
        // } else {
        //     alert("Please Check logs")
        //     console.log(error.response, error)
        // }
    })



    // var config = {
    //     method: 'get',
    //     url: `${process.env.BASE_URL}api/config/get/result/constituency/${constituency.toUpperCase()}`,
    //     headers: { 'Authorization': localStorage.getItem("access_token") },
    // }    
    // axios(config)
    // .then((response) => {       
    //     console.log(response, "candidate-list-response")
    //     if (response.status == 200) {
    //         dispatch({
    //             type: t.SET_CANDIDATES_LIST,
    //             payload: response.data.sort(sortByProperty("votes", "desc"))
    //         })
    //     } 
    // })
    // .catch((error) => {
    //     console.log(error.response, error)
    // }) 
}
