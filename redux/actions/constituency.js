import * as t from "../types";
import axios from "axios";
import { sortByProperty } from '../../js/helpers.js'

import NA_GEOJSON from '../../geojson/national_updated.json'

export const getConstituenciesByProvince = (province) => dispatch => {

    dispatch({ type: t.GET_CONSTITUENCY_LIST, payload: [] })

    const apiEndpoint = !province ? "api/config/get/result/national/constituency/" : `api/config/get/result/province/constituency/${province}`

    var config = {
        method: 'get',
        url: `${process.env.BASE_URL}${apiEndpoint}`,
        headers: { 'Authorization': localStorage.getItem("access_token") },
    }    
    axios(config)
    .then((response) => {       
        if (response.status == 200) {
            dispatch({
                type: t.GET_CONSTITUENCY_LIST,
                payload: response.data.sort(sortByProperty("name"))
            })
        } 
    })
    .catch((error) => {
        console.log(error.response)
    })    
}

export const getWinningPartiesByConstituency = (constituency) => dispatch => {

    // dispatch({ type: t.SET_PARTIES, payload: [] })

    var config = {
        method: 'get',
        url: `${process.env.BASE_URL}api/config/get/result/constituency/${constituency.toUpperCase()}/parties`,
        headers: { 'Authorization': localStorage.getItem("access_token") },
    }    
    axios(config)
    .then((response) => {       
        if (response.status == 200) {
            dispatch({
                type: t.SET_PARTIES,
                payload: response.data.sort(sortByProperty("votes", "desc"))
            })
        } 
    })
    .catch((error) => {
        console.log(error.response)
    }) 

}

export const getConstituencyGeoJSONByAssemblyAndConstituency = ({ assembly, constituency }) => dispatch => {
    console.log(assembly, constituency, "province an dconstit")
    let geojson = {}
    switch (assembly) {

        case "national":
            geojson = NA_GEOJSON.features.filter(c => c.properties["NA_Cons"].toUpperCase() == constituency.toUpperCase())
            console.log(geojson, "geojson...")
            dispatch({ type: t.SET_CONSTITUENCY_GEOJSON, payload: geojson })        
            break

        default:
            break
    }
}

export const setConstituencyGeoJSON = (geojson) => dispatch => {
    dispatch({
        type: t.SET_CONSTITUENCY_GEOJSON,
        payload: geojson
    })
}

export const setParties = (parties) => dispatch => {
    dispatch({
        type: t.SET_PARTIES,
        payload: parties
    })

}