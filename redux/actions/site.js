import * as t from "../types";
import axios from "axios"

export const setRedirectPath = (path) => dispatch => {
    dispatch({
        type: t.SET_REDIRECT_PATH,
        payload: path
    })
}

export const setErrors = (errors) => dispatch => {
    dispatch({
        type: t.SET_ERRORS,
        payload: errors
    })
}

export const setLoading = (state) => dispatch => {
    dispatch({
        type: t.SET_LOADING,
        payload: state
    })
}

export const setLoading2 = (state) => dispatch => {
    dispatch({
        type: t.SET_LOADING2,
        payload: state
    })
}

export const setMessage = (data) => dispatch => {
    dispatch({
        type: t.SET_MSG,
        payload: data
    })
}

// MAP-RELATED-DATA
export const setLatLng = (data) => dispatch => {
    dispatch({
        type: t.SET_LATLNG,
        payload: data
    })
}

