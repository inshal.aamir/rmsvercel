import * as t from "../types"
import axios from "axios"

export const setUser = (user) => dispatch => {
    dispatch({
        type: t.SET_USER,
        payload: user
    })
}

export const getUsers = ({ path }) => dispatch => {

    var config = {
        method: 'get',
        url: `${process.env.BASE_URL}user/get-users/?${path}`,
        headers: { 'Authorization': localStorage.getItem("access_token") }
    }    
    axios(config)
    .then((response) => {       
        if (response.status == 200) {
            dispatch({
                type: t.GET_USERS,
                payload: response.data
                // [
                //     { id: "001", name: "Akram Waqas", designation: ["ROLE 1", "ROLE 2"], email: "akram.waqas@gmail.com", cnic: "17301-5571261-9", created: "23-03-2022", updated: "24-03-2022" },
                //     { id: "002", name: "Ali Raza", designation: ["TRUE STORY", "ROLE 1", "ROLE 2"], email: "akram.waqas@gmail.com", cnic: "17301-5571261-9", created: "23-03-2022", updated: "24-03-2022" },
                //     { id: "003", name: "Rizwan Sajjid", designation: ["ROLE 1", "ROLE 2"], email: "akram.waqas@gmail.com", cnic: "17301-5571261-9", created: "23-03-2022", updated: "24-03-2022" },
                //     { id: "004", name: "Rizwan Sajjid", designation: ["ROLE 1", "ROLE 2"], email: "akram.waqas@gmail.com", cnic: "17301-5571261-9", created: "23-03-2022", updated: "24-03-2022" },
                //     { id: "005", name: "Shaheer Ahmad", designation: ["ROLE 1", "ROLE 2"], email: "akram.waqas@gmail.com", cnic: "17301-5571261-9", created: "23-03-2022", updated: "24-03-2022" },
                //     { id: "006", name: "Haris Ismail", designation: ["ROLE 1", "ROLE 2"], email: "akram.waqas@gmail.com", cnic: "17301-5571261-9", created: "23-03-2022", updated: "24-03-2022" },
                //     { id: "007", name: "Abid Iqbal", designation: ["ROLE 1", "ROLE 2"], email: "akram.waqas@gmail.com", cnic: "17301-5571261-9", created: "23-03-2022", updated: "24-03-2022" },
                //     { id: "008", name: "Zuhaib Imran", designation: ["ROLE 1", "ROLE 2"], email: "akram.waqas@gmail.com", cnic: "17301-5571261-9", created: "23-03-2022", updated: "24-03-2022" },
                // ]
            })
        } 
    })
    .catch((error) => {
        console.log(error.response, "GET_USERS_RESPONSE")
    })    
}

export const getRoles = (user = false) => dispatch => {
    dispatch({ type: t.GET_ROLES, payload: [] })
    var config = {
        method: 'get',
        url: `${process.env.BASE_URL}user/get/roles`,
        headers: { 'Authorization': localStorage.getItem("access_token") }
    }    
    axios(config)
    .then((response) => {       
        if (response.status == 200) {
            let { data } = response
            if (user && user.role) data.forEach(role => { if ( user.role.filter(urole => urole.role == role.role).length > 0 ) role["checked"] = true })
            
            dispatch({ type: t.GET_ROLES, payload: data })    
        } 
    })
    .catch((error) => {
        console.log(error.response, "GET_USER_ROLES_RESPONSE")
    })   
}

export const setRoles = (roles) => dispatch => {
    dispatch({ type: t.SET_ROLES, payload: roles })        
}

export const setPermissions = (permissions, user = false) => dispatch => {
    
    console.log(user, "my-user")
    if (user) permissions.forEach(p => { if (user.user_permissions.filter(upermission => upermission.codename == p.codename).length > 0 ) p["checked"] = true })        

    dispatch({ type: t.SET_PERMISSIONS, payload: permissions })

}

export const createUser = (data) => dispatch => {

    var config = {
        method: 'post',
        url: `${process.env.BASE_URL}user/add-user/`,
        headers: { 'Authorization': localStorage.getItem("access_token") },
        data
    }

    dispatch({ type: t.SET_LOADING, payload: true })
    axios(config)
    .then((response) => {       
        if (response.status == 200) {
            dispatch({ type: t.SET_ERRORS, payload: null }) 
            dispatch({ type: t.SET_MSG, payload: {
                title: "User Created",
                description: "Congratulations! New user is created successfully",
                onclose: { pathname: "/user-management" }
            }})
        }

        dispatch({ type: t.SET_LOADING, payload: false })
        console.log(response, "ADD_USER_RESPONSE")
    })
    .catch((error) => {
        dispatch({ type: t.SET_LOADING, payload: false }) 
        error && error.response && error.response.status == 403 ? 
            dispatch({ type: t.SET_ERRORS, payload: error.response.data }) 
        :   null

        console.log(error.response, error, "CREATE_USER_ERROR")
    })   
}




export const updateUser = (data) => dispatch => {
    var config = {
        method: 'put',
        url: `${process.env.BASE_URL}user/edit/`,
        headers: { 'Authorization': localStorage.getItem("access_token") },
        data
    }    
    dispatch({ type: t.SET_LOADING, payload: true })
    axios(config)
    .then((response) => {        
        if (response.status == 202) {
            console.log(response, "UPDATE_USER_RESPONSE")
            dispatch({ type: t.SET_ERRORS, payload: null }) 
            dispatch({ type: t.SET_LOADING, payload: false })
            dispatch({ type: t.SET_MSG, payload: {
                title: "User Updated",
                description: "Congratulations! The user profile has been updated successfully",
                onclose: "/user-management"
            }})
        } 
    })
    .catch((error) => {
        dispatch({ type: t.SET_LOADING, payload: false }) 
        error && error.response && error.response.status == 400 ? 
            dispatch({ type: t.SET_ERRORS, payload: error.response.data }) 
        :   null
        console.log(error, error.response, "UPDATE_USER_ERROR")
    })   
}

export const deleteUser = (username) => dispatch => {
    var config = {
        method: 'delete',
        url: `${process.env.BASE_URL}user/delete/${username}`,
        headers: { 'Authorization': localStorage.getItem("access_token") }
    }    
    axios(config)
    .then((response) => {        
        if (response.status == 200) {
            console.log(response, "UPDATE_USER_RESPONSE")
            location.href = "/user-management"
        } 
    })
    .catch((error) => { 
        console.log(error.response, "UPDATE_USER_ERROR")
    })   

}