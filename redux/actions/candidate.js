import * as t from "../types"
import axios from "axios"
import { sortByProperty } from "../../js/helpers"

export const getCandidatesByConstituency = (constituency) => dispatch => {
    var config = {
        method: 'get',
        url: `${process.env.BASE_URL}api/config/get/result/constituency/${constituency.toUpperCase()}`,
        headers: { 'Authorization': localStorage.getItem("access_token") },
    }    
    axios(config)
    .then((response) => {       
        console.log(response, "candidate-list-response")
        if (response.status == 200) {
            dispatch({
                type: t.SET_CANDIDATES_LIST,
                payload: response.data.sort(sortByProperty("votes", "desc"))
            })
        } 
    })
    .catch((error) => {
        console.log(error.response, error)
    }) 
}
