import * as t from "../types";
import axios from "axios";

export const setPollingStationData = (data) => dispatch => {
    dispatch({
        type: t.SET_POLLING_STATION_DATA,
        payload: data
    })
}

export const getPollingStationsByConstituency = (constituency) => dispatch => {

    dispatch({ type: t.SET_POLLING_STATION_LIST, payload: [] })

    var config = {
        method: 'get',
        url: `${process.env.BASE_URL}api/config/get/polling-station/constituency/${constituency}`,
        headers: { 'Authorization': localStorage.getItem("access_token") }
    }

    dispatch({ type: t.SET_LOADING, payload: true })
    axios(config)
    .then((response) => {       
        if (response.status == 200) {
            dispatch({ type: t.SET_ERRORS, payload: null }) 
            dispatch({ type: t.SET_POLLING_STATION_LIST, payload: response.data })
        }

        dispatch({ type: t.SET_LOADING, payload: false })
        console.log(response, "GET_POLLING_STATIONS_BY_CONSTITUENCY_RESPONSE")
    })
    .catch((error) => {
        dispatch({ type: t.SET_LOADING, payload: false }) 
        error && error.response && error.response.status == 403 ? 
            dispatch({ type: t.SET_ERRORS, payload: error.response.data }) 
        :   null
        console.log(error.response, error, "GET_POLLING_STATIONS_BY_CONSTITUENCY_ERROR")
    })  
}