import { useRouter } from 'next/router'
import React, { useEffect, useRef, useState } from 'react'
import { GeoJSON } from 'react-leaflet'
import { connect } from 'react-redux'
import { v4 as uuidv4 } from 'uuid';

import { getConstituenciesByProvince, setConstituencyGeoJSON } from '../../redux/actions/constituency'
import { setLatLng } from '../../redux/actions/site';

const AssemblyConstituenciesGeoJSON = ({ data, constituencies, getConstituenciesByProvince, setConstituencyGeoJSON, setLatLng, myprop }) => {

    const geoJsonLayer = useRef(null),
        router = useRouter(),
        { asPath } = router,        
        { assembly, view, province = assembly == "provincial" ? "balochistan" : null } = router.query,        
        [prop, setProp] = useState(myprop)

    useEffect(() => getConstituenciesByProvince(province), [assembly, province])
    useEffect(() => data ? geoJsonLayer.current.clearLayers().addData(data) : null, [data])

    function routeToSpecificConstituency(e) {
        console.log(e, "Individual Constituency (e)")
        
        setLatLng(e.latlng)
        setConstituencyGeoJSON([e.target.feature])

        asPath = asPath.includes("?") ? asPath.split("?")[0] : asPath
        router.push({
            pathname: asPath,
            query: {
                ...{
                    constituency: e.target.feature.properties[prop], view
                }, ...(province && { province })
            }            
        })            
    }

    const onEachFeature = (feature, layer) => {          
        let c = constituencies.filter(constituency => constituency.name == feature.properties[prop])[0] // c => Constituency
        let popupContent = 
        `
            <div class="bg-white py-3 px-0 border-radius__10">
                <p class="font-opensans-semibold border-bottom pb-2 px-3">${feature.properties[prop]} - ${feature.properties.District || "Undefined"} (${feature.properties.Province})</p>
                <div class="row mx-0 small font-opensans-regular mt-3">
                    <p class="col-lg-5 font-opensans-semibold pb-2">Winning Party:</p> 
                    <p class="col-lg-7 pb-2 pl-0">
                        <span class="py-0 px-3 mr-1 border-radius__2 box-shadow__dark2" style="background-color: ${c ? c.winner.party.color : ""}"></span>
                        <span class="text__wrap"> ${c ? c.winner.party.name : "Unknown"}</span> 
                    </p>
                    <p class="col-lg-5 font-opensans-semibold pb-2">Candidate Name:</p> <p class="col-lg-7 pb-2 pl-0"> 
                        ${c ? c.winner.name : "Unknown"}
                    </p>
                    <p class="col-lg-5 font-opensans-semibold pb-2">Votes Polled:</p> <p class="col-lg-7 pb-2 pl-0"> 
                        ${c && c.winner && c.winner.votes } Votes
                    </p>
                </div>
            </div>
        `
        layer.bindTooltip(popupContent, { permanent: false, direction: "left" })    


        // layer.bindTooltip(popupContent)
        // .openPopup();
        layer.on({ click: (e) => routeToSpecificConstituency(e) })

        // let constituency = constituencies.filter(constituency => constituency.name == feature.properties[prop])

        if (c) {

            layer.options.fillColor = c.winner.party.color

            // if (parties.filter(party => party.id == c.winner.party.id).length == 0) {
            //     parties.push({
            //         id: c.winner.party.id,
            //         name: c.winner.party.name,
            //         color: c.winner.party.color,
            //         logo: c.winner.party.logo
            //     })
            // }

        } else {
            layer.options.fillColor = "rgba(0, 0, 0, 0)"
        } 
    }

    return (
        <GeoJSON     
            style={() => ({
                fillOpacity: 0.75,
                stroke: 1,
                weight: 0.40,
                opacity: 1,
                color: '#363434'
            })}               
            data={data}
            ref={geoJsonLayer} 
            key={uuidv4()}
            onEachFeature={onEachFeature}
        />  
    )

}

const mapStateToProps = state => {
    return { 
        constituencies: state.constituency.list 
    }
}

const mapDispatchToProps = {
    getConstituenciesByProvince, setLatLng, setConstituencyGeoJSON
}
export default connect(mapStateToProps, mapDispatchToProps)(AssemblyConstituenciesGeoJSON)
