
import Navbar from './partials/navbar'
import Footer from './partials/footer'
import Sidebar from './partials/Sidebar'

export default function LayoutNone({ children }) {
  return (
    <>
        <div className="container-fluid px-0">
            <div className="d-flex">
                {children}
            </div>
    </div>
    </>
  )
}