
import Navbar from './partials/navbar'
import Footer from './partials/footer'
import Sidebar from './partials/Sidebar'

export default function Layout({ children }) {
  return (
    <>
      <div className="container-fluid px-0">
          <div className="d-flex">
              <div style={{ width: "220px"}}>
                  <Sidebar />
              </div>
              <div className="w-100 overflow__auto" style={{maxHeight: "100vh"}}>
                  <Navbar />
                  <main className="px-4 pt-3 mx-2">{children}</main>
              </div>
          </div>
          <Footer />
    </div>
    </>
  )
}