import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

export default function Sidebar() {
  const [menu, setMenu] = useState();

  useEffect(() => {
    const user_data = JSON.parse(localStorage.getItem("access_user"));

    if (user_data && user_data.is_superuser) {
      // if (false) {
      setMenu([
        {
          name: "Home",
          key: "",
          path: "/",
          iconMain: "/assets/img/icons/home.svg",
          iconActive: "/assets/img/icons/home_green.svg",
        },
        {
          name: "User Management",
          key: "user-management",
          path: "/user-management",
          iconMain: "/assets/img/icons/user-management.svg",
          iconActive: "/assets/img/icons/user-management_green.svg",
        },
        {
          name: "Inventory",
          key: "inventory",
          path: "/inventory/dashboard",
          iconMain: "/assets/img/icons/inventory.svg",
          iconActive: "/assets/img/icons/inventory_green.svg",
        },
        {
          name: "Resource Allocation",
          key: "resource-allocation",
          path: "/resource-allocation",
          iconMain: "/assets/img/icons/resource-allocation.svg",
          iconActive: "/assets/img/icons/resource-allocation_green.svg",
        },
        {
          name: "Election Results",
          key: "election-results",
          path: "/election-results/polling-scheme",
          iconMain: "/assets/img/icons/election-result.svg",
          iconActive: "/assets/img/icons/election-result_green.svg",
          nestedMenu: [
            {
              name: "Polling Scheme",
              key: "polling-scheme",
              path: "/election-results/polling-scheme",
            },
            {
              name: "Party Position",
              key: "party-position",
              path: "/election-results/party-position/national",
            },
          ],
        },
      ]);
    } else {
      let allowedFields = [];

      user_data && user_data.role.map((val, index) => {
        if ("User Management".toLowerCase().trim() === val.role.toLowerCase().trim()) {
          allowedFields.push({
            name: "User Management",
            key: "user-management",
            path: "/user-management",
            iconMain: "/assets/img/icons/user-management.svg",
            iconActive: "/assets/img/icons/user-management_green.svg",
          });
        } else if ("Inventory" === val.role) {
          allowedFields.push({
            name: "Inventory",
            key: "inventory",
            path: "/inventory/dashboard",
            iconMain: "/assets/img/icons/inventory.svg",
            iconActive: "/assets/img/icons/inventory_green.svg",
          });
        } else if ("Resource Allocation" === val.role) {
          allowedFields.push({
            name: "Resource Allocation",
            key: "resource-allocation",
            path: "/resource-allocation",
            iconMain: "/assets/img/icons/resource-allocation.svg",
            iconActive: "/assets/img/icons/resource-allocation_green.svg",
          });
        } else if ("Result" === val.role) {
          allowedFields.push({
            name: "Election Results",
            key: "election-results",
            path: "/election-results/polling-scheme",
            iconMain: "/assets/img/icons/election-result.svg",
            iconActive: "/assets/img/icons/election-result_green.svg",
            nestedMenu: [
              {
                name: "Polling Scheme",
                key: "polling-scheme",
                path: "/election-results/polling-scheme",
              },
              {
                name: "Party Position",
                key: "party-position",
                path: "/election-results/party-position/national",
              },
            ],
          });
        }
      });
      setMenu(allowedFields.reverse());
    }
  }, []);

  const router = useRouter(),
    pathname = router.pathname.split("/"),
    controller = pathname[1], // GET THE CONTROLLER
    action = pathname[2]; // GET THE ACTION

  return (
    <div className="h__100vh bg__dark w-100 animate__animated animate__slideInLeft animate__faster position-relative">
      <div className="text-center px-4 py-4">
        <img src="/assets/img/logo.svg" width="120px" />
      </div>
      <div className="mt-4">
        {menu &&
          menu.map((menuitem, key) => (
            <div key={`${key}sidebar`}>
              <Link href={menuitem.path}>
                <a className="btn w-100 bg-transparent py-3 px-0 d-flex align-items-center mb-2 position-relative">
                  <img
                    src={
                      controller == menuitem.key
                        ? menuitem.iconActive
                        : menuitem.iconMain
                    }
                    width="20px"
                    className="ml__20px mr-2"
                  />
                  <span
                    className={
                      "small ml-1 " +
                      (controller == menuitem.key
                        ? "font-opensans-bold text__green"
                        : "text-white")
                    }
                  >
                    {menuitem.name}
                  </span>
                  {controller == menuitem.key && !menuitem.nestedMenu ? (
                    <div
                      style={{ width: "7px" }}
                      className="h-100 border-radius__35 position-absolute bg__green__gradient"
                    ></div>
                  ) : null}
                </a>
              </Link>
              {menuitem.nestedMenu && controller == menuitem.key ? (
                <div className="">
                  {menuitem.nestedMenu.map((nestedMenuitem, k) => (
                    <Link href={nestedMenuitem.path} key={`${k}nestedsidebar`}>
                      <a
                        className={
                          "btn w-100 text-left py-1 px-0 mb-1 " +
                          (action == nestedMenuitem.key
                            ? "bg__green2"
                            : " bg-transparent")
                        }
                      >
                        <span className="small text-white ml-5 pl-2">
                          {nestedMenuitem.name}
                        </span>
                      </a>
                    </Link>
                  ))}
                </div>
              ) : null}
            </div>
          ))}
      </div>
    </div>
  );
}
