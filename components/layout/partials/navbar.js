import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import IconButton from "../../widgets/IconButton";
import NotificationButton from "../../widgets/NotificationButton";
import '../../../components/config.js'

export default function useNavbar() {
  const user_data = typeof window !== 'undefined' ? JSON.parse(localStorage.getItem("access_user")) : null,
    router = useRouter(),
    { route, query } = router,
    [navTitle, setNavTitle] = useState("Loading...")

    useEffect(() => {
      switch(route) {
        case "/":
          setNavTitle("Dashboard - General Elections 2022")
          break
        case "/user-management":
        case "/user-management/create-new-user":
          setNavTitle("User Management")
          break
        case "/inventory/dashboard":
          setNavTitle("Inventory")
          break
        case "/resource-allocation":
          setNavTitle("Resource Allocation")
          break
        case "/inventory/allocation":
          setNavTitle(`<span class="text__grey9">Inventory</span> &nbsp; / &nbsp; Inventory Allocation`)
          break
        case "/election-results/polling-scheme":
        case "/election-results/party-position/[assembly]":
          query.constituency ? setNavTitle(`<span class="text__grey9">Election Results</span> &nbsp; / &nbsp; ${query && query.constituency.toUpperCase()} Area Map`) : setNavTitle("Election Results")
          break
        case "/election-results/party-position/[assembly]/[constituency]":
          setNavTitle(`<span class="text__grey9">Election Results</span> &nbsp; / &nbsp; ${query && query.constituency.toUpperCase()}`)
          break
        case "/election-results/party-position/[assembly]/[constituency]/map":
          setNavTitle(`<span class="text__grey9">Election Results &nbsp; / &nbsp; ${query && query.constituency.toUpperCase()}</span> &nbsp; / &nbsp; Map View`)
          break
      }
    
    }, [route, query])

  // let name = asPath.split("/").slice(1);
  return (
    <div className="bg-white row mx-0 w-100 px-4 py-4">
      <div className="col-8 align-self-center">
        <h5 className="font-opensans-semibold mb-0" dangerouslySetInnerHTML={{ __html: navTitle }}></h5>
      </div>
      <div className="col-4 text-right">
        <div className="d-flex align-items-center justify-content-end">
          {/* <IconButton
                      icon="/assets/img/icons/search.svg"
                  />
                  <NotificationButton 
                      icon="/assets/img/icons/messages.svg"
                      count="1"
                  /> */}
          <NotificationButton icon="/assets/img/icons/bell.svg" count="2" />
          {/* <a href="#" className="text__dark mx-3 font-opensans-bold">
          Need Help?
        </a> */}
          <div
            style={{ width: "1px", height: "40px" }}
            className="bg__light2 mr-2 d-none"
          ></div>
          <div className="dropdown">
            <button
              type="button"
              className="btn bg-white btn__icon bg__light ml-3 btn-secondary dropdown-toggle"
              id="dropdownMenuButton"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <img src={user_data && user_data.profilePicture ? process.env.BASE_URL + user_data.profilePicture.slice(1) : "/assets/img/icons/user.svg"} width="22px" />
            </button>

            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <Link
                href={{
                  pathname: `/user-management`,
                  query: { username: user_data && user_data.username },
                }}
              >
                <a className="dropdown-item">Profile</a>
              </Link>
              <button 
                onClick={() => {
                  localStorage.removeItem("refresh_token")
                  router.push({ pathname: "/register/login"})
                }}
                className="dropdown-item">
                Log Out
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
