import Navbar from "./partials/navbar";
import Footer from "./partials/footer";
import Sidebar from "./partials/Sidebar";

export default function LayoutRegistration({ children }) {
  return (
    <div className="row mx-0 h__100vh">
      <div className="col-6 bg__light7">
        <div className="container">
          <div className="row mx-0 h__100vh">
            <div className="col-12 text-center align-self-center">
              <h3 className="text__dark4 font-opensans-bold">New with EVM?</h3>
              <p className="small text__dark4 mb-4">
                Simple and secure. Your provider for digital elections and
                voting with more than 5 million voters.
              </p>
              <img
                src="/assets/img/registeration_page.svg"
                width="375px"
                className="mt-3"
              />
              <div className="row mx-0  justify-content-center mt-3">
                <div className="col-5 pr-0">
                  <p className="mb-0 d-flex align-items-center small">
                    <span className="mr-2 mdi mdi-check text__green mdi-24px"></span>{" "}
                    Simple Election Management
                  </p>
                  <p className="mb-0 d-flex align-items-center small">
                    <span className="mr-2 mdi mdi-check text__green mdi-24px"></span>{" "}
                    Highest security and data protection
                  </p>
                  <p className="mb-0 d-flex align-items-center small">
                    <span className="mr-2  mdi mdi-check text__green mdi-24px"></span>{" "}
                    Made in Pakistan
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-6 bg-white">
        <div className="container">
          <div className="row mx-0 h__100vh">
            <div className="col-12 text-center align-self-center">
              <img src="/assets/img/logo2.svg" width="90px" />
              {children}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
