import React, { useState } from 'react'
import 'chart.js/auto';
import { Chart } from 'react-chartjs-2';
import { randomIntArrayInRange } from '../../../js/helpers';

export default function GenericLineChart() {
    const [data] = useState({
        labels: ["ICT", "Punjab", "Sindh", "Balochistan", "KPK", "FATA"],
        datasets: [
            {
                label: "Control Unit",
                data: [25600, 41200, 28000, 22210, 23120, 21200],
                backgroundColor: "#F4BE37"
            },
            {
                label: "Ballout Unit",
                data: [50123, 82400, 56120, 44123, 46240, 42400],
                backgroundColor: "#FF9F40"
            },
            {
                label: "Printer Box",
                data: [49123, 82400, 54120, 43123, 46240, 42300],
                backgroundColor: "#0D2535"
            },
            {
                label: "Battery Pack",
                data: [24600, 82400, 27500, 21210, 42520, 42400],
                backgroundColor: "#5388D8"
            },
        ], 
    }),
        doughnutChart = React.createRef()

    function toggleData(key) {
        const chart = doughnutChart.current
        console.log(chart.isDatasetVisible(key), key)
        if (chart.isDatasetVisible(key) === true) {
            console.log(key, "key")
            chart.hide(key)
        }
    }
    return (
        <div className={"px-4 row mx-0 py-4"}>
            <div className={"col-lg-12 px-4"}>
                <Chart ref={doughnutChart} type='bar' 
                    data={data}
                    options={{
                        elements: {
                            bar: {
                                borderWidth: 2,
                                borderColor: "rgba(0, 0, 0, 0)",
                                borderRadius: 5  
                            }
                        },
                        plugins: {
                        legend: {
                            display: true
                        },                     
                        }
                    }}
                />    
            </div>
            {/* <div className="row mx-0 mt-3 pt-3 col-lg-5 pl-5">
                {data && data.labels.map((label, key) => (
                    <div key={key} className={"d-flex mb-2 px-4 col-lg-12"}>
                        <span style={{ color: data.datasets[0].backgroundColor[key] }} className={"small mdi mdi-circle mr-2 mt-1"}></span>
                        <div>
                            <p className="font-opensans-semibold text__grey7 mb-2">{ label }</p>
                            <h6 className="text-dark font-opensans-semibold">{ data.datasets[0].data[key] }, 253</h6>
                        </div>
                    </div>
                ))}
            </div>         */}
        </div>
    )
}
