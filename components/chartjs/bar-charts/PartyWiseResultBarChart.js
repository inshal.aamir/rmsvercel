import React, { useState } from "react";
import "chart.js/auto";
import { Chart } from "react-chartjs-2";

export default function PartWiseResultBarChart({ axis }) {
  const [data] = useState({
    labels: [
      "Pakistan Muslim League (N)",
      "Pakistan Threek-E-Insaf",
      "Pakistan Peoples Party Parliamentarians",
      "Independent",
      "Muttahidda Qaumi Movement",
      "Jamiat Ulama-E-Islam (F)",
      "Awami National Party",
      "MUTAHIDA DEENI MAHAZ",
      "Pukhtoonkhwa Milli Awami Party",
      "National Peoples Party",
      "Pakistan Muslim League (Z)",
      "Bahawalpur National Awami Party",
      "Jamait Ulama-E-Islam Nazryati Pakistan",
    ],
    datasets: [
      {
        label: "Part Wise Result",
        data: [116, 64, 43, 13, 12, 6, 4, 4, 3, 2, 1, 1, 1],
        backgroundColor: [
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
          "#2FBA93",
        ],
        barPercentage: 0.6,
      },
    ],
  });

  return (
    <div className="px-5 pt-3 pb-5 h-100">
      <Chart
        type="bar"
        data={data}
        options={{
          scales: {
            [axis]: {
              grid: { display: false },
            },
          },
          maintainAspectRatio: false,
          indexAxis: axis,
          elements: {
            bar: {
              borderWidth: 2,
              borderColor: "rgba(0, 0, 0, 0)",
              borderRadius: 6,
            },
          },
          responsive: true,
          plugins: {
            legend: {
              position: "top",
              display: false,
            },
            datalabels: {
              color: "black",
              display: function (context) {
                return context.dataset.data[context.dataIndex] > 0;
              },
              font: {
                weight: "bold",
              },
              formatter: Math.round,
            },
            // title: {
            //     display: true,
            //     text: 'Chart.js Horizontal Bar Chart'
            // }
          },
        }}
      />
    </div>
  );
}
