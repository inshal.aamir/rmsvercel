import React, { useEffect, useState } from 'react'
import 'chart.js/auto';
import { Chart } from 'react-chartjs-2';
import { useRouter } from 'next/router';

import { connect } from 'react-redux'
import { getCandidatesByConstituency } from '../../../redux/actions/candidate'

const PartyPositionBarChart = ({ getCandidatesByConstituency, candidates }) => {
    const [data, setData] = useState(""),
        router = useRouter(),
        { constituency } = router.query

    useEffect(() => constituency ? getCandidatesByConstituency(constituency) : null, [constituency])
    useEffect(() => {
        if (candidates.length == 0) return

        let filtered = candidates.filter(c => c.votes > 0)
        setData({
            labels: filtered.map(candidate => { return `${candidate.name} (${candidate && candidate.party && candidate.party.name.match(/\b([A-Z])/g).join('')})` }),
            datasets: [
                {
                    label: "Votes Polled",
                    data: filtered.map(candidate => { return candidate.votes }),
                    backgroundColor: filtered.map(candidate => { return candidate && candidate.party && candidate.party.color }),
                    barPercentage: 0.8,
                },
            ],
        })

    }, [candidates])

    return (
        <div className="px-5 pt-3 pb-5 h-100" style={{ maxHeight: "100vh" }}>
            { data &&
                <Chart type='bar' 
                    data={data}
                    options={{
                        indexAxis: "y",
                        elements: {
                            bar: {
                                borderWidth: 4,
                                borderColor: "rgba(0, 0, 0, 0)",
                                borderRadius: 7  
                            }
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            legend: {
                                position: 'top',
                                display: false
                            },
                            // title: {
                            //     display: true,
                            //     text: 'Chart.js Horizontal Bar Chart'
                            // }
                        }
                    }}
                />                       
            }
        </div>
    )
}

const mapStateToProps = state => {
    return { candidates: state.candidate.list }
}

const mapDispatchToProps = {
    getCandidatesByConstituency
}

export default connect(mapStateToProps, mapDispatchToProps)(PartyPositionBarChart)
