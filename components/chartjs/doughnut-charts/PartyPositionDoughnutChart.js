import React, { useEffect, useState } from 'react'
import 'chart.js/auto';
import { numberWithCommas } from '../../../js/helpers';
import { Chart } from 'react-chartjs-2'
import { connect } from 'react-redux'

import { getWinningPartiesByConstituency } from '../../../redux/actions/constituency';
import { useRouter } from 'next/router';

const PartyPositionDoughnutChart = ({ getWinningPartiesByConstituency, winningParties }) => {
    const [data, setData] = useState(""),
        router = useRouter(),
        { constituency } = router.query

    useEffect(() => getWinningPartiesByConstituency(constituency), [constituency])

    useEffect(async () => {

        if (winningParties.length == 0) return

        setData({
            labels: winningParties.map(function (party) { return party.name }),
            datasets: [
                {
                    data: winningParties.map(function (party) { return party.votes }),
                    backgroundColor: winningParties.map(function (party) { return party.color })
                },
            ], 
        })

    }, [winningParties])

    return (
        <div className="bg-white border-radius__5 py-2">
            <p className="border-bottom px-3 pb-2 pt-1 font-opensans-semibold">Party Position</p>
            {
                data &&
                <div className="row mx-0 pl-4 pr-2 py-4 align-items-center">
                    <div className="col-lg-6 pl-0">
                        <Chart type='doughnut' 
                            data={data}
                            options={{
                                plugins: {
                                legend: {
                                    display: false
                                },                     
                                }
                            }}
                        />    
                    </div>
                    <div className="col-lg-6 pr-0 pl-4 overflow__auto" style={{ maxHeight: "350px" }}>
                        {data && data.labels.map((label, key) => (
                            <div title={label} key={key} className={"row mx-0 small mb-4"}>
                                <div className="col-lg-6 d-flex px-0">
                                    <span style={{ color: data.datasets[0].backgroundColor[key] }} className={"small mdi mdi-circle mr-2 mt-1"}></span>
                                    <p className="font-opensans-semibold text__grey7 mb-2">{ label.match(/\b([A-Z])/g).join('') }</p>
                                </div>
                                <p className="col-lg-6 mb-0 text-dark font-opensans-semibold">{ numberWithCommas(data.datasets[0].data[key]) }</p>
                            </div>
                        ))}
                    </div>        
                </div>
            }
        </div>
    )
}

const mapStateToProps = state => {
    return { 
        winningParties: state.constituency.parties
    }
}

const mapDispatchToProps = {
    getWinningPartiesByConstituency
}
 
export default connect(mapStateToProps, mapDispatchToProps)(PartyPositionDoughnutChart)