import React, { useState } from "react";
import "chart.js/auto";
import { Chart } from "react-chartjs-2";

export default function GenericDoughnutChart() {
  const [data] = useState({
      labels: ["Control Units", "Ballot Units", "Printer Box", "Battery Pack"],
      datasets: [
        {
          data: [502004, 1006300, 1020400, 700120],
          backgroundColor: ["#F4BE37", "#FF9F40", "#0D2535", "#5388D8"],
        },
      ],
    }),
    doughnutChart = React.createRef();

  function toggleData(key) {
    const chart = doughnutChart.current;
    console.log(chart.isDatasetVisible(key), key);
    if (chart.isDatasetVisible(key) === true) {
      console.log(key, "key");
      chart.hide(key);
    }
  }
  return (
    <div className={"px-4 row mx-0 py-4"}>
      <div className="col-lg-1"></div>
      <div className={"col-lg-6 p-4"}>
        <Chart
          ref={doughnutChart}
          type="doughnut"
          data={data}
          options={{
            plugins: {
              legend: {
                display: false,
              },
            },
          }}
        />
      </div>
      <div className="row mx-0 mt-3 pt-3 col-lg-5 pl-5">
        {data &&
          data.labels.map((label, key) => (
            <div key={key} className={"d-flex mb-2 px-4 col-lg-12"}>
              <span
                style={{ color: data.datasets[0].backgroundColor[key] }}
                className={"small mdi mdi-circle mr-2 mt-1"}
              ></span>
              <div>
                <p className="font-opensans-semibold text__grey7 mb-2">
                  {label}
                </p>
                <h6 className="text-dark font-opensans-semibold">
                  {data.datasets[0].data[key]}
                </h6>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
}
