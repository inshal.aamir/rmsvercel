import { useRouter } from 'next/router'
import React, { useState } from 'react'

export default function ProvincesSelectOptions() {
    const [provinces, setProvinces] = useState([
        { name: "Balochistan", key: "balochistan" },
        { name: "KPK", key: "kpk" },
        { name: "Sindh", key: "sindh" },
        { name: "Punjab", key: "punjab" },
    ]),
        router = useRouter(),
        { asPath } = router,        
        { province = "", view } = router.query

    function switchProvince(e) {
        asPath = asPath.includes("?view") ? asPath.split("?view")[0] : asPath

        router.push({
            pathname: asPath,
            query: {
                view: view || "table",
                province: e.target.value
            }
        })
    }
    
    return (          
        <select 
            onChange={(e) => switchProvince(e) }
            className="custom-select border__light3 border-radius__5 px-3" style={{ maxWidth: "200px" }}>
                {provinces && provinces.map(p => (
                    <option 
                        selected={p.key == province}
                        key={p.key} value={p.key}>{ p.name }</option>
                ))}
        </select>
    )
}
