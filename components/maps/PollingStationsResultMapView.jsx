import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'

import { MapContainer, GeoJSON, Tooltip, TileLayer, Popup, useMapEvents, Marker, CircleMarker, Circle, useMap } from "react-leaflet"
import L from 'leaflet'

import { connect } from 'react-redux'
import { getPollingStationsByConstituency } from '../../redux/actions/pollingstation'
import ChangeView from '../leaflet/ChangeView'
import ProvincesSelectOptions from '../select/ProvincesSelectOptions'
import AssemblyConstituenciesGeoJSON from '../leaflet/AssemblyConstituenciesGeoJSON'

const PollingStationsResultMapView = ({ latlng, getPollingStationsByConstituency, pollingStations, constituencyGeoJSON }) => {

    const router = useRouter(),
        { assembly, constituency, province = assembly == "provincial" ? "balochistan" : null } = router.query,
        [center, setCenter] = useState({ lat: 0, lng: 0 })    
    
    useEffect(() => pollingStations.length > 0 ? document.getElementById("pollingstationmap").focus() : null, [pollingStations])
    useEffect(() => latlng ? setCenter(latlng) : null, [latlng])
    useEffect(() => constituency ? getPollingStationsByConstituency(constituency) : null, [constituency])
    

    return (
        <>
            <div className="d-flex w-100 justify-content-between pt-4 align-items-center">
                <div className="d-flex w-100 align-items-center">
                    <h5 className="font-opensans-semibold pl-4 mr-4 mb-0 text-capitalize"> { constituency } Area Map</h5>
                    { assembly == "provincial" ? <ProvincesSelectOptions /> : null }            
                </div>
            </div>
            {
                pollingStations && pollingStations.length > 0 ?
                    <div className="pt-3">
                        <MapContainer id="pollingstationmap" maxZoom={20} style={{ height: "80vh" }} scrollWheelZoom={true}>   
                            <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" maxZoom={18} />
                            <ChangeView center={center} zoom={8} />
                            <GeoJSON data={constituencyGeoJSON} />                          

                            {pollingStations &&  pollingStations.map((pollingStation, i) => {
                                return (
                                    <CircleMarker
                                        key={i} 
                                        eventHandlers={{
                                            click: (e) => {
                                                $(".polling__station__item").removeClass("bg__green3 font-weight-bold")
                                                let elem = $(`#pollingstation-${pollingStation.pollingStationID}`),
                                                    container = $("#pollingstations-container")

                                                    
                                                container.animate({ scrollTop: 0 }, 499)
                                                setTimeout(() => $("#pollingstations-container").animate({ scrollTop: elem.offset().top - 200 }, 999), 499)                                    
                                                setTimeout(() => elem.addClass("bg__green3 font-weight-bold"), 499)
                                                
                                            }}} 
                                        fillColor={pollingStation.state == "COMPLETED" ? "rgba(44, 174, 137, 1)" : "rgba(255, 197, 0, 1)"} 
                                        color={"rgba(0, 0, 0, 0.50"}
                                        fillOpacity={0.75}
                                        stroke={true}    
                                        weight={4}                            
                                        center={[
                                                pollingStation.coordinate && 
                                                pollingStation.coordinate.geometry &&
                                                pollingStation.coordinate.geometry.coordinates ? 
                                                pollingStation.coordinate.geometry.coordinates[1] : 0, 

                                                pollingStation.coordinate &&
                                                pollingStation.coordinate.geometry &&
                                                pollingStation.coordinate.geometry.coordinates ? 
                                                pollingStation.coordinate.geometry.coordinates[0] : null
                                            ]}
                                        radius={8} 
                                        >
                                        <Tooltip>
                                            <p className="small font-opensans-bold px-2 py-2 border-radius__10">
                                                <span className="font-opensans-semibold">Polling Station No.</span> {pollingStation.pollingStationID}
                                            </p>
                                        </Tooltip>
                                    </CircleMarker>      
                                )
                            })} 
                        </MapContainer>
                    </div>
                :
                    <div className="w-100 text-center p-5">
                        <p className="p-5 my-5 small"><span className="mdi mdi-loading mdi-spin text__green mr-2"></span> Loading map...</p>
                    </div>
            }
        </> 
    )
}

const mapStateToProps = state => {
    return { 
        latlng: state.site.latlng,
        pollingStations: state.pollingStation.list,
        constituencyGeoJSON: state.constituency.geojson
    }
}

const mapDispatchToProps = {
    getPollingStationsByConstituency
}
export default connect(mapStateToProps, mapDispatchToProps)(PollingStationsResultMapView)
