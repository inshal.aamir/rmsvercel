import React, { createRef, useEffect, useMemo, useRef, useState } from 'react'
import axios from 'axios'
import '../../components/config.js'

import NA_GEOJSON from '../../geojson/national_updated.json'
import NA_BALOCHISTAN_GEOJSON from '../../geojson/national-assemblies/balochistan.json'

import PA_BALOCHISTAN_GEOJSON from '../../geojson/provincial-assemblies/Balochistan.json'
import PA_KPK_GEOJSON from '../../geojson/provincial-assemblies/KPK.json'
import PA_PUNJAB_GEOJSON from '../../geojson/provincial-assemblies/Punjab.json'
import PA_SINDH_GEOJSON from '../../geojson/provincial-assemblies/Sindh.json'

import geojsonData from '../../geojson/national.json'

import { connect } from "react-redux"
import { getConstituenciesByProvince, setParties } from '../../redux/actions/constituency'

import { MapContainer, GeoJSON, Tooltip, TileLayer, Popup, useMapEvents, Marker, CircleMarker, Circle, useMap } from "react-leaflet"

import "leaflet/dist/leaflet.css"
import 'react-leaflet-markercluster/dist/styles.min.css' // inside .js file

import { useRouter } from 'next/router'
import ChangeView from '../leaflet/ChangeView'

import MarkerClusterGroup from 'react-leaflet-markercluster' 
import ProvincesSelectOptions from '../select/ProvincesSelectOptions.jsx'
import Geojson from '../leaflet/Geojson.js'


const ConstituenciesMapView = (props) => {
    const { constituencies, getConstituenciesByProvince, setParties } = props,
        router = useRouter(),
        { assembly, province = assembly == "provincial" ? "balochistan" : null } = router.query,
        [geojson, setGeojson] = useState([]),
        [center, setCenter] = useState({ lat: 30.3753, lng: 69.3451 }),
        [zoom, setZoom] = useState(6), 
        [keywords, setKeywords] = useState(""),
        [searchSuggestions, setSearchSuggestions] = useState([]),
        [prop, setProp] = useState("PA"),
        [pollingStations, setPollingStations] = useState([]), 
        parties = []


    useEffect(() => setParties(parties), [parties])
    useEffect(() => {        
        console.log(assembly, province, "assembly and province")
        switch(assembly) {
            // NATIONAL ASSEMBLIES
            case "national":
                setCenter({ lat: 30.3753, lng: 69.3451 })
                setZoom(6)
                // geoJsonRef.current.leafletElement.clearLayers().addData(NA_GEOJSON.features)
                setGeojson(NA_GEOJSON.features) 
                // console.log(geoJsonLayer, "geojson-ref")
                // geoJsonLayer.current.clearLayers().addData(NA_BALOCHISTAN_GEOJSON.features)

                setProp("NA_Cons")
                // useMap
                break
            
            // PROVINCIAL ASSEMBLIES
            case "provincial":
                switch (province) {
                    case "balochistan":
                        setCenter({ lat: 28.4907, lng: 65.0958 })
                        setZoom(6.5) 
                        // geoJsonRef.current.leafletElement.clearLayers().addData(NA_BALOCHISTAN_GEOJSON.features) 
                        setGeojson(PA_BALOCHISTAN_GEOJSON.features)
                        setProp("Constituen")                             
                        break
                    case "kpk":
                        setCenter({ lat: 34.2526, lng: 72.3311 })
                        setZoom(6.5)
                        // geoJsonRef.current.leafletElement.clearLayers().addData(PA_KPK_GEOJSON.features)
                        // geoJsonLayer.current.clearLayers().addData(NA_BALOCHISTAN_GEOJSON.features)
                        setGeojson(PA_KPK_GEOJSON.features)
                        setProp("Constituen")                         
                        break
                    case "punjab":
                        setCenter({ lat: 31.1704, lng: 72.7097 })
                        setZoom(6.5)
                        // geoJsonRef.current.leafletElement.clearLayers().addData(PA_PUNJAB_GEOJSON.features)
                        // setGeojson(PA_PUNJAB_GEOJSON.features)
                        setProp("Constituen")                         
                        break
                    case "sindh":
                        setCenter({ lat: 25.8943, lng: 68.5247 })
                        setZoom(6.5)
                        // geoJsonRef.current.leafletElement.clearLayers().addData(PA_SINDH_GEOJSON.features)
                        // setGeojson(PA_SINDH_GEOJSON.features)
                        setProp('Constituen')                         
                        break
                
                    default:
                        break
                }

                break

            default:
                setProp("PA")
        }

        getConstituenciesByProvince(province)
        
    }, [assembly, province]) 


    const onEachFeature = async (feature, layer) => {  
        
        let c = await constituencies.filter(constituency => constituency.name == feature.properties[prop])[0] // c => Constituency
        
        // console.log(constituencies, feature.properties, "ccccccccccccccc")
        let popupContent = 
        `
            <div class="bg-white py-3 px-0 border-radius__10">
                <p class="font-opensans-semibold border-bottom pb-2 px-3">${feature.properties[prop]} - ${feature.properties.District || "Undefined"} (${feature.properties.Province})</p>
                <div class="row mx-0 small font-opensans-regular mt-3">
                    <p class="col-lg-5 font-opensans-semibold pb-2">Winning Party:</p> 
                    <p class="col-lg-7 pb-2 pl-0">
                        <span class="py-0 px-3 mr-1 border-radius__2 box-shadow__dark2" style="background-color: ${c ? c.winner.party.color : ""}"></span>
                        <span class="text__wrap"> ${c ? c.winner.party.name : "Unknown"}</span> 
                    </p>
                    <p class="col-lg-5 font-opensans-semibold pb-2">Candidate Name:</p> <p class="col-lg-7 pb-2 pl-0"> 
                        ${c ? c.winner.name : "Unknown"}
                    </p>
                    <p class="col-lg-5 font-opensans-semibold pb-2">Votes Polled:</p> <p class="col-lg-7 pb-2 pl-0"> 
                        ${c ? c.winner.resultCandidate.votes : "Unknown"} Votes
                    </p>
                </div>
            </div>
        `
        layer.bindTooltip(popupContent, { permanent: false, direction: "left" })    


        // layer.bindTooltip(popupContent)
        // .openPopup();
        layer.on({ click: (e) => getPollingStations(feature) })

        // let constituency = constituencies.filter(constituency => constituency.name == feature.properties[prop])

        if (c) {

            layer.options.fillColor = c.winner.party.color

            // feature.properties[prop] == "NA-263" ? layer.options.fillColor = "red" : null 
            // feature.properties[prop] == "NA-260" ? layer.options.fillColor = "yellow" : null 

            if (parties.filter(party => party.id == c.winner.party.id).length == 0) {
                parties.push({
                    id: c.winner.party.id,
                    name: c.winner.party.name,
                    color: c.winner.party.color,
                    logo: c.winner.party.logo
                })
            }

        } else {
            layer.options.fillColor = "rgba(0, 0, 0, 0)"
        }

    }

    const pointToLayer = (feature, latlng) => {
        console.log(feature, latlng, "feature and latlng")
        return CircleMarker(latlng, {
            radius: 8,
            fillColor: "#ff7800",
            color: "#000",
            weight: 1,
            opacity: 1,
            fillOpacity: 0.8
        })
    }

    function updateSuggestions() {
        
        if (geojson) {
            setSearchSuggestions(geojson.filter(record => {
                    // console.log(record.properties[prop].split(keywords).join(`<strong>${keywords}</strong>`))
                    let consituency = record.properties[prop]
                    return consituency.toLowerCase().includes(keywords.toLowerCase())

                }
            ))
        } else {
            alert("Geojson Features not found.")
        }
    }

    function getPollingStations(e) {
        var config = {
            method: 'get',
            url: `${process.env.BASE_URL}api/config/get/polling-station/constituency/NA-263`,
            headers: { 'Authorization': localStorage.getItem("access_token") },
        }
        
        axios(config)
        .then(function (response) {
            if (response.status == 200) {
                setPollingStations(response.data)
            }
          console.log(response, "getPollingStations Response");
        })
        .catch(function (error) {
          console.log(error, "error");
        });        

        console.log(e, "target...")
    }

    function getCandidates(pollingStationID) {
        var config = {
            method: 'get',
            url: `${process.env.BASE_URL}api/config/get/polling-station/${pollingStationID}`,
            headers: { 'Authorization': localStorage.getItem("access_token") },
        }
        
        console.log("getting candidates of polling station")
        axios(config)
        .then(function (response) {
            console.log(response, "my-response")
        })
        .catch(function (error) {
          console.log(error, "error");
        })        

    }

    
    return (
        <div className="overflow__auto bg-white border-radius__5">
            <div className="d-flex w-100 justify-content-between py-4 align-items-center mb-2">
                <div className="d-flex w-100 align-items-center">
                    <h5 className="font-opensans-semibold pl-4 mr-4 mb-0 text-capitalize"> {assembly == "national" ? "NA" : "PA" } Constituencies Map View</h5>
                    { assembly == "provincial" ? <ProvincesSelectOptions /> : null }            
                </div>
                <div className="position-relative mx-4" style={{ width: "425px"}}>
                    <div className="w-100 input-group bg-white border-radius__5 border__light">
                        <input 
                            value={keywords}
                            onChange={(e) => setKeywords(e.target.value) }
                            onKeyUp={(e) => updateSuggestions() }
                            style={{borderRadius: "0px 35px 35px 0px"}} type="text" className="form-control border-0 bg-transparent py-2" placeholder="Search Consistuency / Candidate Name / Party" aria-label="Username" aria-describedby="basic-addon1" />
                        <div className="input-group-append border-0">
                            <span  
                                className="input-group-text bg-transparent pr-3 pl-1 border-0">
                                <img src="/assets/img/icons/search.svg" width="16px" />
                            </span>
                        </div>
                    </div>
                    <div className="position-relative">
                        <div className="w-100 bg-white zindex__4000 border-radius__5 box-shadow__light position-absolute overflow__auto" style={{ maxHeight: "400px" }}>
                            {searchSuggestions && searchSuggestions.map((suggestion, key) => (
                                <button 
                                    onClick={() => {
                                        setCenter({
                                            lat: 28.985162400046946,
                                            lng: 63.61369237025613
                                        })

                                        setZoom(8)
                                        setSearchSuggestions([])

                                        console.log(center, suggestion, "center")
                                    }}
                                    key={key} 
                                    className="btn btn-block hover__light text-left bg-transparent px-2 border-bottom py-0 my-0">
                                        <span 
                                            className="small"
                                            dangerouslySetInnerHTML={{ __html: suggestion.properties[prop] }}>
                                        </span>
                                    </button>
                            ))}
                        </div>
                    </div>
                </div>
            </div> 
            <div className="p-3 border">
                <MapContainer
                    maxZoom={20}
                    minZoom={6}
                    onclick={(e) => console.log(e.latlng, "lat-lng")}
                    style={{ height: "80vh" }} scrollWheelZoom={true}>  
                    {/* <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" maxZoom={18} /> */}
                    
                    <Geojson data={geojson} />
                    

                    {/* <MarkerClusterGroup> */}
                    {/* <GeoJSON     
                            style={() => ({
                                fillOpacity: 0.75,
                                stroke: 1,
                                weight: 0.40,
                                opacity: 1,
                                color: '#363434',  //Outline color
                            })}               
                            key={geojson} 
                            data={geojson}  
                            pointToLayer={pointToLayer}
                            onEachFeature={onEachFeature} />       */}
                    {/* </MarkerClusterGroup> */}

                    <ChangeView center={{ lat: center.lat, lng: center.lng }} zoom={zoom} />
                    {pollingStations &&  pollingStations.map((pollingStation, i) => {
                        
                        return (
                            <CircleMarker key = {pollingStation.pollingStationID}
                                eventHandlers={{
                                    click: (e) => {
                                        getCandidates(pollingStation.pollingStationID)
                                        console.log(pollingStation, "polling-station-details")
                                    } }}
                                fillColor={pollingStation.state == "COMPLETED" ? "rgb(199, 239, 117)" : "rgb(64, 118, 249)"}
                                color={pollingStation.state == "COMPLETED" ? "rgb(199, 239, 117)" : "rgb(64, 118, 249)"}
                                opacity={1}
                                center={[pollingStation.coordinate.geometry.coordinates ? pollingStation.coordinate.geometry.coordinates[1] : 0, pollingStation.coordinate.geometry.coordinates ? pollingStation.coordinate.geometry.coordinates[0] : null]}
                                // position={{ lat: , lng: pollingStation.coordinate.geometry.coordinates[1] }} 
                                radius={5}>
                                    { console.log(pollingStation, "polling-stations")}
                                <Popup>
                                <span>
                                    {/* <strong>LatLng: </strong> {pollingStation.coordinate.geometry.coordinates[0]}, {pollingStation.coordinate.geometry.coordinates[1]} <br /> */}
                                    <strong>Name: </strong> {pollingStation.name}<br />
                                    <strong>Polling Station ID: </strong> {pollingStation.pollingStationID}<br />
                                    <strong>State: </strong> {pollingStation.state}<br />
                                    <strong>District: </strong> {pollingStation.coordinate.properties.DISTRICT }<br />
                                    {/* <strong>Gender: </strong> {pollingStation.coordinate.properties.GENDER }<br /> */}
                                    <strong>Province: </strong> {pollingStation.coordinate.properties.PROVINCE }<br />
                                </span>
                                </Popup>
                            </CircleMarker>      
                        )
                    })}                    
                </MapContainer>
            </div>
        </div> 
    )
}

const mapStateToProps = state => {
    return { 
        constituencies: state.constituency.list 
    }
}

const mapDispatchToProps = {
    getConstituenciesByProvince, setParties
}
export default connect(mapStateToProps, mapDispatchToProps)(ConstituenciesMapView)
