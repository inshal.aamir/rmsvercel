import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'

import { MapContainer, TileLayer, GeoJSON } from "react-leaflet"
import "leaflet/dist/leaflet.css"
import 'react-leaflet-markercluster/dist/styles.min.css' // inside .js file

import { connect } from 'react-redux'
import ChangeView from '../leaflet/ChangeView'
import { getConstituencyGeoJSONByAssemblyAndConstituency, setConstituencyGeoJSON } from '../../redux/actions/constituency'
import { setLatLng } from '../../redux/actions/site'

const IndividualConstituencyMapView = ({ getConstituencyGeoJSONByAssemblyAndConstituency, constituencyGeoJSON, setConstituencyGeoJSON, setLatLng }) => {

    const router = useRouter(),
        { asPath } = router,   
        { constituency, assembly, province = assembly == "provincial" ? "balochistan" : null } = router.query,
        [center] = useState({ lat: 29.274995070544108, lng: 69.18349028468037 })    

    useEffect(() => getConstituencyGeoJSONByAssemblyAndConstituency({ assembly, constituency }), [constituency, assembly])
    
    function routeToSpecificConstituency(e) {
        console.log(e, "Individual Constituency (e)")
        
        setLatLng(e.latlng)
        setConstituencyGeoJSON([e.target.feature])

        asPath = `/election-results/party-position/${assembly}`
        router.push({
            pathname: asPath,
            query: {
                ...{
                    constituency: e.target.feature.properties["NA_Cons"], view: "map"
                }, ...(province && { province })
            }            
        })            
    }
    
    return (
        <div className="bg-white border-radius__5 mb-4">
            <div className="d-flex align-items-center border-bottom mb-2 justify-content-between">
                <h6 className="font-opensans-semibold px-3 pt-3 pb-2">{ constituency && constituency.toUpperCase() } Map</h6>
                <button 
                    onClick={() => 
                        router.push({ pathname: (asPath.includes("?") ? asPath.split("?")[0] : asPath) + "/map" })}
                    className="btn mdi mdi-fullscreen bg-transparent mdi-24px d-none"></button>            
            </div>
            <div className="px-3 pb-3 pt-1">
                <MapContainer maxZoom={20} style={{ height: "450px" }} scrollWheelZoom={true}>   
                    <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" maxZoom={18} />
                    <GeoJSON     
                        style={() => ({
                            fillOpacity: 0.75,
                            stroke: 4,
                            weight: 4,
                            opacity: 1,
                            color: '#3455d9',
                            fillColor: "rgba(43, 77, 214, 0.25)"
                        })}               
                        data={constituencyGeoJSON}
                        onEachFeature={(feature, layer) => layer.on({ click: (e) => routeToSpecificConstituency(e) })}
                    />  
                    <ChangeView center={center} zoom={6} />
                </MapContainer>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return { 
        constituencyGeoJSON: state.constituency.geojson
    }
}

const mapDispatchToProps = {
    getConstituencyGeoJSONByAssemblyAndConstituency, setLatLng, setConstituencyGeoJSON
}
export default connect(mapStateToProps, mapDispatchToProps)(IndividualConstituencyMapView)
