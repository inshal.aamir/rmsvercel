import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'

import { MapContainer, TileLayer, GeoJSON } from "react-leaflet"
import "leaflet/dist/leaflet.css"
import 'react-leaflet-markercluster/dist/styles.min.css' // inside .js file

import { connect } from 'react-redux'
import ChangeView from '../leaflet/ChangeView'
import { getConstituencyGeoJSONByAssemblyAndConstituency } from '../../redux/actions/constituency'

const IndividualConstituencyWideMapView = ({ getConstituencyGeoJSONByAssemblyAndConstituency, constituencyGeoJSON }) => {

    const router = useRouter(),
        { constituency, assembly } = router.query,
        [center] = useState({ lat: 29.274995070544108, lng: 69.18349028468037 })    

    useEffect(() => getConstituencyGeoJSONByAssemblyAndConstituency({ assembly, constituency }), [constituency, assembly])

    return (
        <div className="bg-white border-radius__5 h-100">
            <div className="h-100">
                <MapContainer maxZoom={20} className="h-100" scrollWheelZoom={true}>   
                    <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" maxZoom={18} />
                    <GeoJSON     
                        style={() => ({
                            fillOpacity: 0.75,
                            stroke: 4,
                            weight: 4,
                            opacity: 1,
                            color: '#3455d9',
                            fillColor: "rgba(43, 77, 214, 0.25)"
                        })}               
                        data={constituencyGeoJSON}
                    />  
                    <ChangeView center={center} zoom={8} />
                </MapContainer>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return { 
        constituencyGeoJSON: state.constituency.geojson
    }
}

const mapDispatchToProps = {
    getConstituencyGeoJSONByAssemblyAndConstituency
}
export default connect(mapStateToProps, mapDispatchToProps)(IndividualConstituencyWideMapView)
