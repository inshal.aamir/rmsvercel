import React, { useEffect, useMemo, useRef, useState } from 'react'
import geojsonData from '../../geojson/test-geojson2.json'
import { MapContainer, GeoJSON, Tooltip, TileLayer, Popup, useMapEvents, Marker, CircleMarker, Circle } from "react-leaflet"
import "leaflet/dist/leaflet.css"
const center = [51.505, -0.09]


export default function PartyPositionMayorMapPlaceholder() {

    // const map = useMapEvents({
    //     zoomend(e) {
    //         console.log(e, "zoom-in")
    //     }
    // })

    function TooltipCircle() {
        const [clickedCount, setClickedCount] = useState(0)
        const eventHandlers = useMemo(
          () => ({
            click() {
              setClickedCount((count) => count + 1)
            },
          }),
          [],
        )
      
        const clickedText =
          clickedCount === 0
            ? 'Click this Circle to change the Tooltip text'
            : `Circle click: ${clickedCount}`
      
        return (
          <Circle
            center={center}
            eventHandlers={eventHandlers}
            pathOptions={{ fillColor: 'blue' }}
            radius={200}>
            <Tooltip>{clickedText}</Tooltip>
          </Circle>
        )
      }
         
    const onEach = (feature, layer, test) => {
        // console.log(feature, layer, "feature and layer")
        let popupContent =
            "<pre>" +
            feature.properties.NAME_3
            // JSON.stringify(feature.properties, null, " ").replace(/[\{\}"]/g, "") 
            +
            "</pre>";
            feature.properties.NAME_3 == "Chagai" ? layer.options.fillColor = "red" : null
        // layer.bindCircle([50.5, 30.5], {radius: 200})
        layer.bindTooltip(popupContent, { permanent: true, direction: "center" }).openPopup();
    };
    // console.log(geojsonData.features, "features")
    return (
        <div className="overflow__auto bg-white border-radius__5 mb-4">
            <h6 className="font-opensans-semibold px-3 pt-3 pb-2">National Assembly Constituencies Map View</h6>
            <div className="p-3">
                <MapContainer
                    useMapEvents={{
                        zoomend: (e) => console.log(e, "eeeee")
                    }}
                    on={(viewport) => console.log(viewport, "viewport")} 
                    style={{ height: "80vh" }} center={{ lat: 30.3753, lng: 69.3451 }} zoom={6} scrollWheelZoom={true}>
                    
                    <GeoJSON
                        pointToLayer={(feature, latlng) => {
                            console.log(feature, "feature", latlng, "latlng")
                        }}
                        style={() => ({
                        // color: '#4a83ec',
                        // weight: 0.5,
                        // fillColor: "#1a1d62",
                        // fillOpacity: 1,
                        })} 
                        data={geojsonData.features} onEachFeature={onEach}></GeoJSON>
                
                        {/* <TileLayer
                            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a>
                        contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        /> */}
                    {/* <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    /> */}
                    {/* <TooltipCircle /> */}
                    {/* <CircleMarker
                        center={[51.51, -0.12]}
                        pathOptions={{ color: 'red' }}
                        radius={20}>
                        <Tooltip>Tooltip for CircleMarker</Tooltip>
                    </CircleMarker> */}

                </MapContainer>
            </div>
        </div> 
    )
}
