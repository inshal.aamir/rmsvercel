import React, { createRef, useEffect, useMemo, useRef, useState } from "react";
import axios from "axios";
import "../config.js";

import NA_GEOJSON from "../../geojson/national_updated.json";
import NA_BALOCHISTAN_GEOJSON from "../../geojson/national-assemblies/balochistan.json";

import PA_BALOCHISTAN_GEOJSON from "../../geojson/provincial-assemblies/Balochistan.json";
import PA_KPK_GEOJSON from "../../geojson/provincial-assemblies/KPK.json";
import PA_PUNJAB_GEOJSON from "../../geojson/provincial-assemblies/Punjab.json";
import PA_SINDH_GEOJSON from "../../geojson/provincial-assemblies/Sindh.json";

import geojsonData from "../../geojson/national.json";

import { connect } from "react-redux";
import {
  getConstituenciesByProvince,
  setParties,
} from "../../redux/actions/constituency";

import {
  MapContainer,
  GeoJSON,
  Tooltip,
  TileLayer,
  Popup,
  useMapEvents,
  Marker,
  CircleMarker,
  Circle,
  useMap,
} from "react-leaflet";

import "leaflet/dist/leaflet.css";
import "react-leaflet-markercluster/dist/styles.min.css"; // inside .js file

import { useRouter } from "next/router";
import ChangeView from "../leaflet/ChangeView";

import ProvincesSelectOptions from "../select/ProvincesSelectOptions.jsx";
import AssemblyConstituenciesGeoJSON from "../leaflet/AssemblyConstituenciesGeoJSON.js";

const ConstituenciesResultMapView = (props) => {
  const { getConstituenciesByProvince, setParties } = props,
    router = useRouter(),
    { assembly, province = assembly == "provincial" ? "balochistan" : null } =
      router.query,
    [geojson, setGeojson] = useState([]),
    [center, setCenter] = useState({ lat: 30.3753, lng: 69.3451 }),
    [zoom, setZoom] = useState(6),
    [keywords, setKeywords] = useState(""),
    [searchSuggestions, setSearchSuggestions] = useState([]),
    [prop, setProp] = useState("PA"),
    parties = [];

  useEffect(() => document.getElementById("constituencymap").focus());
  useEffect(() => {
    console.log(assembly, province, "assembly and province");
    switch (assembly) {
      // NATIONAL ASSEMBLIES
      case "national":
        setCenter({ lat: 30.3753, lng: 69.3451 });
        setZoom(6);
        // geoJsonRef.current.leafletElement.clearLayers().addData(NA_GEOJSON.features)
        setGeojson(NA_GEOJSON.features);
        // console.log(geoJsonLayer, "geojson-ref")
        // geoJsonLayer.current.clearLayers().addData(NA_BALOCHISTAN_GEOJSON.features)

        setProp("NA_Cons");
        // useMap
        break;

      // PROVINCIAL ASSEMBLIES
      case "provincial":
        switch (province) {
          case "balochistan":
            setCenter({ lat: 28.4907, lng: 65.0958 });
            setZoom(6);
            // geoJsonRef.current.leafletElement.clearLayers().addData(NA_BALOCHISTAN_GEOJSON.features)
            setGeojson(PA_BALOCHISTAN_GEOJSON.features);
            setProp("PA");
            break;
          case "kpk":
            setCenter({ lat: 34.2526, lng: 72.3311 });
            setZoom(6.5);
            // geoJsonRef.current.leafletElement.clearLayers().addData(PA_KPK_GEOJSON.features)
            // geoJsonLayer.current.clearLayers().addData(NA_BALOCHISTAN_GEOJSON.features)
            setGeojson(PA_KPK_GEOJSON.features);
            setProp("PA");
            break;
          case "punjab":
            setCenter({ lat: 31.1704, lng: 72.7097 });
            setZoom(6.5);
            // geoJsonRef.current.leafletElement.clearLayers().addData(PA_PUNJAB_GEOJSON.features)
            setGeojson(PA_PUNJAB_GEOJSON.features);
            setProp("PA");
            break;
          case "sindh":
            setCenter({ lat: 25.8943, lng: 68.5247 });
            setZoom(6.5);
            // geoJsonRef.current.leafletElement.clearLayers().addData(PA_SINDH_GEOJSON.features)
            setGeojson(PA_SINDH_GEOJSON.features);
            setProp("PA");
            break;

          default:
            break;
        }
        break;
      default:
        setProp("PA");
    }

    getConstituenciesByProvince(province);
  }, [assembly, province]);

  function updateSuggestions() {
    if (geojson) {
      setSearchSuggestions(
        geojson.filter((record) => {
          let consituency = record.properties[prop];
          return consituency.toLowerCase().includes(keywords.toLowerCase());
        })
      );
    } else {
      alert("Geojson Features not found.");
    }
  }

  return (
    <>
      <div className="d-flex w-100 justify-content-between pt-4 align-items-center">
        <div className="d-flex w-100 align-items-center">
          <h5 className="font-opensans-semibold pl-4 mr-4 mb-0 text-capitalize">
            {" "}
            {assembly == "national" ? "NA" : "PA"} Constituencies Map View
          </h5>
          {assembly == "provincial" ? <ProvincesSelectOptions /> : null}
        </div>
        <div className="position-relative mx-4" style={{ width: "425px" }}>
          <div className="w-100 input-group bg-white border-radius__5 border__light">
            <input
              value={keywords}
              onChange={(e) => setKeywords(e.target.value)}
              onKeyUp={(e) => updateSuggestions()}
              style={{ borderRadius: "0px 35px 35px 0px" }}
              type="text"
              className="form-control border-0 bg-transparent py-2"
              placeholder="Search Consistuency / Candidate Name / Party"
              aria-label="Username"
              aria-describedby="basic-addon1"
            />
            <div className="input-group-append border-0">
              <span className="input-group-text bg-transparent pr-3 pl-1 border-0">
                <img src="/assets/img/icons/search.svg" width="16px" />
              </span>
            </div>
          </div>
          <div className="position-relative">
            <div
              className="w-100 bg-white zindex__4000 border-radius__5 box-shadow__light position-absolute overflow__auto"
              style={{ maxHeight: "400px" }}
            >
              {searchSuggestions &&
                searchSuggestions.map((suggestion, key) => (
                  <button
                    onClick={() => {
                      setCenter({
                        lat: 28.985162400046946,
                        lng: 63.61369237025613,
                      });

                      setZoom(8);
                      setSearchSuggestions([]);

                      console.log(center, suggestion, "center");
                    }}
                    key={key}
                    className="btn btn-block hover__light text-left bg-transparent px-2 border-bottom py-0 my-0"
                  >
                    <span
                      className="small"
                      dangerouslySetInnerHTML={{
                        __html: suggestion.properties[prop],
                      }}
                    ></span>
                  </button>
                ))}
            </div>
          </div>
        </div>
      </div>
      <div className="p-3">
        <MapContainer
          id="constituencymap"
          maxZoom={20}
          minZoom={6}
          style={{ height: "80vh" }}
          scrollWheelZoom={true}
        >
          <ChangeView
            center={{ lat: center.lat, lng: center.lng }}
            zoom={zoom}
          />
          <AssemblyConstituenciesGeoJSON data={geojson} myprop={prop} />
        </MapContainer>
      </div>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    constituencies: state.constituency.list,
  };
};

const mapDispatchToProps = {
  getConstituenciesByProvince,
  setParties,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConstituenciesResultMapView);
