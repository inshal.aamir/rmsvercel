import React, { createRef, useEffect, useMemo, useRef, useState } from 'react'
import axios from 'axios'
import '../../components/config.js'

import NA_GEOJSON from '../../geojson/national.json'
import NA_BALOCHISTAN_GEOJSON from '../../geojson/national-assemblies/balochistan.json'

import PA_BALOCHISTAN_GEOJSON from '../../geojson/provincial-assemblies/Balochistan.json'
import PA_KPK_GEOJSON from '../../geojson/provincial-assemblies/KPK.json'
import PA_PUNJAB_GEOJSON from '../../geojson/provincial-assemblies/Punjab.json'
import PA_SINDH_GEOJSON from '../../geojson/provincial-assemblies/Sindh.json'

import geojsonData from '../../geojson/national.json'

import { connect } from "react-redux"
import { getConstituencyList } from '../../redux/actions/constituency'

import { MapContainer, GeoJSON, Tooltip, TileLayer, Popup, useMapEvents, Marker, CircleMarker, Circle, useMap } from "react-leaflet"
import "leaflet/dist/leaflet.css"
import { useRouter } from 'next/router'
import ChangeView from '../leaflet/ChangeView'

import * as L from 'leaflet'
import "leaflet.markercluster"


const ConstituenciesMapView2 = (props) => {

    const { constituencyList, getConstituencyList } = props,
        router = useRouter(),
        { province, view } = router.query,
        [loading, setLoading] = useState(true),
        [geojson, setGeojson] = useState([]),
        [center, setCenter] = useState({ lat: 30.3753, lng: 69.3451 }),
        [zoom, setZoom] = useState(6),
        // [map, setMap] = useState(null),
        [keywords, setKeywords] = useState(""),
        [searchSuggestions, setSearchSuggestions] = useState([]),
        [prop, setProp] = useState("PA"),
        [pollingStations, setPollingStations] = useState([])


    useEffect(() => {
        setTimeout(() => {
            const map = L.map("map").setView([30.3753, 69.3451], 6)
            const osm = L.tileLayer(
              "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              {
                maxZoom: 19,
                attribution:
                  '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
              }
            ).addTo(map)    
            
            // const markersCluster = L.markerClusterGroup()
            // const markers = markersCluster.addLayer(geojson)
            // map.addLayer(markers)
    
            console.log(new L.markerClusterGroup(), "L")                
        }, 999)

    }, [])
    
    useEffect(() => {
        switch(province) {
            case "national":
                setCenter({ lat: 30.3753, lng: 69.3451 })
                setZoom(6)
                setGeojson(NA_GEOJSON.features) 
                setProp("NA")
                // useMap
                break
            
            // PROVINCIAL ASSEMBLIES
            case "balochistan":
                setCenter({ lat: 28.4907, lng: 65.0958 })
                setZoom(6)
                setGeojson(NA_BALOCHISTAN_GEOJSON.features)
                setProp("Constituen")
                break
            case "kpk":
                setCenter({ lat: 34.2526, lng: 72.3311 })
                setZoom(6.5)
                setGeojson(PA_KPK_GEOJSON.features)
                break
            case "punjab":
                setCenter({ lat: 31.1704, lng: 72.7097 })
                setZoom(6.5)
                setGeojson(PA_PUNJAB_GEOJSON.features)
                break
            case "sindh":
                setCenter({ lat: 25.8943, lng: 68.5247 })
                setZoom(6.5)
                setGeojson(PA_SINDH_GEOJSON.features)
                break

            default:
                setProp("PA")
        }
    }, [province])

    useEffect(() => constituencyList.length == 0 ? getConstituencyList({ province: "balochistan"}) : null, [province])
    useEffect(() => constituencyList.length > 0 ? setLoading(false) : null, [constituencyList])


    const onEachFeature = (feature, layer) => {
        let popupContent = "<pre>" + feature.properties[prop] + "</pre>"
        layer.bindTooltip(popupContent, { permanent: true, direction: "center" }).openPopup();
        layer.on({ click: (e) => getPollingStations(feature) })
        let constituency = constituencyList.filter(constituency => constituency.name == feature.properties[prop])
        constituency && constituency.length > 0 ? layer.options.fillColor = constituency[0].winner.party.color : layer.options.fillColor = "#FE3737"
        // feature.properties[prop] == "PB-39" ? layer.options.fillColor = "red" : null
    }

    function updateSuggestions() {
        
        if (geojson) {
            setSearchSuggestions(geojson.filter(record => {
                    // console.log(record.properties[prop].split(keywords).join(`<strong>${keywords}</strong>`))
                    let consituency = record.properties[prop]
                    return consituency.toLowerCase().includes(keywords.toLowerCase())

                }
            ))
        } else {
            alert("Geojson Features not found.")
        }
    }

    function getPollingStations(e) {
        var config = {
            method: 'get',
            url: `${process.env.BASE_URL}api/config/get/polling-station/constituency/NA-260`,
            headers: { 'Authorization': localStorage.getItem("access_token") },
        }
        
        axios(config)
        .then(function (response) {
            if (response.status == 200) {
                setPollingStations(response.data)
            }
          console.log(response, "getPollingStations Response");
        })
        .catch(function (error) {
          console.log(error, "error");
        });        

        console.log(e, "target...")
    }

    function getCandidates(pollingStationID) {
        var config = {
            method: 'get',
            url: `${process.env.BASE_URL}api/config/get/polling-station/${pollingStationID}`,
            headers: { 'Authorization': localStorage.getItem("access_token") },
        }
        
        console.log("getting candidates of polling station")
        axios(config)
        .then(function (response) {
            console.log(response, "my-response")
        })
        .catch(function (error) {
          console.log(error, "error");
        })        

    }

    
    return (
        <div className="overflow__auto bg-white border-radius__5 mb-4">
            <div className="d-flex w-100 justify-content-between py-3 align-items-center mb-2">
                
                <h6 className="font-opensans-semibold px-3">National Assembly Constituencies Map View</h6>
                <div className="position-relative mx-4" style={{ width: "425px"}}>
                    <div className="w-100 input-group bg-white border-radius__5 border__light">
                        <input 
                            value={keywords}
                            onChange={(e) => setKeywords(e.target.value) }
                            onKeyUp={(e) => updateSuggestions() }
                            style={{borderRadius: "0px 35px 35px 0px"}} type="text" className="form-control border-0 bg-transparent py-2" placeholder="Search Consistuency / Candidate Name / Party" aria-label="Username" aria-describedby="basic-addon1" />
                        <div className="input-group-append border-0">
                            <span  
                                className="input-group-text bg-transparent pr-3 pl-1 border-0">
                                <img src="/assets/img/icons/search.svg" width="16px" />
                            </span>
                        </div>
                    </div>
                    <div className="position-relative">
                        <div className="w-100 bg-white zindex__4000 border-radius__5 box-shadow__light position-absolute overflow__auto" style={{ maxHeight: "400px" }}>
                            {searchSuggestions && searchSuggestions.map((suggestion, key) => (
                                <button 
                                    onClick={() => {
                                        setCenter({
                                            lat: 28.985162400046946,
                                            lng: 63.61369237025613
                                        })

                                        setZoom(8)
                                        setSearchSuggestions([])

                                        console.log(center, suggestion, "center")
                                    }}
                                    key={key} 
                                    className="btn btn-block hover__light text-left bg-transparent px-2 border-bottom py-0 my-0">
                                        <span 
                                            className="small"
                                            dangerouslySetInnerHTML={{ __html: suggestion.properties[prop] }}>
                                        </span>
                                    </button>
                            ))}
                        </div>
                    </div>
                </div>
            </div> 
            
            <div className="p-3">
                <div id="map"></div>
                
                <>
                    {/* <MapContainer
                        onclick={(e) => console.log(e.latlng, "lat-lng")}
                        style={{ height: "80vh" }} scrollWheelZoom={true}>                    
                        <GeoJSON     
                            style={() => ({
                            fillOpacity: 0.5,
                            })}                
                            data={geojson} onEachFeature={onEachFeature}></GeoJSON>
                        <ChangeView center={{ lat: center.lat, lng: center.lng }} zoom={zoom} />
                        {pollingStations &&  pollingStations.map((pollingStation, i) => {
                            return (
                                <CircleMarker 
                                    eventHandlers={{
                                        click: (e) => {
                                            getCandidates(pollingStation.pollingStationID)
                                            console.log(pollingStation, "polling-station-details")
                                        } }}
                                    fillColor="red"
                                    color="red"
                                    opacity={1}
                                    center={[pollingStation.coordinate.geometry.coordinates[1], pollingStation.coordinate.geometry.coordinates[0]]}
                                    // position={{ lat: , lng: pollingStation.coordinate.geometry.coordinates[1] }} 
                                    radius={5}>
                                    <Popup>
                                    <span>
                                        <strong>LatLng: </strong> {pollingStation.coordinate.geometry.coordinates[0]}, {pollingStation.coordinate.geometry.coordinates[1]} <br />
                                        <strong>Name: </strong> {pollingStation.name}<br />
                                        <strong>Polling Station ID: </strong> {pollingStation.pollingStationID}<br />
                                        <strong>State: </strong> {pollingStation.state}<br />
                                        <strong>District: </strong> {pollingStation.coordinate.properties.DISTRICT }<br />
                                        <strong>Gender: </strong> {pollingStation.coordinate.properties.GENDER }<br />
                                        <strong>Province: </strong> {pollingStation.coordinate.properties.PROVINCE }<br />
                                    </span>
                                    </Popup>
                                </CircleMarker>      
                            )
                        })}                    
                    </MapContainer> */}
                
                </>
            </div>
        </div> 
    )
}

const mapStateToProps = state => {
    return { 
        constituencyList: state.constituency.list 
    }
}

const mapDispatchToProps = {
    getConstituencyList
}
export default connect(mapStateToProps, mapDispatchToProps)(ConstituenciesMapView2)
