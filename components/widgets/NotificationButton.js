import React from 'react';

export default function NotificationButton(props) {
    let { count, icon } = props
    return (
        <button type="button" className="btn bg-transparent btn__icon d-none">
            <span className="position-relative">
                <img src={icon} width="22px" />
                <span className="border__white position-absolute bg__green text-white" style={{ fontSize: "9.5px", width: "16px", height: "16px", borderRadius: "22px", top: "-5px", right: "-5px"}}>
                    {count}
                </span>
            </span>
        </button>      
    )
}
