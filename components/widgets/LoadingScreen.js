import React from 'react'
import Head from 'next/head'



export default function LoadingScreen() {
    return (
        <>
            <Head>
                <title>Loading... | EVM Portal</title>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />            
            </Head>           
            <div style={{ 
                width: "100vw",
                height: "100vh",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                background: "#1D1D1D"
            }}>
                <img className="animate__animated animate__flipInX" src="/assets/img/logo.svg" width="250px" />
            </div>        
        </>
    )
}
