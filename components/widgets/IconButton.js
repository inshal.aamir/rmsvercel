import React from 'react';

export default function IconButton(props) {
    let { icon } = props
    return (
        <button type="button" className="btn bg-transparent btn__icon">
            <img src={icon} width="22px" />
        </button>      
    )
}
