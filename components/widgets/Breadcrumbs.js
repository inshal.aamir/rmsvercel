import Link from 'next/link';
import React from 'react';

export default function Breadcrumbs({ links }) { 
    return (
        <div className="w-100 d-flex align-items-center text__grey4 mt-3">
            {links && links.map((link, key) => (
                <span key={key}>
                    <Link href={link.key}>
                            <a className="px-0 py-0 text__grey4 text-decoration__none">
                                <span 
                                    className={"small " + ( key == links.length - 1 ? "font-opensans-bold text__green" : "")}>{ link.name }</span>
                            </a>
                    </Link>  
                    { key < links.length - 1 ? <span className="mx-2 mt-1">/</span> : null }                
                </span>
            ))}
        </div>

    )
}
