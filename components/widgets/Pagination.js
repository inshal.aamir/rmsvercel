import React from 'react'
import { Link } from 'react-router-dom'

export default function Pagination() {
    return (
        <tr>
            <td colSpan={8} className="py-4 text-right px-4">
                <div className="d-flex align-items-center justify-content-end mr-4 py-2">
                    {/* <Link href="/"> */}
                        <button className="btn mdi mdi-arrow-left text-white bg__dark2 border-radius__5 mr-2"></button>
                    {/* </Link> */}
                    <button className="btn bg-transparent text__green"><span className="small font-weight-bold">1</span></button>
                    <button className="btn bg-transparent text-dark"><span className="small font-weight-bold">2</span></button>
                    <button className="btn bg-transparent text-dark"><span className="small font-weight-bold">3</span></button>
                    <button className="btn bg-transparent text-dark"><span className="small font-weight-bold">4</span></button>
                    <button className="btn bg-transparent text-dark"><span className="small font-weight-bold">5</span></button>
                    <p className="text-dark mb-0 mx-1">...</p>
                    <button className="btn bg-transparent text-dark"><span className="small font-weight-bold">10</span></button>
                    {/* <Link href={`/`}> */}
                        <button className="btn mdi mdi-arrow-right text-white bg__green border-radius__5 ml-2"></button>                                        
                    {/* </Link> */}
                </div>
            </td>
        </tr>    
    )
}
