import React, { useEffect, useState } from 'react'
import { updateObjectInArray } from '../../js/helpers'

import { connect } from "react-redux"
import { getRoles, setPermissions, setRoles } from '../../redux/actions/user'

const SearchSuggestionDropdown = ({ disabled = false, roles, getRoles, setRoles, setPermissions, user }) => {
    const [showSuggestions, setShowSuggestions] = useState(false)

    useEffect(() => getRoles() , [])
    // useEffect(() => roles && roles.length > 0 ? setDesignations(roles) : null , [roles])   

    // useEffect(() => {

    // }, [roles, ])
    
    useEffect(() => {
        let filteredPermissions = roles.filter((role) => role.checked),
            permissions = []

        filteredPermissions.forEach(p => permissions = permissions.concat(p.permissions))
        !user ? permissions.forEach(permission => permission["checked"] = false) : null
        setPermissions(permissions)

    }, [roles])

    useEffect(() => {
        if (user) {    // user editing...
            // setPermissions(user.user_permissions)
            // roles.forEach(role => {
            //     if (user.role.filter(userRole => userRole.role == role.role).length > 0) {
            //         role["checked"] = true
            //     }
            // })

            // setRoles(roles)
            // console.log(user, "editable-user")
        }

    }, [user])
    
    return (
        <>
            <div 
                className={"border border__light3 transition__3 " + (showSuggestions ? "box-shadow__dark2" : "") }
                style={{ borderRadius: showSuggestions ? "12px 12px 0px 0px" : "12px" }}>
                <button 
                    onClick={() => (roles.filter(d => d.checked).length == 0 || !showSuggestions) && !disabled ? setShowSuggestions(!showSuggestions) : null}
                    type="button" 
                    className={
                        "btn w-100 text-left bg-transparent pr-2 transition__3 py-1 " + 
                        (roles.filter(d => d.checked).length == 0 ? " pl-3 " : " pl-2 ") + 
                        (disabled ? "py-2" : "py-1") }>
                    <span className="d-flex align-items-center justify-content-between">
                        <span>
                            {
                                roles.filter(d => d.checked).length == 0 
                                ? "Select"
                                : roles.filter(d => d.checked).map((r, key) => (
                                    <span
                                        onClick={() => (
                                            !disabled ?
                                            setRoles(
                                                updateObjectInArray({ 
                                                    arr: roles, 
                                                    obj: {...r, checked: false},
                                                    i: key,
                                                    prop: "role"
                                                })
                                            ) : null
                                        )} 
                                        key={key} className=" bg__green3 text__green2 mr-2 pl-3 mb-0 small border-radius__24 py__6px">
                                        { r.role }
                                        <span className="mdi mdi-close pr-2 pl-2 text-dark"></span>
                                    </span>
                                ))
                            }
                        </span>
                        { disabled ? <span></span> :
                            <span
                                onClick={() => setShowSuggestions(!showSuggestions)} 
                                className="mdi mdi-chevron-down mdi-20px"></span>                        
                        }
                    </span>
                </button>
            </div>
            <div 
                onMouseLeave={() => setShowSuggestions(false)}                                            
                className={"position-absolute bg-white w-100 zindex__2000 box-shadow__dark2 overflow__hidden " + (showSuggestions ? "border-left border-right border-bottom" : null )} 
                style={{ borderRadius: "0px 0px 12px 12px", maxHeight: showSuggestions ? "999px" : "0px", height: "auto" }}>
                <div className="input-group justify-content-between flex-nowrap d-none">
                    <input 
                        type="text"
                        className="form-control py-2 my-1 px-3 border-0 w-100 small border-radius__15 bg-transparent" placeholder="Search"/>
                        <div className="input-group-append">
                            <span className={"p-0 input-group-text bg-transparent border-0 pr-3"}><img src="/assets/img/icons/search.svg" width={"17px"} /></span>
                        </div>
                </div>                                               
                <div className="px-4 border-top pt-3 pb-1 mb-1">
                    {
                        roles && roles.map((role, key) => (
                            <div key={key} className="mb-3">
                                <input
                                    onChange={(e) => (
                                        setRoles(
                                            updateObjectInArray({ 
                                                arr: roles, 
                                                obj: {...role, checked: e.target.checked},
                                                i: key,
                                                prop: "role"
                                            })
                                        )
                                    )} 
                                    type="checkbox" id={`user__${key}`} name="radio-group" checked={role.checked} />                
                                <label htmlFor={`user__${key}`} className="text-capitalize">
                                    <span></span>
                                    {role.role}
                                </label>
                            </div>    
                        ))                                                    
                    }                                                                   
                </div>
            </div>                                  
        </>
    )
}

const mapStateToProps = state => {
    return { 
        roles: state.user.roles,
        user: state.user.data
    }
}

const mapDispatchToProps = {
    getRoles, setPermissions, setRoles
}

SearchSuggestionDropdown.layout = "L"
export default connect(mapStateToProps, mapDispatchToProps)(SearchSuggestionDropdown)