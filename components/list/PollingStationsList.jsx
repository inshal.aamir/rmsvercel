import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import PollingStationDetailsModal from '../modals/PollingStationDetailsModal'
import { setPollingStationData } from '../../redux/actions/pollingstation'
import { numberWithCommas } from '../../js/helpers'

const PollingStationsList = ({ pollingStations, setPollingStationData }) => {

    useEffect(() => console.log(pollingStations, "polling-stations-list-fetched"), [pollingStations])
    return (
        <>
            <PollingStationDetailsModal /> 
            <div id="pollingstations-container" className="row mx-0 overflow__auto bg-white border-radius__5 pt-3 h-100 small overflow__auto box-shadow__light" style={{ maxHeight: "90vh" }}>
                    {
                        pollingStations && pollingStations.length > 0 ?
                        <>
                            <p className="col-lg-2 pr-1 pb-2 font-opensans-semibold border-bottom">Polling Station No</p>
                            <p className="col-lg-5 px-2 font-opensans-semibold border-bottom">Polling Station complete address and Location</p>
                            <p className="col-lg-2 px-1 font-opensans-semibold border-bottom">No. of Booths</p>
                            <p className="col-lg-3 px-1 font-opensans-semibold border-bottom">Total Number of Voters</p>
                            {pollingStations.map((pollingStation, key) => (
                                <div
                                    onClick={() => setPollingStationData(pollingStation)}
                                    data-toggle="modal" data-target="#pollingStationDetailsModal"
                                    id={`pollingstation-${pollingStation.pollingStationID}`} key={key} className="cursor__pointer transition__3 row mx-0 col-lg-12 px-0 border-bottom py-2 polling__station__item">
                                    <p className="col-lg-2 mb-0 pb-0 pl-4">{ numberWithCommas(pollingStation.pollingStationID) }</p>
                                    <p className="col-lg-5 mb-0 pb-0 text-capitalize d-flex">
                                        <span className={"mdi mdi-circle mr-1 pr-1 small pt-1 " + (pollingStation.state && pollingStation.state.toUpperCase().includes("COMPLETED") ? "text__green" : "text__yellow" )}></span> 
                                        <span>{ pollingStation.coordinate && pollingStation.coordinate.properties && pollingStation.coordinate.properties.NAME }</span>
                                    </p>
                                    <p className="col-lg-2 mb-0 pb-0">{numberWithCommas(parseInt(pollingStation.maleBooth) + parseInt(pollingStation.femaleBooth))}</p>
                                    <p className="col-lg-3 mb-0 pb-0 text__green font-opensans-semibold">{numberWithCommas(parseInt(pollingStation.maleVoter) + parseInt(pollingStation.femaleVoter))}</p>
                                </div>
                            ))}
                        </>

                        :
                            <div className="w-100 text-center p-5">
                                <p className="p-5 my-5"><span className="mdi mdi-loading mdi-spin text__green mr-2"></span> Loading polling stations</p>
                            </div>
        
                    }                
            </div>
        </>
    )
}

const mapStateToProps = state => {
    return { 
        pollingStations: state.pollingStation.list
    }
}

const mapDispatchToProps = {
    setPollingStationData
}
 
export default connect(mapStateToProps, mapDispatchToProps)(PollingStationsList)
