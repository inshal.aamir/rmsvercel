import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { connect } from "react-redux"

const WinningPoliticalPartiesInConstituenciesList = ({ constituencies }) => {
    const [parties, setParties] = useState([
        // { name: "Independent", color: "#A1211A" },
        // { name: "Pak Sarzameen Party", color: "#63512d" },
        // { name: "Balochistan National Movement", color: "#F0F076" },
        // { name: "Allah-o-Akbar Tehreek", color: "#712784" },
        // { name: "PPPP", color: "#FE3737" },
        // { name: "PTI", color: "#FFBEBE" },
        // { name: "Independent", color: "#9BAECA" },
        // { name: "Other Political Parties", color: "#F0F076" },
        // { name: "Awaited", color: "#C6913A" },
    ]),
    // const { parties } = props,
        router = useRouter(),
        { assembly } = router.query

    useEffect(() => {
        let p = []
        constituencies.forEach(c => {
            if (p.filter(p => p.name == (c && c.winner && c.winner.party && c.winner.party.name)).length == 0 ) p.push({ ...c.winner.party, winner: c.winner.name, outcome: c.winner.outcome })
        })
        setParties(p)
        
    }, [constituencies])
    return (
        <div className="overflow__auto bg-white border-radius__5 px-4 pt-3 h-100">
            <h6 className="font-opensans-semibold pt-3 pb-2 text-capitalize">{assembly} Assembly Constituencies</h6>
            <p className="small mb-4 pb-2">Winning Political Parties</p>
            {parties && parties.length > 0 ? 
                parties.map((party, key) => (
                <div key={key} className="row mx-0 align-items-center mb-3 pb-1">
                    <div className="col-lg-2 py-3 border-radius__2 box-shadow__light" style={{ backgroundColor: party.color }}></div>
                    <p className="col-lg-10 mb-0">{ party.name } <i className="small text-info font-opensans-semibold d-none">(winner)</i></p>
                </div>
            )) : 
                <div className="w-100 text-center p-5">
                    <p className="p-5 my-5 small"><span className="mdi mdi-loading mdi-spin text__green mr-2"></span> Loading Winning Parties</p>
                </div>
            }
        </div>
    )
}

const mapStateToProps = state => {
    return { 
        constituencies: state.constituency.list 
    }
}

const mapDispatchToProps = { 
}
export default connect(mapStateToProps, mapDispatchToProps)(WinningPoliticalPartiesInConstituenciesList)