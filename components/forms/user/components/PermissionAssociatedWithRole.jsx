import React, { useEffect } from 'react'

import { connect } from 'react-redux'
import { updateObjectInArray } from '../../../../js/helpers'

import { setPermissions } from '../../../../redux/actions/user'

const PermissionAssociatedWithRole = ({ setPermissions, permissions, roles, user }) => {

    useEffect(() => {
        let filtered = roles.filter((role) => role.checked), permissions = []
        filtered.forEach(p => permissions = permissions.concat(p.permissions))
        setPermissions(permissions, user) // user is optional here            
    
    }, [roles, user])

    return (
        <>
            {permissions.length > 0 ? 
                permissions && permissions.map((permission, key) => (
                    <div key={key} className="mb-3">
                        <input
                            onChange={(e) => (
                                setPermissions(
                                    updateObjectInArray({ 
                                        arr: permissions, 
                                        obj: {...permission, checked: e.target.checked},
                                        i: key,
                                        prop: "codename"
                                    })
                                )
                            )}
                            type="checkbox" id={`permission__${key}`} name="radio-group" checked={permission.checked} />
                        <label htmlFor={`permission__${key}`} className="text-capitalize"><span></span>{permission.name}</label>
                    </div>                                                                                                    
                ))
            :
                <p className="small mb-0 text__grey5 mt-1"><i>Please select a role(s) to show permissions here</i></p>
            }           
        </>
    )
}

const mapStateToProps = state => {
    return { 
        permissions: state.user.permissions,
        roles: state.user.roles,
        user: state.user.data
    }
}

const mapDispatchToProps = {
    setPermissions
}

export default connect(mapStateToProps, mapDispatchToProps)(PermissionAssociatedWithRole)