import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import FormData from 'form-data'

import { connect } from 'react-redux'
import { setUser } from '../../../redux/actions/user'
import $ from 'jquery'

import { createUser, getRoles } from '../../../redux/actions/user'
import { setErrors } from '../../../redux/actions/site'
import RoleSuggestionDropdown from '../../dropdowns/RoleSuggestionDropdown' 
import PermissionAssociatedWithRole from './components/PermissionAssociatedWithRole'
import ApiResponseModal from '../../modals/ApiResponseModal'

const CreateNewUserForm = (props) => {
    
    const { permissions, roles, createUser, errors, getRoles, loading, message, setUser, setErrors } = props 

    const [username, setUsername] = useState(""),
        [firstName, setFirstName] = useState(""),
        [lastName, setLastName] = useState(""),
        [email, setEmail] = useState(""),
        [cnic, setCnic] = useState(""),
        [password, setPassword] = useState(""),
        [profile, setProfile] = useState(""),
        [preview, setPreview] = useState(null),
        [passwordHidden, setPasswordHidden] = useState(true),
        [designation, setDesignation] = useState(""),
        [errors1, setErrors1] = useState(false)
    
    useEffect(() => getRoles(), [])
    // useEffect(() => message ? window.$("#apiResponseModal").modal() : null , [message])

    function showPreview(event) {
        if(event.target.files.length > 0) {
            let { files } = event.target
            var src = URL.createObjectURL(files[0])

            setProfile(files[0])
            setPreview(src)
        }        
    }

    function submitFormData(e) {
        e.preventDefault() 

        if (cnic.length<13) return setErrors({cnic:['Please enter a valid CNIC']})
        if (roles.filter(r => r.checked).length == 0) return setErrors({ roles: ["Please select a role"]})
                
        var data = new FormData()
        data.append('designation', designation)
        data.append('username', username)
        data.append('password', password)
        data.append('password2', password)
        data.append('email', email)
        data.append('first_name', firstName)
        data.append('last_name', lastName)
        data.append('cnic', cnic)        
        data.append("permissions", JSON.stringify(permissions.filter(p => p.checked)))
        data.append("roles", JSON.stringify(roles.map(r => { if (r.checked) return r.role }).filter(role => role)))
        data.append("profilePicture", profile) 
        
        createUser(data)
    }

    return (
        <>
            {/* <ApiResponseModal message={message} /> */}
            <form action="" onSubmit={submitFormData} className="w-100 bg-white border-radius__5 pt-4 mt-3 mb-5">
                <h6 className="border-bottom px-3 pb-2 font-opensans-bold text__grey4">CREATE NEW USER</h6>
                <div className="container px-0 ml-0 my-4">
                    <div className="row mx-0">
                        <div className="col-3">
                            <div className="upload-btn-wrapper p-2">
                                <button type="button" className="btn bg-white btn__icon__lg bg__light4 ml-3 p-0 box-shadow__light overflow__hidden">
                                    { preview ? <img src={preview} width="100%" /> : <img src="/assets/img/icons/user_light.svg" width="100px" /> }
                                </button>                            
                                <input onChange={showPreview} type="file" name="myfile" accept="image/*" />
                                <p className="small text-center mt-3 text__grey4 font-opensans-bo">Upload Photo</p>
                                {errors && errors.profilePicture ? 
                                    <small className="ml-2 text-center form-text text__danger">{errors.profilePicture[0]}</small>  
                                : null}                            
                            </div>                                 
                        </div>
                        <div className="col-8 mt-5">
                            <div className="row mx-0 align-items-center mb-3">
                                <p className="col-lg-3 mb-0 text__dark3">Designation</p>
                                <div className="col-lg-6 px-0">
                                    <div className="input-group">
                                        <input 
                                            type="text" 
                                            value={designation} 
                                            onChange={(e) => setDesignation(e.target.value)}
                                            required
                                            className="form-control border__light3 border-radius__12 py-2 px-3" placeholder="Designation"/>
                                    </div>                                      
                                </div>
                                {errors && errors.designation ? 
                                    <div className="row mx-0 w-100">
                                        <div className="col-3"></div>
                                        <div className="col-6 px-0">
                                            <small className="ml-2 form-text text__danger">{errors.designation[0]}</small>  
                                        </div>
                                    </div>                                    
                                : null}
                            </div>
                            <div className="row mx-0 align-items-center mb-3">
                                <p className="col-lg-3 mb-0 text__dark3">Username</p>
                                <div className="col-lg-6 px-0">
                                    <div className="input-group">
                                        <input 
                                            type="text" 
                                            value={username} 
                                            onChange={(e) => setUsername(e.target.value)}
                                            required
                                            className="form-control border__light3 border-radius__12 py-2 px-3" placeholder="Username"/>
                                    </div>                                      
                                </div>
                                {errors && errors.username ? 
                                    <div className="row mx-0 w-100">
                                        <div className="col-3"></div>
                                        <div className="col-6 px-0">
                                            <small className="ml-2 form-text text__danger">{errors.username[0]}</small>  
                                        </div>
                                    </div>                                    
                                : null}
                            </div>
                            <div className="row mx-0 align-items-center mb-3">
                                <p className="col-lg-3 mb-0 text__dark3">Full Name</p>
                                <div className="col-lg-3 pl-0">
                                    <div className="input-group">
                                        <input 
                                            type="text" 
                                            value={firstName} 
                                            onChange={(e) => setFirstName(e.target.value)}
                                            required
                                            className="form-control border__light3 border-radius__12 py-2 px-3" placeholder="First Name"/>
                                    </div>                                      
                                </div>
                                <div className="col-lg-3 pl-1 pr-0">
                                    <div className="input-group">
                                        <input 
                                            type="text" 
                                            value={lastName} 
                                            onChange={(e) => setLastName(e.target.value)}
                                            required
                                            className="form-control border__light3 border-radius__12 py-2 px-3" placeholder="Last Name"/>
                                    </div>                                      
                                </div>
                                {errors && (errors.first_name ||  errors.last_name) ? 
                                    <div className="row mx-0 w-100">
                                        <div className="col-3"></div>
                                        <div className="col-6 px-0">
                                            <small className="ml-2 form-text text__danger">{errors.first_name[0] ||  errors.last_name[0]}</small>  
                                        </div>
                                    </div>                                    
                                : null}
                            </div>
                            <div className="row mx-0 align-items-center mb-3">
                                <p className="col-lg-3 mb-0 text__dark3">Roles</p>
                                <div className="col-lg-6 px-0 position-relative">
                                    <RoleSuggestionDropdown roles={roles} />     
                                    {errors && errors.roles ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.roles[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}

                                </div>
                            </div> 
                            <div className="row mx-0 align-items-center mb-3">
                                <p className="col-lg-3 mb-0 text__dark3">Email</p>
                                <div className="col-lg-6 px-0">
                                    <div className="input-group">
                                        <input 
                                            value={email} 
                                            onChange={(e) => setEmail(e.target.value)}
                                            required
                                            type="email" className="form-control border__light3 border-radius__12 py-2 px-3" placeholder="Email Address"/>
                                    </div>                                        
                                </div>
                                {errors && errors.email ? 
                                    <div className="row mx-0 w-100">
                                        <div className="col-3"></div>
                                        <div className="col-6 px-0">
                                            <small className="ml-2 form-text text__danger">{errors.email[0]}</small>  
                                        </div>
                                    </div>                                    
                                : null}
                            </div>
                            <div className="row mx-0 align-items-center mb-3">
                                <p className="col-lg-3 mb-0 text__dark3">CNIC</p>
                                <div className="col-lg-6 px-0">
                                    <div className="input-group">
                                        <input 
                                            value={cnic} 
                                            required
                                            // onChange={(e) => {
                                            //     e.target.value.length == 5 || e.target.value.length == 13 ? setCnic(e.target.value + "-") : setCnic(e.target.value)
                                                
                                            // }}
                                            onChange={(e) =>
                                                {
                                                    if (e.target.value.length>13){

                                                    }
                                                    else{
                                                        const result = event.target.value.replace(/\D/g, '');
                                                        setCnic(result);
                                                    }
                                                }
                                                }
                                            type="text" className="form-control border__light3 border-radius__12 py-2 px-3" placeholder="CNIC"
                                            minLength={13}
                                            maxLength={13}/>
                                    </div>                                        
                                </div>
                                {errors && errors.cnic ? 
                                    <div className="row mx-0 w-100">
                                        <div className="col-3"></div>
                                        <div className="col-6 px-0">
                                            <small className="ml-2 form-text text__danger">{errors.cnic[0]}</small>  
                                        </div>
                                    </div>                                    
                                : null}
                            </div>
                            <div className="row mx-0 align-items-center mb-3 d-none">
                                <p className="col-lg-3 mb-0 text__dark3">Password</p>
                                <div className="col-lg-6 px-0">
                                    <div className="input-group border__light3 border-radius__12 justify-content-between flex-nowrap">
                                        <input 
                                            value={password} 
                                            onChange={(e) => setPassword(e.target.value)}
                                            type={passwordHidden ? "password" : "text"} 
                                            className="control py-2 px-3 border-0 border-radius__12 w-100" placeholder="Password"/>
                                            <div className="input-group-append">
                                                <span
                                                    onClick={() => setPasswordHidden(!passwordHidden)} 
                                                    className={"btn p-0 input-group-text bg-transparent border-0 px-3 mdi " + (passwordHidden ? "mdi-eye-off-outline" : "mdi-eye-outline")}
                                                ></span>
                                            </div>
                                    </div>                                        
                                </div>
                                {errors && errors.password ? 
                                    <div className="row mx-0 w-100">
                                        <div className="col-3"></div>
                                        <div className="col-6 px-0">
                                            <small className="ml-2 form-text text__danger">{errors.password[0]}</small>  
                                        </div>
                                    </div>                                    
                                : null}
                            </div>

                            <div className="row mx-0 mb-3 mt-4">
                                <p className="col-lg-3 mb-0 text__dark3">Permissions</p>
                                <div className="col-lg-6 px-0 overflow__auto"  style={{ maxHeight: "220px" }}>
                                    <PermissionAssociatedWithRole />                                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="w-100 text-right px-4 py-3 mt-5 box-shadow__inset__light">
                    {/* <Link href="/user-management">
                        <a className="btn px-4 py-2 border-radius__15 bg-transparent text-dark border mx-2">
                            <span className="small px-2">Cancel</span>
                        </a>
                    </Link>   */}
                    <button 
                        disabled={loading}
                        className="btn px-4 py-2 border-radius__15 bg__green text-white mx-2">
                        {loading ? 
                            <span className="mdi mdi-loading mdi-spin"></span>
                        : null}
                        <span className="small px-2">Create user</span>
                    </button>

                </div>
            </form>   
        </>      
    )
}


const mapStateToProps = state => {
    return { 
        permissions: state.user.permissions,
        roles: state.user.roles,
        errors: state.site.errors,
        loading: state.site.loading,
        message: state.site.msg
    }
}

const mapDispatchToProps = {
    createUser, getRoles, setUser, setErrors
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateNewUserForm)