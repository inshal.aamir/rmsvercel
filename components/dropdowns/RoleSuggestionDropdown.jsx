import React, { useEffect, useState } from 'react'
import { updateObjectInArray } from '../../js/helpers'

import { connect } from "react-redux"
import { setRoles } from '../../redux/actions/user'
import $ from 'jquery'
import { v4 as uuidv4 } from 'uuid'

const RoleSuggestionDropdown = ({ disabled = false, roles, setRoles }) => {
    const [showSuggestions, setShowSuggestions] = useState(false)

    return (
        <>
            <div 
                className={"border border__light3 transition__3 " + (showSuggestions ? "box-shadow__dark2" : "") }
                style={{ borderRadius: showSuggestions ? "12px 12px 0px 0px" : "12px" }}>
                <button 
                    onClick={() => (roles.filter(d => d.checked).length == 0 || !showSuggestions) && !disabled ? setShowSuggestions(!showSuggestions) : null}
                    type="button" 
                    disabled={disabled}
                    className={ "btn w-100 text-left bg-transparent pr-0 transition__3 py-2 " + (roles.filter(d => d.checked).length == 0 ? " pl-3 " : " pl-2 ") + (disabled ? "py-2" : "py-1") }>
                    <span className="d-flex align-items-center justify-content-between">
                        <span>
                            {
                                roles.filter(d => d.checked).length == 0 
                                ? "Select"
                                // user.role.map((role, key_) =>
                                //   (key_ < 2 && user.role.length == 2) || key_ == 0 ? (
                                //     <p
                                //       key={key_ + "designation"}
                                //       className="d-inline-block bg__green3 text__green2 mr-2 px-3 mb-0 font-weight-bold small border-radius__24 py-1"
                                //     >
                                //       {role.role}
                                //     </p>
                                //   ) : key_ == 1 && user.role.length > 2 ? (
                                //     <button className="btn p-0 text__green2 bg-transparent">
                                //       <span className="small font-weight-bold">
                                //         +{user.role.length - 1} more
                                //       </span>
                                //     </button>
                                //   ) : null
                                // )
                                : roles.filter(d => d.checked).map((r, key) => (
                                    (key < 2 && roles.filter(r => r.checked).length == 2) || key < 1 ? (
                                        <span
                                            onClick={() => ( !disabled ?
                                                setRoles(
                                                    updateObjectInArray({ 
                                                        arr: roles, 
                                                        obj: {...r, checked: false},
                                                        i: key,
                                                        prop: "role"
                                                    })
                                                ) : null
                                            )} key={key} className=" bg__green3 text__green2 mr-2 pl-3 mb-0 small border-radius__24 py__6px">{ r.role }
                                            <span className="mdi mdi-close pr-2 pl-2 text-dark"></span>
                                        </span>
                                    ) : key == 1 && roles.filter(r => r.checked).length > 2 ? (
                                        <span className="small font-opensans-semibold ml-2">
                                            +{roles.filter(r => r.checked).length - 1} more
                                        </span>
                                    ) : null
                                ))
                            }
                        </span>
                        { disabled ? <span></span> :
                            <span
                                onClick={() => setShowSuggestions(!showSuggestions)} 
                                className="mdi mdi-chevron-down mdi-20px pl-4 pr-2"></span>                        
                        }
                    </span>
                </button>
            </div>
            <div 
                onMouseLeave={() => setShowSuggestions(false)}
                className={"position-absolute bg-white w-100 zindex__2000 box-shadow__dark2 overflow__hidden " + (showSuggestions ? "border-left border-right border-bottom" : null )} 
                style={{ borderRadius: "0px 0px 12px 12px", maxHeight: showSuggestions ? "999px" : "0px", height: "auto" }}>
                <div className="input-group justify-content-between flex-nowrap d-none">
                    <input 
                        type="text"
                        className="form-control py-2 my-1 px-3 border-0 w-100 small border-radius__15 bg-transparent" placeholder="Search"/>
                        <div className="input-group-append">
                            <span className={"p-0 input-group-text bg-transparent border-0 pr-3"}><img src="/assets/img/icons/search.svg" width={"17px"} /></span>
                        </div>
                </div>                                
                {/*  border-top */}
                <div className="px-4 pt-3 pb-1 mb-1"> 
                    {
                        roles && roles.map((role, key) => (
                            <div key={key} className="mb-3">
                                <input
                                    onChange={(e) => (
                                        setRoles(
                                            updateObjectInArray({ 
                                                arr: roles, 
                                                obj: {...role, checked: e.target.checked},
                                                i: key,
                                                prop: "role"
                                            })
                                        )
                                    )} 
                                    type="checkbox" id={`usercreate__${key}`} name="radio-group" checked={role.checked} />                
                                <label htmlFor={`usercreate__${key}`} className="text-capitalize">
                                    <span></span>{role.role}
                                </label>
                            </div>    
                        ))                                                    
                    }                                                                   
                </div>
            </div>                                  
        </>
    )
}

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = {
    setRoles
}

export default connect(mapStateToProps, mapDispatchToProps)(RoleSuggestionDropdown)