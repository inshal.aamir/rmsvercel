import React, { useState } from 'react'

export default function ResourceAllocationDetailTable() {
    const [list, setList] = useState([
        { id: "001", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "002", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "003", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "004", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "005", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "006", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "007", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "008", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
    ])
    return (
        <table className="w-100 small mt-4">
            <thead className="bg__light1"> 
                <tr>
                    <th className="py-2 text-center pl-4">ID</th>
                    <th className="py-2 text-center">Control Unit</th>
                    <th className="py-2 text-center">Ballot Unit</th>
                    <th className="py-2 text-center">Printers</th>
                    <th className="py-2 text-center">Printer Rolls</th>
                    <th className="py-2 text-center">Data Cables</th>
                    <th className="py-2 text-center">Battery Cover</th>
                    <th className="py-2 px-4 text-center">Battery Charger</th>                            
                </tr>
            </thead>
            <tbody>
                {list && list.map((item, key) => (
                <tr 
                        key={key}
                        className="">
                        <td className="py__10px text-center pl-4">{item.id}</td>
                        <td className="py__10px text-center">{item.controlUnit}</td>
                        <td className="py__10px text-center">{item.balloutUnit}</td>
                        <td className="py__10px text-center">{item.printers}</td>
                        <td className="py__10px text-center">{item.printerRolls}</td>
                        <td className="py__10px text-center">{item.dataCables}</td>
                        <td className="py__10px text-center">{item.batteryCover}</td>
                        <td className="py__10px text-center">{item.batteryCharger}</td>
                    </tr>
                ))}
                <tr>
                    <td colSpan={10} className="py-4 my-4 text-right px-5">
                    </td>
                </tr>
            </tbody>
        </table>   
    )
}
