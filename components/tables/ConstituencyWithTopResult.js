import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react'; 
import { connect } from "react-redux"
import { getConstituenciesByProvince } from '../../redux/actions/constituency' 
import ProvincesSelectOptions from '../select/ProvincesSelectOptions';
import { numberWithCommas } from '../../js/helpers.js'

const ConstituencyWithTopResult = (props) => {

    const { constituencies, getConstituenciesByProvince } = props,
        [loading, setLoading] = useState(true),
        [localCon, setLocalCon] = useState([]),
        [filter, setFilter] = useState('')
        const router = useRouter(),
        { assembly, province = assembly == "provincial" ? "balochistan" : null } = router.query
 
    useEffect(() => { getConstituenciesByProvince(province)
    console.log(constituencies) 
    setLocalCon(constituencies)
    }, [province, assembly])
    // useEffect(() => constituencies && constituencies.length > 0 ? setLoading(false) : null, [constituencies])

    return (
        <>
            <div className="d-flex align-items-center justify-content-between pr-4 mb-4">
                <div className="d-flex w-100 align-items-center">
                    <h5 className="font-opensans-semibold pl-4 mr-4 mb-0 text-capitalize">{assembly} Assembly Election Results</h5>
                    { assembly == "provincial" ? <ProvincesSelectOptions /> : null }            
                </div>
                <div className="input-group bg-white border-radius__5 border__light mx-2" style={{ width: "425px"}}>

                    <input style={{borderRadius: "0px 35px 35px 0px"}} 
                    type="text" 
                    className="form-control border-0 bg-transparent py-2" 
                    placeholder="Search Consistuency / Candidate Name" 
                    aria-label="Username" 
                    aria-describedby="basic-addon1" 
                    onChange={event => setFilter(event.target.value)}
                    />

                    <div className="input-group-append border-0">
                        <span  
                            className="input-group-text bg-transparent pr-3 pl-1 border-0">
                                <img src="/assets/img/icons/search.svg" width="16px" />
                            </span>
                    </div>
                </div> 
            </div>
            
            {/* BELOW IS TABLE... */}
            <div>
                <div className="row mx-0 border-bottom mt-1">
                    <div className="col-lg-2 text-center">
                        <p className="font-opensans-semibold my-0 py-3">Constituency</p>
                    </div>
                    <div className="col-lg-10">
                        <div className="row mx-0">
                            <p className="col-lg-4 font-opensans-semibold my-0 py-3">Candidate  { loading.toString() }</p>
                            <p className="col-lg-4 font-opensans-semibold my-0 py-3">Party Affiliation</p>
                            <p className="col-lg-4 font-opensans-semibold my-0 py-3 text-center">Votes Polled</p>
                        </div>
                    </div>
                </div>
                {constituencies.length == 0 ?
                    <p className="d-flex p-5 text-center w-100 align-items-center justify-content-center my-5">
                        <span className="mdi mdi-loading mdi-spin mdi-24px text__green mr-3"></span>
                        <span className="text-muted">Please wait, Loading...</span>
                    </p>                             
                : constituencies && constituencies.length > 0 ? 
                constituencies.filter(f=> f.name.toLowerCase().includes(filter.toLocaleLowerCase())|| f.winner.name.toLowerCase().includes(filter.toLocaleLowerCase()) || filter==='').map((constituency, key) => (
                    <div key={key} className={"row mx-0 border-bottom py-1"}>
                        <div className="col-lg-2 text-center">
                            <p className="font-opensans-semibold text__green my-0 py-2">
                                <Link href={`/election-results/party-position/${assembly}/${constituency.name.toLowerCase()}`}>
                                    <a className="font-opensans-semibold text__green">
                                        {constituency.name}
                                    </a>
                                </Link>                                                 
                            </p>
                        </div>

                        <div className="col-lg-10">
                            <div className="row mx-0">
                                <p className="col-lg-4 my-0 py-2">
                                    <Link href={`/election-results/party-position/${assembly}/${constituency.name.toLowerCase()}`}>
                                        <a className="text-dark">{constituency.winner && constituency.winner.name ? constituency.winner.name : "Undefined" }</a>
                                    </Link>       
                                </p>
                                <p className="col-lg-4 my-0 py-2">
                                    <img
                                        style={{ border: `1px solid ${constituency.winner ? constituency.winner.party.color : "transparent"}`}} 
                                        src="/assets/img/icons/kitaab.svg" width="25px" className={"mr-2 border-radius__5 py__2px bw__2 px__3px bg-white "} />
                                    {constituency.winner && constituency.winner.party ? constituency.winner.party.name : "Undefined"}
                                </p>
                                <p className="col-lg-4 my-0 py-2 text-center">{constituency.winner && numberWithCommas(constituency.winner.votes) }</p>
                            </div>
                        </div>
                    </div>     
                                    
                ))  
                : 
                    <p className="p-5 text-center w-100 my-5">
                        <span className="text-muted">No Constituency Found!</span>
                    </p>                     
                }
                <div className="row mx-0 justify-content-end d-none">
                    <div className="col-lg-6 justify-content-end pt-3 px-4 row mx-0 align-items-center">
                        <p className="mr-4 mb-0 text-muted">558-769 of 1240</p>                        
                        <Link href={"/"}> 
                            <a className="mdi mdi-chevron-left mdi-24px text-muted mr-2 p-0 bg-transparent"></a>                              
                        </Link>
                        <Link href={"/"}>
                            <a className="mdi mdi-chevron-right mdi-24px text-muted p-0 bg-transparent"></a>                    
                        </Link>
                    </div>
                </div>
            </div>         
        
        </>


    );
}

const mapStateToProps = state => {
    return { 
        constituencies: state.constituency.list 
    }
}

const mapDispatchToProps = {
    getConstituenciesByProvince
}
export default connect(mapStateToProps, mapDispatchToProps)(ConstituencyWithTopResult)