import Link from 'next/link';
import React, { useState } from 'react';
import { numberWithCommas } from '../../js/helpers';

export default function PollingSchemeTable() {
    const [pollingScheme] = useState([
        { constituencyNo: "NA-52", constituencyName: "Islamabad-I", maleVoters: numberWithCommas("8193957"), femaleVoters: numberWithCommas("8193957"), totalVoters: numberWithCommas("8193957"), malePollingStations: numberWithCommas("4024"), femalePollingStations: numberWithCommas("4342"), combinedPollingStations: numberWithCommas("4563"), totalPollingStations: numberWithCommas("12064"), malePollingBooths: numberWithCommas("8193957"), femalePollingBooths: numberWithCommas("8193957"), totalPollingBooths: numberWithCommas("8193957") },
        { constituencyNo: "NA-53", constituencyName: "Islamabad-II", maleVoters: numberWithCommas("8193957"), femaleVoters: numberWithCommas("8193957"), totalVoters: numberWithCommas("8193957"), malePollingStations: numberWithCommas("4024"), femalePollingStations: numberWithCommas("4342"), combinedPollingStations: numberWithCommas("4563"), totalPollingStations: numberWithCommas("12064"), malePollingBooths: numberWithCommas("8193957"), femalePollingBooths: numberWithCommas("8193957"), totalPollingBooths: numberWithCommas("8193957") },
        { constituencyNo: "NA-54", constituencyName: "Islamabad-III", maleVoters: numberWithCommas("8193957"), femaleVoters: numberWithCommas("8193957"), totalVoters: numberWithCommas("8193957"), malePollingStations: numberWithCommas("4024"), femalePollingStations: numberWithCommas("4342"), combinedPollingStations: numberWithCommas("4563"), totalPollingStations: numberWithCommas("12064"), malePollingBooths: numberWithCommas("8193957"), femalePollingBooths: numberWithCommas("8193957"), totalPollingBooths: numberWithCommas("8193957") },
       ]),
        [tabs] = useState([
            { name: "National" },
            { name: "Balochistan" },
            { name: "FATA" },
            { name: "Islamabad" },
            { name: "KPK" },
            { name: "Punjab" },
            { name: "Sindh" },
        ])
    return ( 
        <div className="bg-white border-radius__5 my-4 small pb-3 pt-1">
            <div className="row mx-0 border-bottom my-1">
                <h6 className="col-lg-4 font-opensans-semibold px-3 py-2 my-0">Polling Scheme – Federal Capital </h6>                
                <div className="col-lg-8 px-0 mt-1 text-right">  
                    {tabs && tabs.map((tab, key) => (
                        <button key={key} className="bg-transparent btn pt-0 pb-1 px-4 border-bottom h-100">
                            <span className="small text-dark font-opensans-regular">{tab.name}</span>
                        </button>
                    ))}          
                </div>
            </div>
            <div className="row mx-0 text-center mt-2">
                <div className="col-lg-4 px-0">
                    <div className="row mx-0">
                        <p className="col-lg-2 font-opensans-semibold my-0 py-3 px-0">Sr#</p>
                        <p className="col-lg-5 font-opensans-semibold my-0 py-3 px-0">Constituency No.</p>
                        <p className="col-lg-5 font-opensans-semibold my-0 py-3 px-0">Constituency Name</p>
                    </div>
                </div>
                <div className="col-lg-8 px-0">
                    <div className="row mx-0">
                        <p className="col-lg-4 font-opensans-semibold my-0 py-3 px-0">No of Voters</p>
                        <p className="col-lg-4 font-opensans-semibold my-0 py-3 px-0">No of Polling Stations</p>
                        <p className="col-lg-4 font-opensans-semibold my-0 py-3 px-0">No of Polling Booths</p>
                    </div>
                </div>
            </div>

            <div className="row mx-0 text-center">
                <div className="col-lg-4 px-0">
                    <div className="row mx-0 bg__light5">
                        <p className="col-lg-2 font-opensans-semibold my-0 py-3 px-0 border-right"></p>
                        <p className="col-lg-5 font-opensans-semibold my-0 py-3 px-0 border-right"></p>
                        <p className="col-lg-5 font-opensans-semibold my-0 py-3 px-0 text-white border-right">.</p>
                    </div>
                </div>
                <div className="col-lg-8 px-0">
                    <div className="row mx-0">
                        <div className="col-lg-4 px-0">
                            <div className="row mx-0 bg__light5 border-right">
                                <p className="col-lg-4 font-opensans-semibold my-0 py-3 px-0">Male</p>
                                <p className="col-lg-4 font-opensans-semibold my-0 py-3 px-0">Female</p>
                                <p className="col-lg-4 font-opensans-semibold my-0 py-3 px-0">Total</p>
                            </div>
                        </div>
                        <div className="col-lg-4 px-0 border-right">
                            <div className="row mx-0 bg__light5">
                                <p className="col-lg-3 font-opensans-semibold my-0 py-3 px-0">Male</p>
                                <p className="col-lg-3 font-opensans-semibold my-0 py-3 px-0">Female</p>
                                <p className="col-lg-3 font-opensans-semibold my-0 py-3 px-0">Combined</p>
                                <p className="col-lg-3 font-opensans-semibold my-0 py-3 px-0">Total</p>
                            </div>
                        </div>
                        <div className="col-lg-4 px-0">
                            <div className="row mx-0 bg__light5">
                                <p className="col-lg-4 font-opensans-semibold my-0 py-3 px-0">Male</p>
                                <p className="col-lg-4 font-opensans-semibold my-0 py-3 px-0">Female</p>
                                <p className="col-lg-4 font-opensans-semibold my-0 py-3 px-0">Total</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            {pollingScheme && pollingScheme.map((record, key) => (

                <div key={key} 
                    className={"row mx-0 text-center " + (key%2!=0 ? "bg__light1" : null)}>
                    <div className="col-lg-4 px-0">
                        <div className="row mx-0">
                            <p className="col-lg-2 my-0 py-3 px-0 border-right">{key+1}</p>
                            <p className="col-lg-5 my-0 py-3 px-0 border-right">{record.constituencyNo}</p>
                            <p className="col-lg-5 my-0 py-3 px-0 border-right">{record.constituencyName}</p>
                        </div>
                    </div>
                    <div className="col-lg-8 px-0">
                        <div className="row mx-0">
                            <div className="col-lg-4 px-0 border-right">
                                <div className="row mx-0">
                                    <p className="col-lg-4 my-0 py-3 px-0">{record.maleVoters}</p>
                                    <p className="col-lg-4 my-0 py-3 px-0">{record.femaleVoters}</p>
                                    <p className="col-lg-4 my-0 py-3 px-0">{record.totalVoters}</p>
                                </div>
                            </div>
                            <div className="col-lg-4 px-0 border-right">
                                <div className="row mx-0">
                                    <p className="col-lg-3 my-0 py-3 px-0">{record.malePollingStations}</p>
                                    <p className="col-lg-3 my-0 py-3 px-0">{record.femalePollingStations}</p>
                                    <p className="col-lg-3 my-0 py-3 px-0">{record.combinedPollingStations}</p>
                                    <p className="col-lg-3 my-0 py-3 px-0">{record.totalPollingStations}</p>
                                </div>
                            </div>
                            <div className="col-lg-4 px-0">
                                <div className="row mx-0">
                                    <p className="col-lg-4 my-0 py-3 px-0">{record.malePollingBooths}</p>
                                    <p className="col-lg-4 my-0 py-3 px-0">{record.femalePollingBooths}</p>
                                    <p className="col-lg-4 my-0 py-3 px-0">{record.totalPollingBooths}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                 
            ))}
        </div>
    );
}
