import Link from 'next/link';
import React, { useState } from 'react';

export default function InventoryTable() {

    const [list, setList] = useState([
        { sr: "1", balloutUnit: "BU-001", controlUnit: "BU-001", printers: "P-001", batteryUnit: "B-001", batteryCover: "Conver-001", charger: "Cover-001", province: { name: "Punjab", color: "green" } },
        { sr: "2", balloutUnit: "CU-001", controlUnit: "BU-001", printers: "P-001", batteryUnit: "PR-001", batteryCover: "D-001", charger: "Cover-002", province: { name: "Punjab", color: "green" } },
        { sr: "3", balloutUnit: "CU-001", controlUnit: "BU-001", printers: "P-001", batteryUnit: "PR-001", batteryCover: "D-001", charger: "Cover-003", province: { name: "Balochistan", color: "blue" } },
        { sr: "4", balloutUnit: "CU-001", controlUnit: "BU-001", printers: "P-001", batteryUnit: "PR-001", batteryCover: "D-001", charger: "Cover-001", province: { name: "Balochistan", color: "blue" } },
        { sr: "5", balloutUnit: "CU-001", controlUnit: "BU-001", printers: "P-001", batteryUnit: "PR-001", batteryCover: "D-001", charger: "Cover-002", province: { name: "Punjab", color: "green" } },
        { sr: "6", balloutUnit: "CU-001", controlUnit: "BU-001", printers: "P-001", batteryUnit: "PR-001", batteryCover: "D-001", charger: "Cover-004", province: { name: "Balochistan", color: "blue" } },
        { sr: "7", balloutUnit: "CU-001", controlUnit: "BU-001", printers: "P-001", batteryUnit: "PR-001", batteryCover: "D-001", charger: "Cover-005", province: { name: "Punjab", color: "green" } },
        { sr: "8", balloutUnit: "CU-001", controlUnit: "BU-001", printers: "P-001", batteryUnit: "PR-001", batteryCover: "D-001", charger: "Cover-001", province: { name: "Balochistan", color: "blue" } },
        { sr: "9", balloutUnit: "CU-001", controlUnit: "BU-001", printers: "P-001", batteryUnit: "PR-001", batteryCover: "D-001", charger: "Cover-002", province: { name: "Balochistan", color: "blue" } },
        { sr: "10", balloutUnit: "CU-001", controlUnit: "BU-001", printers: "P-001", batteryUnit: "PR-001", batteryCover: "D-001", charger: "Cover-003", province: { name: "Punjab", color: "green" } },
    ])
    return ( 
        <table className="w-100 mt-4">
            <thead> 
                <tr>
                    <th className="py-2 text-center pl-4">Sr#</th>
                    <th className="py-2 text-center">Ballot Unit</th>
                    <th className="py-2 text-center">Control Unit</th>
                    <th className="py-2 text-center">Printer</th>
                    <th className="py-2 text-center">Battery Unit</th>
                    <th className="py-2 text-center">Battery Cover</th>
                    <th className="py-2 text-center">Charger</th>
                    <th className="py-2 px-4 text-center">province</th>                            
                    <th className="py-2 px-4 text-center"></th>                            
                </tr>
            </thead>
            <tbody>
                {list && list.map((item, key) => (
                <tr 
                        key={key}
                        className={key%2 == 0 ? "bg__light1" : null}>
                        <td className="py__5px text-center pl-4">{item.sr}</td>
                        <td className="py__5px text-center">{item.balloutUnit}</td>
                        <td className="py__5px text-center">{item.controlUnit}</td>
                        <td className="py__5px text-center">{item.printers}</td>
                        <td className="py__5px text-center">{item.batteryUnit}</td>
                        <td className="py__5px text-center">{item.batteryCover}</td>
                        <td className="py__5px text-center">{item.charger}</td>
                        <td className={"py__5px text-center text__" + item.province.color}>{item.province.name}</td>                                        
                        <td className="py__5px px-4 d-flex justify-content-center">
                            <button type="button" className="btn bg__green mx-1 btn__icon__sm d-flex align-items-center justify-content-center">
                                <img src="/assets/img/icons/edit.svg" width="10px" />
                            </button>  
                        </td>
                    </tr>
                ))}
                <tr>
                    <td colSpan={10} className="py-2 my-4 text-right px-5">
                    </td>
                </tr>
            </tbody>
        </table>
    );
}
