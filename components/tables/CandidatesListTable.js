import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { numberWithCommas } from '../../js/helpers';
import PartyPositionBarChart from '../chartjs/bar-charts/PartyPositionBarChart';
import axios from 'axios'

// REDUX
import { connect } from "react-redux"
import { setPollingStationData } from '../../redux/actions/pollingstation'

const CandidatesListTable = (props) => {
    const { data, setPollingStationData } = props
    const [candidatesList, setCandidatesList] = useState([
        // { constituency: "NA-1", candidate: "Raja Khurram Shahzad Nawaz", partyAffiliation: "Muttahida Majlis-E-Amal Pakistan", votesPolled: "49035", icon: "/assets/img/icons/kitaab.svg", color: "yellow" },
        // { constituency: "NA-2", candidate: "Muhammad Afzal Khokhar", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        // { constituency: "NA-3", candidate: "Tariq Fazal Chaudhry", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        // { constituency: "NA-4", candidate: "Muhammad Afzal Khokhar", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        // { constituency: "NA-5", candidate: "Tariq Fazal Chaudhary", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        // { constituency: "NA-6", candidate: "Rizwan Abbasi", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        // { constituency: "NA-7", candidate: "Bilal Faisal Amin", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        // { constituency: "NA-8", candidate: "Hafiz Muhammad Aslam", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        // { constituency: "NA-9", candidate: "Syed Amjad Ali Shah", partyAffiliation: "Pakistan Muslim League (N)", votesPolled: "49035", icon: "/assets/img/icons/tiger.svg", color: "green" },
        // { constituency: "NA-10", candidate: "Asrar Ahmad Abassi", partyAffiliation: "Independent", votesPolled: "49035", },
        // { constituency: "NA-11", candidate: "Rizwan Abassi", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        // { constituency: "NA-12", candidate: "Muhammad Afzal Khokhar", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        // { constituency: "NA-13", candidate: "Tariq Fazal Chaudhary", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
    ]),
        [pageno, setPageno] = useState(1),
        [tableView, setTableView] = useState(true),
        [loading, setLoading] = useState(true),
        [filter, setFilter] = useState(''),
        router = useRouter(),
        { province, constituency } = router.query

    useEffect(() => {
        if (constituency) {
            let path = router.asPath.split("?")[1] || "page_number=1"
            setPageno(parseInt(path.split("=")[1]))
            var config = {
                method: 'get',
                url: `${process.env.BASE_URL}api/config/get/result/constituency/${constituency.toUpperCase()}`,
                headers: { 'Authorization': localStorage.getItem("access_token") },
            }
            
            axios(config)
            .then(function (response) {       
                console.log(response, "response")
                if (response.status == 200) {
                    setCandidatesList(response.data)
                    setLoading(false)
                } 
            })
            .catch((error) => console.log(error))            
        }
    }, [constituency])

    return (
        <div className="bg-white border-radius__5 small pt-4 pb-3 mb-5 h-100">
            
            <div className="d-flex align-items-center justify-content-between pr-4 mb-4">
                <div className="input-group bg-white border-radius__5 border__light mx-4" style={{ width: "425px"}}>
                    <input style={{borderRadius: "0px 35px 35px 0px"}} 
                    type="text" className="form-control border-0 bg-transparent py-2" 
                    placeholder="Search Candidate Name" 
                    aria-label="Username" 
                    aria-describedby="basic-addon1"
                    onChange={event => setFilter(event.target.value)} />

                    <div className="input-group-append border-0">
                        <span  
                            className="input-group-text bg-transparent pr-3 pl-1 border-0">
                                <img src="/assets/img/icons/search.svg" width="16px" />
                            </span>
                    </div>
                </div> 
                {constituency ? 
                    <button 
                        onClick={() => setTableView(!tableView)} 
                        className={"btn mdi border text__green mdi-24px bg-transparent border-radius__20 " + (tableView ? "mdi-chart-bar " : "mdi-format-list-bulleted")}></button>
                : null}
            </div>
            
            {tableView ? 
                <div>
                    <div className="row mx-0 border-bottom mt-1">
                        <div className="col-lg-4 pl-4">
                            <p className="font-opensans-semibold my-0 py-3">Candidate</p>
                        </div>
                        <div className={"col-lg-8"}>
                            <div className="row mx-0">
                                <p className="col-lg-8 font-opensans-semibold my-0 py-3">Party Affliation</p>
                                <p className="col-lg-4 text-center font-opensans-semibold my-0 py-3">Votes Polled</p>
                            </div>
                        </div>
                    </div>                            
                    {loading ?
                        <p className="d-flex p-5 text-center w-100 align-items-center justify-content-center my-5">
                            <span className="mdi mdi-loading mdi-spin mdi-24px text__green mr-3"></span>
                            <span className="text-muted">Please wait, Loading...</span>
                        </p>                             
                    : !loading && candidatesList.length > 0 ? 
                    <div className="overflow__auto" style={{ maxHeight: "85vh" }}>
                        {
                            candidatesList && candidatesList.filter(
                                f=> f.name.toLowerCase().includes(filter.toLowerCase()) || filter===''
                            ).map((record, key) => (
                                <div key={key} className={"row mx-0 border-bottom py-1"}>
                                    <div className="col-lg-4 pl-4">
                                        <p className="font-opensans-semibold my-0 py-2 bg-transparent">
                                            {record.name}
                                        </p>
                                    </div>

                                    <div className={"col-lg-8"}>
                                        <div className="row mx-0">
                                            <p className="col-lg-8 my-0 py-2" style={{ color: "black" }}>
                                                <img
                                                    style={{ border: `1px solid ${record.party.color}`}} 
                                                    src="/assets/img/icons/kitaab.svg" width="25px" className={"mr-2 bw__2 border-radius__5 py__2px px__3px bg-white "} />
                                                {record.party ? record.party.name : "Undefined"}
                                            </p>

                                            <p className="col-lg-4 text-center my-0 py-2">
                                                { numberWithCommas(record.votes) }
                                            </p>
                                        </div>
                                    </div>
                                </div>                   
                            ))
                        }
                    </div>
                    : 
                        <p className="p-5 text-center w-100 my-5">
                            <span className="text-muted small">No Candidates Found!</span>
                        </p>                     
                    }
                    {/* <div className="row mx-0 justify-content-end d-none">
                        <div className="col-lg-6 justify-content-end pt-3 px-4 row mx-0 align-items-center">
                            <p className="mr-4 mb-0 text-muted">{pageno}-769 of 1240</p>                        
                            <Link href={pageno > 1 ? `/election-results/party-position/${province}/${constituency}?page_number=${pageno-1}` : `/election-results/party-position/${province}/${constituency}` }> 
                                <a className="mdi mdi-chevron-left mdi-24px text-muted mr-2 p-0 bg-transparent"></a>                              
                            </Link>
                            <Link href={`/election-results/party-position/${province}/${constituency}?page_number=${pageno+1}`}>
                                <a className="mdi mdi-chevron-right mdi-24px text-muted p-0 bg-transparent"></a>                    
                            </Link>
                        </div>
                    </div> */}
                </div>  
            : <PartyPositionBarChart />
            }        


        </div>
    )
}

const mapStateToProps = state => {
    return { data: state.pollingStation.data }
}

const mapDispatchToProps = {
    setPollingStationData
}

export default connect(mapStateToProps, mapDispatchToProps)(CandidatesListTable)