import React, { useState } from 'react';
import { numberWithCommas } from '../../js/helpers';

export default function PollingSchemeSummaryTable() {
    const [summaryOfPollingScheme] = useState([
        { province: "KP", maleVoters: numberWithCommas("8193957"), femaleVoters: numberWithCommas("8193957"), totalVoters: numberWithCommas("8193957"), malePollingStations: numberWithCommas("4024"), femalePollingStations: numberWithCommas("4342"), combinedPollingStations: numberWithCommas("4563"), totalPollingStations: numberWithCommas("12064"), malePollingBooths: numberWithCommas("8193957"), femalePollingBooths: numberWithCommas("8193957"), totalPollingBooths: numberWithCommas("8193957") },
        { province: "FATA", maleVoters: numberWithCommas("8193957"), femaleVoters: numberWithCommas("8193957"), totalVoters: numberWithCommas("8193957"), malePollingStations: numberWithCommas("4024"), femalePollingStations: numberWithCommas("4342"), combinedPollingStations: numberWithCommas("4563"), totalPollingStations: numberWithCommas("12064"), malePollingBooths: numberWithCommas("8193957"), femalePollingBooths: numberWithCommas("8193957"), totalPollingBooths: numberWithCommas("8193957") },
        { province: "ICT", maleVoters: numberWithCommas("8193957"), femaleVoters: numberWithCommas("8193957"), totalVoters: numberWithCommas("8193957"), malePollingStations: numberWithCommas("4024"), femalePollingStations: numberWithCommas("4342"), combinedPollingStations: numberWithCommas("4563"), totalPollingStations: numberWithCommas("12064"), malePollingBooths: numberWithCommas("8193957"), femalePollingBooths: numberWithCommas("8193957"), totalPollingBooths: numberWithCommas("8193957") },
        { province: "Punjab", maleVoters: numberWithCommas("8193957"), femaleVoters: numberWithCommas("8193957"), totalVoters: numberWithCommas("8193957"), malePollingStations: numberWithCommas("4024"), femalePollingStations: numberWithCommas("4342"), combinedPollingStations: numberWithCommas("4563"), totalPollingStations: numberWithCommas("12064"), malePollingBooths: numberWithCommas("8193957"), femalePollingBooths: numberWithCommas("8193957"), totalPollingBooths: numberWithCommas("8193957") },
        { province: "Sindh", maleVoters: numberWithCommas("8193957"), femaleVoters: numberWithCommas("8193957"), totalVoters: numberWithCommas("8193957"), malePollingStations: numberWithCommas("4024"), femalePollingStations: numberWithCommas("4342"), combinedPollingStations: numberWithCommas("4563"), totalPollingStations: numberWithCommas("12064"), malePollingBooths: numberWithCommas("8193957"), femalePollingBooths: numberWithCommas("8193957"), totalPollingBooths: numberWithCommas("8193957") },
        { province: "Balochistan", maleVoters: numberWithCommas("8193957"), femaleVoters: numberWithCommas("8193957"), totalVoters: numberWithCommas("8193957"), malePollingStations: numberWithCommas("4024"), femalePollingStations: numberWithCommas("4342"), combinedPollingStations: numberWithCommas("4563"), totalPollingStations: numberWithCommas("12064"), malePollingBooths: numberWithCommas("8193957"), femalePollingBooths: numberWithCommas("8193957"), totalPollingBooths: numberWithCommas("8193957") },
    ])
    return (
        <div className="bg-white border-radius__5 my-4 small pb-3">
        <h6 className="font-opensans-semibold border-bottom px-3 py-3">Summary of Polling Scheme 2018</h6>
        <div className="row mx-0 text-center">
            <div className="col-lg-2"></div>
            <div className="col-lg-3">
                <p className="font-opensans-semibold my-0 py-3">No of Voters</p>
            </div>
            <div className="col-lg-4">
                <p className="font-opensans-semibold my-0 py-3">No of Polling Stations</p>
            </div>
            <div className="col-lg-3">
                <p className="font-opensans-semibold my-0 py-3">No of Polling Booths</p>
            </div>
        </div>

        <div className="row mx-0 text-center mb-2">
            <div className="col-lg-2">
                <p className="bg__light5 my-0 py-3 font-opensans-semibold">Provinces</p>
            </div>
            <div className="col-lg-3 pl-0">
                <div className="row mx-0">
                    <p className="col-lg-4 bg__light5 my-0 py-3 font-opensans-semibold">Male</p>
                    <p className="col-lg-4 bg__light5 my-0 py-3 font-opensans-semibold">Female</p>
                    <p className="col-lg-4 bg__light5 my-0 py-3 font-opensans-semibold">Total</p>                                
                </div>
            </div>
            <div className="col-lg-4 px-0">
                <div className="row mx-0">
                    <p className="col-lg-3 bg__light5 my-0 py-3 font-opensans-semibold">Male</p>
                    <p className="col-lg-3 bg__light5 my-0 py-3 font-opensans-semibold">Female</p>
                    <p className="col-lg-3 bg__light5 my-0 py-3 font-opensans-semibold">Combined</p>    
                    <p className="col-lg-3 bg__light5 my-0 py-3 font-opensans-semibold">Total</p>                                 
                </div>
            </div>
            <div className="col-lg-3">
                <div className="row mx-0">
                    <p className="col-lg-4 bg__light5 my-0 py-3 font-opensans-semibold">Male</p>
                    <p className="col-lg-4 bg__light5 my-0 py-3 font-opensans-semibold">Female</p>
                    <p className="col-lg-4 bg__light5 my-0 py-3 font-opensans-semibold">Total</p>                                
                </div>
            </div>
        </div>
        {summaryOfPollingScheme && summaryOfPollingScheme.map((record, key) => (
            <div key={key} 
                className={"row mx-0 text-center py-1 " + (key%2!=0 ? "bg__light1" : null)}>
                <div className="col-lg-2">
                    <p className="my-0 py-2 text__green font-opensans-semibold">{record.province}</p>
                </div>
                <div className="col-lg-3 pl-0">
                    <div className="row mx-0">
                        <p className="col-lg-4 my-0 py-2">{record.maleVoters}</p>
                        <p className="col-lg-4 my-0 py-2">{record.femaleVoters}</p>
                        <p className="col-lg-4 my-0 py-2">{record.totalVoters}</p>                                
                    </div>
                </div>
                <div className="col-lg-4 px-0">
                    <div className="row mx-0">
                        <p className="col-lg-3 my-0 py-2">{record.malePollingStations}</p>
                        <p className="col-lg-3 my-0 py-2">{record.femalePollingStations}</p>
                        <p className="col-lg-3 my-0 py-2">{record.combinedPollingStations}</p>    
                        <p className="col-lg-3 my-0 py-2">{record.totalPollingStations}</p>                                 
                    </div>
                </div>
                <div className="col-lg-3">
                    <div className="row mx-0">
                        <p className="col-lg-4 my-0 py-2">{record.malePollingBooths}</p>
                        <p className="col-lg-4 my-0 py-2">{record.femalePollingBooths}</p>
                        <p className="col-lg-4 my-0 py-2">{record.totalPollingBooths}</p>                                
                    </div>
                </div>
            </div>                    
        ))}
    </div>
    );
}
