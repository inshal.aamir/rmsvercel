import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import PartyPosition from '../../pages/election-results/party-position';
import PartyPositionBarChart from '../chartjs/bar-charts/PartyPositionBarChart';
import axios from 'axios'

export default function PartyPositionMayorTableTemplate() {
    const [summaryOfPollingScheme] = useState([
        { constituency: "NA-1", candidate: "Raja Khurram Shahzad Nawaz", partyAffiliation: "Muttahida Majlis-E-Amal Pakistan", votesPolled: "49035", icon: "/assets/img/icons/kitaab.svg", color: "yellow" },
        { constituency: "NA-2", candidate: "Muhammad Afzal Khokhar", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        { constituency: "NA-3", candidate: "Tariq Fazal Chaudhry", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        { constituency: "NA-4", candidate: "Muhammad Afzal Khokhar", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        { constituency: "NA-5", candidate: "Tariq Fazal Chaudhary", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        { constituency: "NA-6", candidate: "Rizwan Abbasi", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        { constituency: "NA-7", candidate: "Bilal Faisal Amin", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        { constituency: "NA-8", candidate: "Hafiz Muhammad Aslam", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        { constituency: "NA-9", candidate: "Syed Amjad Ali Shah", partyAffiliation: "Pakistan Muslim League (N)", votesPolled: "49035", icon: "/assets/img/icons/tiger.svg", color: "green" },
        { constituency: "NA-10", candidate: "Asrar Ahmad Abassi", partyAffiliation: "Independent", votesPolled: "49035", },
        { constituency: "NA-11", candidate: "Rizwan Abassi", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        { constituency: "NA-12", candidate: "Muhammad Afzal Khokhar", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
        { constituency: "NA-13", candidate: "Tariq Fazal Chaudhary", partyAffiliation: "Pakistan Tehreek-e-Insaf", votesPolled: "49035", icon: "/assets/img/icons/bat.svg", color: "red" },
    ]),
        [tableView, setTableView] = useState(true),
        router = useRouter(),
        { province, constituency } = router.query

    useEffect(() => {
        var config = {
            method: 'get',
            url: `${process.env.BASE_URL}api/config/get/constituency/province/balochistan`,
            headers: { 'Authorization': localStorage.getItem("access_token") },
        }
        
        axios(config)
        .then(function (response) {       
            if (response.status == 200) {
                console.log(console.log(response, "party postion mayor table"))
            } 
        })
        .catch(function (error) {
            console.log(error)
        })        
    }, [])

    return (
        <div className="bg-white border-radius__5 small pt-4 pb-3 mb-5">
        
        <div className="d-flex align-items-center justify-content-between pr-4 mb-4">
            <div className="input-group bg-white border-radius__5 border__light mx-4" style={{ width: "425px"}}>
                <input style={{borderRadius: "0px 35px 35px 0px"}} type="text" className="form-control border-0 bg-transparent py-2" placeholder="Search Consistuency / Candidate Name / Party" aria-label="Username" aria-describedby="basic-addon1" />
                <div className="input-group-append border-0">
                    <span  
                        className="input-group-text bg-transparent pr-3 pl-1 border-0">
                            <img src="/assets/img/icons/search.svg" width="16px" />
                        </span>
                </div>
            </div> 
            {constituency ? 
                <button 
                    onClick={() => setTableView(!tableView)} 
                    className={"btn mdi border text__green mdi-24px bg-transparent border-radius__20 " + (tableView ? "mdi-chart-bar " : "mdi-format-list-bulleted")}></button>
            : null}
        </div>
        
        {tableView ? 
            <div>
                <div className="row mx-0 border-bottom mt-1">
                    {!constituency ? 
                        <div className="col-lg-2 text-center">
                            <p className="font-opensans-semibold my-0 py-3">Constituency</p>
                        </div>
                    : null}
                    <div className={!constituency ? "col-lg-10" : "col-lg-12"}>
                        <div className="row mx-0">
                            <p className="col-lg-4 font-opensans-semibold my-0 py-3">Candidate</p>
                            <p className="col-lg-4 font-opensans-semibold my-0 py-3">Party Affiliation</p>
                            <p className="col-lg-4 font-opensans-semibold my-0 py-3 text-center">Votes Polled</p>
                        </div>
                    </div>
                </div>            
                {summaryOfPollingScheme && summaryOfPollingScheme.map((record, key) => (
                    <div key={key} className={"row mx-0 border-bottom py-1 " + (constituency && constituency == record.constituency.toLowerCase() ? "bg__green__opaque" : null)}>
                        {!constituency ?
                            <div className="col-lg-2 text-center">
                                <p className="font-opensans-semibold text__green my-0 py-2">
                                    <Link href={`/election-results/party-position/${province}/${record.constituency.toLowerCase()}`}>
                                        <a className="font-opensans-semibold text__green">
                                            {record.constituency}
                                        </a>
                                    </Link>                                                 
                                </p>
                            </div>
                        : null}

                        <div className={!constituency ? "col-lg-10" : "col-lg-12"}>
                            <div className="row mx-0">
                                <p className="col-lg-4 my-0 py-2">
                                    <Link href={`/election-results/party-position/${province}/${record.constituency.toLowerCase()}`}>
                                        <a className="text-dark">{record.candidate}</a>
                                    </Link>       
                                </p>
                                <p className="col-lg-4 my-0 py-2">
                                    {record.icon ? 
                                        <img src={record.icon} width="25px" className={"mr-2 border-radius__5 py__2px px__3px bg-white " + (`border__${record.color}`)} />
                                    : null}
                                    {record.partyAffiliation}
                                </p>
                                <p className="col-lg-4 my-0 py-2 text-center">{record.votesPolled}</p>
                            </div>
                        </div>
                    </div>                   
                ))}
            </div>  
        : <PartyPositionBarChart />
        }        


    </div>
    );
}
