import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useState } from 'react'

export default function InventoryAllocationTable() {
    const router = useRouter()
    const [list, setList] = useState([
        { province: "KPK", noOfPollingStations: "12064", noOfPollingBooths: "8193957", cuAllocated: "12064", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "green" } },
        { province: "FATA", noOfPollingStations: "1884", noOfPollingBooths: "1507901", cuAllocated: "1884", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "blue" } },
        { province: "ICT", noOfPollingStations: "31231", noOfPollingBooths: "407492", cuAllocated: "31231", buAllocated: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__orange mr-2 small"></span>`, printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "green" } },
        { province: "Punjab", noOfPollingStations: "1213", noOfPollingBooths: "9461156", cuAllocated: "1213", buAllocated: "12064", printers: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__red mr-2 small"></span>`, batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "green" } },
        { province: "Sindh", noOfPollingStations: "42432", noOfPollingBooths: "12378192", cuAllocated: "42432", buAllocated: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__red mr-2 small"></span>`, printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "green" } },
        { province: "Balochistan", noOfPollingStations: "12121", noOfPollingBooths: "2486091", cuAllocated: "12064", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "blue" } },
    ])
    return (
        <table className="w-100 mt-4 small">
            <thead> 
                <tr className="bg__light5">
                    <th className="py-2 text-center pl-4"></th>
                    <th className="py-2 text-center">No of Polling Stations</th>
                    <th className="py-2 text-center">No of Polling Booths</th>
                    <th className="py-2 text-center">CU Allocated</th>
                    <th className="py-2 text-center">BU Allocated</th>
                    <th className="py-2 text-center">Printers</th>
                    <th className="py-2 text-center">Battery Unit</th>
                    <th className="py-2 px-4 text-center">Charger</th>  
                    <th className="py-2 px-4 text-center">Status</th>                             
                    <th className="py-2 px-4 text-center">Actions</th>                            
                </tr>
            </thead>
            <tbody>
                {list && list.map((item, key) => (
                <tr 
                        key={key}
                        className="border-bottom">
                        <td className="py__5px text__green pl-5 font-opensans-semibold">{item.province}</td>
                        <td className="py__5px text-center pl-4" dangerouslySetInnerHTML={{ __html: item.noOfPollingStations}}></td>
                        <td className="py__5px text-center" dangerouslySetInnerHTML={{ __html: item.noOfPollingBooths}}></td>
                        <td className="py__5px text-center" dangerouslySetInnerHTML={{ __html: item.cuAllocated }}></td>
                        <td className="py__5px text-center" dangerouslySetInnerHTML={{ __html: item.buAllocated }}></td>
                        <td className="py__5px text-center" dangerouslySetInnerHTML={{ __html: item.printers }}></td>
                        <td className="py__5px text-center" dangerouslySetInnerHTML={{ __html: item.batteryUnit }}></td>
                        <td className="py__5px text-center" dangerouslySetInnerHTML={{ __html: item.charger }}></td>
                        <td className={"py__5px text-center font-opensans-semibold text__" + item.status.color}>{item.status.name}</td>  
                        <td className="py__5px px-4 d-flex justify-content-center align-items-center">
                            <Link href={`/inventory/allocate`}>
                                <a className="btn mx-1 p-0 bg-transparent text-dark">
                                    <span className="small font-opensans-regular">View</span>
                                </a>                                              
                            </Link>
                            <span className="text__grey3">|</span>
                            <Link href={`/inventory/allocate`}>
                                <a className="btn mx-1 p-0 bg-transparent text-dark" data-toggle="modal" data-target="#resourceAllocationModal">
                                    <span className="small font-opensans-regular">Allocate</span>
                                </a>                               
                            </Link>
                        </td>
                    </tr>
                ))}
                <tr>
                    <td colSpan={10} className="py-4 my-4 text-right px-5">
                    </td>
                </tr>
            </tbody>
        </table>
    )
}
