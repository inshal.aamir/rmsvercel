import React, { useState } from "react";
import { numberWithCommas } from "../../js/helpers";

export default function InventoryLogByProvinceDivision() {
  const [list, setList] = useState([
    {
      province:
        "<span class='mdi mdi-circle pr-1 small pt-1 text__yellow'></span> Control Unit",
      noOfPollingStations: "502004",
      noOfPollingBooths: "8193957",
      cuAllocated: "12064",
      buAllocated: "12064",
      printers: "12064",
      batteryUnit: "12064",
      charger: "12064",
      status: { name: "Completed", color: "green" },
    },
    {
      province:
        "<span class='mdi mdi-circle pr-1 small pt-1 text__orange'></span> Ballot Unit",
      noOfPollingStations: "1006300",
      noOfPollingBooths: "1507901",
      cuAllocated: "1884",
      buAllocated: "12064",
      printers: "12064",
      batteryUnit: "12064",
      charger: "12064",
      status: { name: "Pending", color: "blue" },
    },
    {
      province:
        "<span class='mdi mdi-circle pr-1 small pt-1 text__darkblue'></span> Printer Box",
      noOfPollingStations: "1020400",
      noOfPollingBooths: "407492",
      cuAllocated: "31231",
      buAllocated: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__orange mr-2 small"></span>`,
      printers: "12064",
      batteryUnit: "12064",
      charger: "12064",
      status: { name: "Completed", color: "green" },
    },
    // { province: "Printer roll", noOfPollingStations: "1213", noOfPollingBooths: "9461156", cuAllocated: "1213", buAllocated: "12064", printers: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__red mr-2 small"></span>`, batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "green" } },
    // { province: "Data Cables", noOfPollingStations: "42432", noOfPollingBooths: "12378192", cuAllocated: "42432", buAllocated: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__red mr-2 small"></span>`, printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "green" } },
    // { province: "Battery Base", noOfPollingStations: "12121", noOfPollingBooths: "2486091", cuAllocated: "12064", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "blue" } },
    {
      province:
        "<span class='mdi mdi-circle pr-1 small pt-1 text__primary3'></span> Battery Pack",
      noOfPollingStations: "700120",
      noOfPollingBooths: "2486091",
      cuAllocated: "12064",
      buAllocated: "12064",
      printers: "12064",
      batteryUnit: "12064",
      charger: "12064",
      status: { name: "Completed", color: "blue" },
    },
  ]);

  return (
    <table className="w-100 mt-4 small border">
      <thead>
        <tr>
          <th className="py-2 text-center pl-4 border-right"></th>
          <th className="py-2 text-center border-right">Total</th>
          <th className="py-2 text-center border-right" colSpan={2}>
            ICT
          </th>
          <th className="py-2 text-center border-right" colSpan={2}>
            Punjab
          </th>
          <th className="py-2 text-center border-right" colSpan={2}>
            Sindh
          </th>
          <th className="py-2 text-center border-right" colSpan={2}>
            Balochistan
          </th>
          <th className="py-2 text-center border-right" colSpan={2}>
            KPK
          </th>
          <th className="py-2 px-4 text-center" colSpan={2}>
            FATA
          </th>
        </tr>
        <tr className="bg__light1">
          <th className="py-2 text-center pl-4 border-right"></th>
          <th className="py-2 text-center border-right"></th>
          {/* ICT */}
          <th className="py-2 text-center text__green">operational</th>
          <th className="py-2 text-center text__red border-right">faulty</th>

          {/* Punjab */}
          <th className="py-2 text-center text__green">operational</th>
          <th className="py-2 text-center text__red border-right">faulty</th>

          {/* Sindh */}
          <th className="py-2 text-center text__green">operational</th>
          <th className="py-2 text-center text__red border-right">faulty</th>

          {/* Balochistan */}
          <th className="py-2 text-center text__green">operational</th>
          <th className="py-2 text-center text__red border-right">faulty</th>

          {/* KPK */}
          <th className="py-2 text-center text__green">operational</th>
          <th className="py-2 text-center text__red border-right">faulty</th>

          {/* FATA */}
          <th className="py-2 text-center text__green">operational</th>
          <th className="py-2 text-center text__red">faulty</th>
        </tr>
      </thead>
      <tbody>
        {list &&
          list.map((item, key) => (
            <tr key={key} className={key % 2 != 0 ? "bg__light1" : null}>
              <td
                className="py__6px text__dark border-right pl-5 font-opensans-bold"
                dangerouslySetInnerHTML={{ __html: item.province }}
              ></td>
              <td
                className="py__6px text-center border-right"
                dangerouslySetInnerHTML={{
                  __html: numberWithCommas(item.noOfPollingStations),
                }}
              ></td>
              <td className="py__6px text-center">
                {key == 0 ? key + 25600 : 25600 * 3 - key + 4}
              </td>
              <td className="py__6px text-center border-right">{key + 4}</td>
              <td className="py__6px text-center">
                {key == 0 ? key + 41200 : 41200 * 5 - key + 1}
              </td>
              <td className="py__6px text-center border-right">{key + 1}</td>
              <td className="py__6px text-center">
                {key == 0 ? key + 28000 : 28000 * 5 - key + 12}
              </td>
              <td className="py__6px text-center border-right">{key + 12}</td>
              <td className="py__6px text-center">
                {" "}
                {key == 0 ? key + 22210 : 22210 * 2 - key + 5}
              </td>
              <td className="py__6px text-center border-right">{key + 5}</td>
              <td className="py__6px text-center">
                {" "}
                {key == 0 ? key + 23120 : 23120 * 3 - key + 8}
              </td>
              <td className="py__6px text-center border-right">{key + 8}</td>
              <td className="py__6px text-center">
                {key == 0 ? key + 21200 : 21200 * 3 - key + 6}
              </td>
              <td className="py__6px text-center border-right">{key + 6}</td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
