import axios from 'axios';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import GraphBarWithIcon from './widgets/GraphBarWithIcon';

export default function PartyPositionGraph() {
    const [votesPolled] = useState([
        { quantity: "12000k" },
        { quantity: "10000k" },
        { quantity: "8000k" },
        // { quantity: "6k" },
        { quantity: "4000k" },
        { quantity: "2000k" },
        { quantity: "0" },
    ]),
        [polls, setPolls] = useState([
            // { color: "#F93A41", icon: "/assets/img/icons/bat.svg", height: "90%", stats: "11,899", label: "PTI"},
            // { color: "#46B897", icon: "/assets/img/icons/tiger.svg", height: "80%", stats: "9,043", label: "PML-N"},
            // { color: "black", icon: "/assets/img/icons/tiger.svg", height: "70%", stats: "8,211", label: "PPPP"},
            // { color: "#A6A8AB", height: "60%", stats: "6,053", label: "IND"},
            // { color: "#EFD641", icon: "/assets/img/icons/kitaab.svg", height: "50%", stats: "5,432", label: "MMA"},
            // { color: "#FF7900", height: "40%", stats: "3,005", label: "MQM-P"},
            // { color: "#28458A", height: "15%", stats: "1,879", label: "BAP"},
        ]),
        [loading, setLoading] = useState(true),
        [threshold] = useState(15500000),
        router = useRouter(),
        { province, constituency } = router.query

    
    useEffect(() => {
        if (constituency) {
            var config = {
                method: 'get',
                url: `${process.env.BASE_URL}api/config/get/result/constituency/${constituency.toUpperCase()}/parties`,
                headers: { 'Authorization': localStorage.getItem("access_token") },
            }
            
            axios(config)
            .then(function (response) {       
                console.log(response, "party-position-graph")
                if (response.status == 200) {
                    let { data } = response,
                        polls = []
                    for (var key in data) {
                        let record = data[key]
                        polls.push({
                            color: record.color,
                            label: key.slice(0, 5),
                            icon: "/assets/img/icons/bat.svg",
                            stats: Math.floor(record.votes/1000) + 'K',
                            height: parseInt(((record.votes/threshold) * 100)) + "%"
                        })
                    }
                    setPolls(polls)
                    setLoading(false)
                } 
            })
            .catch((error) => console.log(error))            
        }
    }, [constituency])


    return (
        <div className="overflow__auto bg-white border-radius__5">
            <div style={{ minWidth: "950px"}} className="w-100">
                <h6 className="font-opensans-semibold border-bottom px-3 pt-3 pb-2">Party Position</h6>
                <div className="row mx-0 mt-4 mb-4 pr-4">
                    <div className="col-1 align-self-center px-0 text-center">
                        <button className="btn bg-transparent p-0 d-inline-block mb-5" style={{transform: "rotate(270deg)", wordBreak: "normal"}}>
                            <span className="small text__dark4">Votes Polled</span>
                        </button>
                    </div>
                    <div className="col-11 position-relative px-0">
                        
                        {votesPolled && votesPolled.map((votePolled, key) => (
                            <div key={key} className="row align-items-center mb-4 pb-2 mx-0">
                                <p className="col-1 pl-0 text-center text__dark4 mb-0 small">{votePolled.quantity}</p>
                                <div className="col-11">
                                    <div className="border-bottom"></div>
                                </div>
                            </div>
                        ))}
                        <div className="w-100 py__4px"></div>
                        <div className="w-100 position-absolute pl-4 h-100 " style={{bottom: "0px", left: "0px"}}>
                                <div className="d-flex px-4 w-100 h-100">
                                        {polls && polls.map((poll, key) => (
                                            <div key={key + "party_position_graph"} className="ml-4 mr-2 align-self-end">
                                                <GraphBarWithIcon
                                                    color={poll.color}
                                                    icon={poll.icon}
                                                    height={poll.height}
                                                    stats={poll.stats}
                                                    label={poll.label}
                                                />
                                            </div>
                                        ))}
                                </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>       
    )
}
