import React from 'react';

export default function GraphBarWithIcon(props) {
    let { color, icon, height, stats, label } = props
    console.log(props, "props")
    return (
        <>
            <div className="d-flex align-items-end position-relative" style={{ height: "235px"}}>
                <p className="small text-center position-absolute w-100 transition__3" style={{ bottom: height, transform: "translateY(15px)" }}>{stats}</p>
                <div className="mx-auto transition__3" style={{ height, width: "20px", borderRadius: "24px 24px 0px 0px", transform: "translateY(5px)", backgroundColor: color }}></div>
            </div>
            <div className="d-inline-block border-radius__24 d-flex align-items-center justify-content-center bw__2 position-relative bg-white" style={{ border: `1px solid ${color}`, width: "40px", height: "30px" }}>
                <img src={icon || "/assets/img/icons/tiger.svg" } width="20px" height="20px" className="" />
            </div>
            <p className="small text-center mb-0" style={{whiteSpace: "nowrap"}}>
                <span className="small font-opensans-regular">{label}</span>
            </p>
        </>
    )
}
