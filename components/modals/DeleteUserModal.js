import React, { useEffect } from 'react'

// REDUX
import { connect } from "react-redux"
import { deleteUser } from '../../redux/actions/user'

const DeleteUserModal = (props) => {
    const { deleteUser, user } = props

    return (
        <div className="modal fade" id="deleteUserModal" tabIndex={-1} role="dialog" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-body px-4 py-4 mx-2">
                        <div className="pt-2">
                            <p className="font-opensans-bold mb-1">Deleting User</p>
                            <p>Are you sure you want to delete this user?</p>
                        </div>
                        <div className="w-100 text-right pt-3 pb-3">
                            <button className="btn bg-transparent border text__dark mr-3 border-radius__10 px-4 py-2 font-opensans-semibold" type="button" data-dismiss="modal" aria-label="Close">Cancel</button>
                            <button 
                                onClick={() => user ? deleteUser(user.username) : alert("User is undefined...")}
                                className="btn bg__danger text-white  border-radius__8 px-4 py-2">Delete</button>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        user: state.user.data
    }
}

const mapDispatchToProps = {
    deleteUser
} 
export default connect(mapStateToProps, mapDispatchToProps)(DeleteUserModal)