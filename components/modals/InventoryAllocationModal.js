import React, { useState } from "react";
// import { Link } from "next/link";
import Link from "next/link";
import { numberWithCommas } from "../../js/helpers";

const constiData = {
  "na-52": [234508, 80035, 835],
  "na-53": [312142, 325, 1104],
  "na-54": [218795, 216, 754],
};

export default function InventoryAllocationModal() {
  const [electionType, setElectionType] = useState("");
  const [consti, setConsti] = useState("");
  const [noCandi, setNoCandi] = useState("");

  return (
    <div
      className="modal fade"
      id="inventoryAllocationModal"
      tabIndex={-1}
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header align-items-center mt-2 py-0 pr-0">
            <p className="mb-0 font-opensans-semibold pl-2">
              Inventory Allocation
            </p>
            <div>
              <button
                className="btn bg-transparent mdi mdi-close text__dark mr-2 mdi-24px"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
          </div>
          <div className="modal-body px-0 py-0">
            <div className="row mx-0 align-items-center mb-3 px-4 pt-4 pb-5">
              <div className="col-lg-12 mb-3 pb-2 px-0">
                {/* <p className="text__dark3 mb-1 px-2 font-opensans-regular">
                  Select Election Type
                </p> */}
                <select
                  className="custom-select border__light3 border-radius__12 px-3"
                  onChange={(val) => setElectionType(val.target.value)}
                >
                  <option value="">Select Election Type</option>
                  <option value="general-elections-2022">
                    General Elections
                  </option>
                  <option value="by-elections">
                    By Election National Assembly
                  </option>
                  <option value="local-elections">
                    By Election Provisional Assembly
                  </option>
                  <option value="senate-elections">Senate Elections</option>
                  <option value="local-government elections">
                    ICT local GOVT Elections
                  </option>
                </select>
              </div>
              {electionType && (
                <div className="col-lg-12 mb-3 pb-2 px-0">
                  {/* <p className="text__dark3 mb-1 px-2 font-opensans-regular">
                    Select Constituency
                  </p> */}
                  <select
                    className="custom-select border__light3 border-radius__12 px-3"
                    onChange={(val) => setConsti(val.target.value)}
                  >
                    <option value="">Select Constituency</option>
                    <option value="na-52">NA-52</option>
                    <option value="na-53">NA-53</option>
                    <option value="na-54">NA-54</option>
                  </select>
                </div>
              )}

              {electionType && consti && (
                <div className="col-lg-12 mb-3 pb-2 bg__light9 py-3 px-3 border-radius__5">
                  <p className="text__dark3 mb-2">
                    Total Registered Voters:{" "}
                    <span className="font-opensans-semibold">
                      &nbsp; {consti && numberWithCommas(constiData[consti][0])}
                    </span>
                  </p>
                  <p className="text__dark3 mb-2">
                    Polling Stations:{" "}
                    <span className="font-opensans-semibold">
                      &nbsp; {consti && numberWithCommas(constiData[consti][1])}
                    </span>
                  </p>
                  <p className="text__dark3 mb-2">
                    Polling Booths:{" "}
                    <span className="font-opensans-semibold">
                      &nbsp; {consti && numberWithCommas(constiData[consti][2])}
                    </span>
                  </p>
                </div>
              )}

              {electionType && consti && (
                <div className="col-lg-12 mb-4 px-0 mt-3">
                  {/* <p className="text__dark3 mb-1 px-2 font-opensans-regular">
                    Enter No. of Candidates
                  </p> */}
                  <input
                    type="text"
                    className="border__light3 w-100 border-radius__12 px-3 py-2"
                    placeholder="Enter No. of Candidates"
                    onChange={(val) => setNoCandi(val.target.value)}
                  />
                </div>
              )}

              <div
                className="col-lg-12 px-0 mt-3 text-right"
                onClick={() =>
                  localStorage.setItem(
                    "inventoryData",
                    JSON.stringify([
                      electionType,
                      consti,
                      noCandi,
                      constiData[consti][0],
                      constiData[consti][1],
                      constiData[consti][2],
                    ])
                  )
                }
              >
                {/* <Link
                  href={{
                    pathname: "/inventory/allocation",
                    query: { data: [electionType, consti, noCandi] },
                  }}
                > */}
                {noCandi && (
                  <a
                    className="btn bg__green border-radius__10 px-4 py-2"
                    href="/inventory/allocation"
                  >
                    <button
                      // className="small text-white"
                      // className="btn bg-transp mdi mdi-close text__dark mr-2 mdi-24px"
                      className="btn bg__green border-radius__12 px-1 py-1 mx-1 text-white"
                    >
                      Calculate Inventory
                    </button>
                  </a>
                )}
                {/* </Link> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
