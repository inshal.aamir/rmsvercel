import React from 'react'

export default function ResourceAllocationModal() {
    return (
        <div className="modal fade" id="resourceAllocationModal" tabIndex={-1} role="dialog" aria-hidden="true">
            <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header align-items-center pl-5 pr-4 mt-2">
                        <div>
                            <p className="mb-0 text__grey5 font-opensans-regular">Presiding Officer - Abid Iqbal (PK-48-Mardan-I)</p>
                            <h5 className="text__dark font-opensans-semibold">Resource Allocation</h5>
                        </div>
                        <div>
                            <button className="btn bg__dark3 text__dark mr-3 border-radius__10 px-4 py-2 font-opensans-semibold" type="button" data-dismiss="modal" aria-label="Close">Cancel</button>
                            <button className="btn bg__green text-white  border-radius__10 px-4 py-2">Save Changes</button>
                        </div>
                    </div>
                    <div className="modal-body px-0 py-0">
                        <div className="row mx-0 align-items-center mb-3 bg__light8 px-4 pt-4 pb-5">
                            <div className="col-lg-6">
                                <p className="text__dark3 mb-1 px-2 font-opensans-regular">Constituency</p>
                                <select 
                                    className="custom-select border__light3 border-radius__15 px-3" id="gender2">
                                    <option>PK-48-Mardan-I</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>                                               
                            </div>
                            <div className="col-lg-6">
                                <p className="text__dark3 mb-1 px-2 font-opensans-regular">Polling Station Allocated</p>
                                <select 
                                    className="custom-select border__light3 border-radius__15 px-3" id="gender2">
                                    <option>Govt.Primary School, Mian Essa </option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>                                               
                            </div>
                        </div>
                        <div className="px-4">
                            <table className="w-100">
                                <thead className="border-bottom small"> 
                                    <tr>
                                        <td className="pb-2 pt-4 text-center">Control Unit</td>
                                        <td className="pb-2 pt-4 text-center">Ballot Unit</td>
                                        <td className="pb-2 pt-4 text-center">Printer</td>
                                        <td className="pb-2 pt-4 text-center">Printer roll</td>
                                        <td className="pb-2 pt-4 text-center">Data Cables</td>
                                        <td className="pb-2 pt-4 text-center">Battery Base</td>                            
                                        <td className="pb-2 pt-4 text-center">Battery Charger</td>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="py__10px px-3">                                            
                                            <input type="text" className="form-control bg-transparent py-2 border-radius__10"/>
                                        </td>
                                        <td className="py__10px px-3">                                            
                                            <input type="text" className="form-control bg-transparent py-2 border-radius__10"/>
                                        </td>
                                        <td className="py__10px px-3">                                            
                                            <input type="text" className="form-control bg-transparent py-2 border-radius__10"/>
                                        </td>
                                        <td className="py__10px px-3">                                            
                                            <input type="text" className="form-control bg-transparent py-2 border-radius__10"/>
                                        </td>
                                        <td className="py__10px px-3">                                            
                                            <input type="text" className="form-control bg-transparent py-2 border-radius__10"/>
                                        </td>
                                        <td className="py__10px px-3">                                            
                                            <input type="text" className="form-control bg-transparent py-2 border-radius__10"/>
                                        </td>
                                        <td className="py__10px px-3">                                            
                                            <input type="text" className="form-control bg-transparent py-2 border-radius__10"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colSpan={10} className="py-4 my-4 text-right">
                                            <button className="btn bg__dark3 border-radius__4 text__grey5 btn-block d-flex w-100 align-items-center justify-content-center">
                                                <span className="mdi mdi-plus mdi-24px mr-2"></span>
                                                <span className="font-opensans-semibold">Add another item</span>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table> 
                        </div>
                    </div>
                    <div className="modal-footer py-4 px-4 border-0">
                        <button className="btn bg-transparent text__green mr-3 border-radius__10 px-2 py-2 font-opensans-semibold">Allocate items equally</button>
                        <button className="btn text__grey5 bg-transparent font-opensans-semibold  border-radius__10 px-2 py-2">Total Allocation &nbsp; 100%</button>
                    </div>
                </div>
            </div>
        </div>
    )
}
