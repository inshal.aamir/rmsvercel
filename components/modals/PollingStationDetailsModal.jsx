import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { numberWithCommas } from "../../js/helpers";
import axios from "axios";

const PollingStationDetailsModal = ({ pollingStation }) => {
  const [topCandidates, setTopCandidates] = useState([
    {
      name: "Naveed Iqbal",
      party: "Pakistan Tehreek-E-Insaf",
      votes: 49035,
      icon: "/assets/img/icons/bat.svg",
      color: "#F93A41",
    },
    {
      name: "Muhammad Afzal Khokhar...",
      party: "Pakistan Muslim League (N)",
      votes: 41135,
      icon: "/assets/img/icons/tiger.svg",
      color: "#2CAE89",
    },
    {
      name: "Sarwar Akhtar",
      party: "Muttahida Majlis-e-Amal Pakistan",
      votes: 36035,
      icon: "/assets/img/icons/kitaab.svg",
      color: "#EDD027",
    },
    {
      name: "Muhammad Haris Khan",
      party: "Pakistan Tehreek-E-Insaf",
      votes: 32135,
      icon: "/assets/img/icons/bat.svg",
      color: "#F93A41",
    },
    {
      name: "Hafiz Muhammad Aslam",
      party: "Pakistan Peoples Party",
      votes: 30135,
      icon: "/assets/img/icons/arrow.svg",
      color: "#1178D6",
    },
  ]);

  useEffect(() => {
    console.log(pollingStation, "polling-station");
    if (pollingStation && pollingStation.pollingStationID) {
      // axios.get(`https://ems-api.block360.io/api/config/get/result/polling-station/${pollingStation}`,)
      var config = {
        method: "get",
        url: `https://ems-api.block360.io/api/config/get/result/polling-station/${pollingStation.pollingStationID}`,
        headers: { Authorization: localStorage.getItem("access_token") },
      };
      axios(config).then((res) => {
        if (res.status == 200) {
          console.log(res.data, "polling station top candidates");
          setTopCandidates(res.data);
          //   res.data.map((val)=>)
        }
      });
    }
  }, [pollingStation]);
  return (
    <div
      className="modal fade"
      id="pollingStationDetailsModal"
      tabIndex={-1}
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header align-items-center mt-2 py-0 pr-0">
            <p className="mb-0 font-opensans-semibold pl-2">
              Polling Station{" "}
              {pollingStation && pollingStation.pollingStationID}
            </p>
            <div>
              <button
                className="btn bg-transparent mdi mdi-close text__dark mr-2 mdi-24px"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
          </div>
          <div className="modal-body px-0 py-0">
            <div className="row mx-0 mb-1 pt-4 small px-3 mx-1 pb-4">
              <h5 className="col-lg-12 px-0 pb-2 font-opensans-semibold">
                Top Candidates
              </h5>

              {topCandidates &&
                topCandidates.map((c, i) => (
                  <div
                    key={i}
                    className="row mx-0 w-100 bg__light10 border-radius__5 mb-2 py-2 pr-2"
                  >
                    <div className="col-lg-8 pr-0 d-flex align-items-center py-2">
                      {/* <img
                        style={{ border: `1px solid ${c.color}` }}
                        src={c.icon}
                        width="44px"
                        height="34px"
                        className={
                          "mr-2 flex-nowrap border-radius__5 py__2px bw__2 px__3px bg-white "
                        }
                      /> */}
                      <div className="w-100 ml-1">
                        <h6 className="font-opensans-semibold mb-0">
                          {c.candidate && c.candidate.name}
                        </h6>
                        <p className="mb-0">
                          {c.candidate && c.candidate.party.name}
                        </p>
                      </div>
                    </div>
                    <div className="col-lg-4 text-right align-self-center">
                      <h6 className="mb-0">
                        <span className="mdi mdi-vote mdi-24px"></span>{" "}
                        {c.votes && c.votes}
                      </h6>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    pollingStation: state.pollingStation.data,
  };
};

const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PollingStationDetailsModal);
