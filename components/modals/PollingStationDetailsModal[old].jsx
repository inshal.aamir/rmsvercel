import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { numberWithCommas } from '../../js/helpers'

const PollingStationDetailsModal = ({ pollingStation }) => {

    useEffect(() => console.log(pollingStation, "polling-station"), [pollingStation])
    return (
        <div className="modal fade" id="pollingStationDetailsModal" tabIndex={-1} role="dialog" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header align-items-center mt-2 py-0 pr-0">
                        <p className="mb-0 font-opensans-semibold pl-2">Polling Station { pollingStation && pollingStation.pollingStationID }</p>
                        <div>
                            <button className="btn bg-transparent mdi mdi-close text__dark mr-2 mdi-24px" type="button" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                    </div>
                    <div className="modal-body px-0 py-0">
                        <div className="row mx-0 mb-1 pt-4 small">
                            <p className="col-lg-4 pl-4 border-bottom pb-2 font-opensans-semibold">Polling Station Address</p>
                            <p className="col-lg-8 border-bottom pb-2">{ pollingStation.coordinate && pollingStation.coordinate.properties && pollingStation.coordinate.properties.NAME }</p>

                            <p className="col-lg-4 pl-4 border-bottom pb-2 font-opensans-semibold">Constituency</p>
                            <p className="col-lg-8 border-bottom">NA-{ pollingStation.coordinate && pollingStation.coordinate.properties && pollingStation.coordinate.properties.NA }</p>

                            <p className="col-lg-4 pl-4 border-bottom pb-2 font-opensans-semibold">Male Booths</p>
                            <p className="col-lg-8 border-bottom">{ pollingStation.maleBooth || 0 }</p>

                            <p className="col-lg-4 pl-4 border-bottom pb-2 font-opensans-semibold">Female Booths</p>
                            <p className="col-lg-8 border-bottom">{ pollingStation.femaleBooth || 0 }</p>

                            <p className="col-lg-4 pl-4 border-bottom pb-2 font-opensans-semibold">Male Voters</p>
                            <p className="col-lg-8 border-bottom">{ pollingStation.maleVoter || 0 }</p>

                            <p className="col-lg-4 pl-4 border-bottom pb-2 font-opensans-semibold">Female Voters</p>
                            <p className="col-lg-8 border-bottom">{ pollingStation.femaleVoter || 0 }</p>

                            <p className="col-lg-4 pl-4 border-bottom pb-2 font-opensans-semibold">Total Voters</p>
                            <p className="col-lg-8 border-bottom">{ numberWithCommas(parseInt(pollingStation.maleVoter) + parseInt(pollingStation.femaleVoter)) || 0 }</p>

                            <div className="row mx-0 col-lg-12 px-0 align-items-center pb-2 border-bottom mb-3">
                                <p className="col-lg-4 pl-4 mb-0 font-opensans-semibold">Voting Status</p>
                                {
                                    pollingStation.state && pollingStation.state.toLowerCase().includes("completed") ?
                                        <div className="col-lg-8 d-flex align-items-center">
                                            <p className="text__green mr-3 mb-0">Completed</p>
                                            {/* <button className="btn px-2 border bg-transparent py-1 border-radius__8 font-opensans-semibold"><span className="small">View Details</span></button> */}
                                        </div>
                                    :
                                    <p className="col-lg-8 mb-0 text__orange ">Pending</p>
                                }
                            </div>
                            {
                                pollingStation.state && pollingStation.state.toLowerCase().includes("completed") && 
                                <>

                                    <p className="col-lg-4 pl-4 border-bottom pb-2 font-opensans-semibold">Winner</p>
                                    <p className="col-lg-8 border-bottom">{ pollingStation && pollingStation.winner && pollingStation.winner.candidate && pollingStation.winner.candidate.name }</p>
        
                                    <p className="col-lg-4 pl-4 border-bottom pb-2 font-opensans-semibold">Winning Party</p>
                                    <p className="col-lg-8 border-bottom">{ pollingStation && pollingStation.winner && pollingStation.winner.candidate && pollingStation.winner.candidate.party && pollingStation.winner.candidate.party.name }</p>

                                </>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return { 
        pollingStation: state.pollingStation.data 
    }
}

const mapDispatchToProps = { }
export default connect(mapStateToProps, mapDispatchToProps)(PollingStationDetailsModal)
