import { useRouter } from 'next/router'
import React from 'react'

// REDUX
import { connect } from "react-redux"
import { setMessage } from '../../redux/actions/site'

const ApiResponseModal = (props) => {
    const { message, setMessage } = props,
        router = useRouter()

    const style = {
        display:  message? 'block' : 'none'
    }
    return (
        <div className="modal animate__animated animate__zoomIn animate__faster" style={style}  id="apiResponseModal" tabIndex={-1} role="dialog">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-body px-4 py-4 mx-2">
                        {
                            message ?
                                <div className="py-2 text-left">
                                    <p className="font-opensans-bold mb-1">{ message.title }</p>
                                    <p>{ message.description }</p>
                                </div>
                            : null
                        }
                        <div className="w-100 text-right pt-4 pb-2">
                            <button 
                                onClick={() => (
                                    setMessage(null),
                                    message && message.onclose ? router.push(message.onclose): null
                                    
                                )}
                                className="btn bg__green text-white  border-radius__8 px-4 py-2"
                                data-dismiss='modal'>
                                Done</button>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        message: state.site.msg 
    }
}

const mapDispatchToProps = { 
    setMessage
} 
export default connect(mapStateToProps, mapDispatchToProps)(ApiResponseModal)