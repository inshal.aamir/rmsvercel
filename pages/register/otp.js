import Link from "next/link";
import { useState } from "react";
import HeadContent from "../../components/partials/HeadContent";
import "../../components/config.js";
import { useRouter } from "next/router"
import { connect } from "react-redux";
import { sendOTP } from "../../redux/actions/auth";

var axios = require("axios");

const Otp = ({ sendOTP, loading, errors }) => {
  const { asPath } = useRouter();
  const router = useRouter();

  const [username, setUsername] = useState(asPath.split("?")[1]), // aa
    [otp, setOtp] = useState()

    // getAccessToken

    function getAccessToken(e) {
    e.preventDefault();
    setLoading(true);
    var config = {
      method: "post",
      url: `${process.env.BASE_URL}api/token/`,
      headers: {},
      data: { otp, username },
    };

    axios(config)
      .then(async (response) => {
        if (response.data && response.data.access) {
          await localStorage.setItem(
            "access_token",
            `ems ${response.data.access}`
          );
          await localStorage.setItem("refresh_token", response.data.refresh);
          getCurrentUser(localStorage.getItem("access_token"));
        }
      })
      .catch(async (error) => {
        console.log(error.response, "error");
        if (error.response.data.detail) {
          alert(error.response.data.detail);
        } else if (error.response.status == 403) {
          await localStorage.setItem("username", username);
          router.push({ pathname: "/register/reset" });
        } else if (error.response.status == 401) {
          alert(error.response.data.detail);
        }
        setLoading(false);
      });
  }

  function getCurrentUser(token) {
    var config = {
      method: "get",
      url: `${process.env.BASE_URL}user/me/`,
      headers: { Authorization: token },
    };

    axios(config)
      .then((response) => {
        console.log(response.data);
        let { data } = response;
        if (data) {
          console.log("inside if");
          localStorage.setItem("access_user", JSON.stringify(data));
          if (data.is_superuser) {            
            console.log("inside super")
            router.push({ pathname: "/" })

          } else {
            if (data && data.role && data.role.length > 0) {
              let role = data.role[0]
              switch(role.role) {
                case "Result":
                  router.push({ pathname: `/election-results/polling-scheme` })
                  break
                case "Inventory":
                  router.push({ pathname: `/inventory/dashboard` })
                  break
                case "User Management":
                  router.push({ pathname: `/user-management` })
                  break
                case "Resource Allocation":
                  router.push({ pathname: `/resource-allocation` })
                  break
                default:
                  router.push({ pathname: `/` })
            }              
              // console.log(role.role.toLowerCase().trim().replaceAll(" ", "-"), data, "my-data")


            } else {
              router.push({ pathname: "/" });
            }
            
          }
          setLoading(false);
        }
      })
      .catch(function (error) {
        console.log(error.response, error);
        // alert(error.response.data.message)
      });
  }

  return (
    <div>
      <HeadContent
        title="User Detail"
        desc="This is a dummy description that will be changed later."
      />

      <main>
        <p className="text__dark4 font-opensans-semibold mt-4">OTP</p>
        <div className="row mx-0 justify-content-center">
          <div className="col-8">
            <div className="row mx-0"></div>
            <form action="" onSubmit={(e) => (e.preventDefault(), sendOTP({ otp, username }))} className="mt-5">
              <div className="input-group">
                <input
                  value={otp}
                  onChange={(e) => setOtp(e.target.value)}
                  required
                  //   type=""
                  className="form-control border__light3 border-radius__5 py-3 px-3"
                  placeholder="OTP"
                />
              </div>
              <div className="w-100 d-flex justify-content-between">              
                {errors && errors.message &&
                    <p className="w-100 text-left mb-1 mt-2 font-opensans-semibold small text__danger"><i>{ errors.message }</i></p>
                }
                <div className="text-right w-100">
                  <button onClick={() => router.push({ "pathname": "/register/login" })} type="button" className="btn p-0 bg-transparent text-muted"><span className="small">Resend OTP</span></button>
                </div>
              
              </div>

              <button
                disabled={loading}
                className="btn btn-block border-radius__5 bg__green text-white border-0 py-3 my-4"
              >
                {loading ? (
                  <span className="mdi mdi-loading mdi-spin mr-3"></span>
                ) : null}
                <span>Submit</span>
              </button>
            </form>
          </div>
        </div>
      </main>
    </div>
  );
};
Otp.layout = "LayoutRegistration";

const mapStateToProps = (state) => {
    return { 
        users: state.user.list,
        loading: state.site.loading,
        errors: state.site.errors
    }
}
  
const mapDispatchToProps = {
  sendOTP
}

export default connect(mapStateToProps, mapDispatchToProps)(Otp);
