import Link from "next/link";
import { useEffect, useState } from "react";
import HeadContent from "../../components/partials/HeadContent";
import "../../components/config.js";
import { useRouter } from "next/router";
var axios = require("axios");

const Reset = () => {
  const { asPath } = useRouter();
  const router = useRouter();
  const [storeData, setStoreData] = useState();

  useEffect(() => {
    // console.log(asPath);
    if (asPath.includes("?")) {
      let getData = asPath.split("?")[1].replace("q=", "");
      console.log(getData);
      setStoreData(getData);
    }
  }, []);

  const [oldPasswordHidden, setOldPasswordHidden] = useState(true),
    [newPasswordHidden, setNewPasswordHidden] = useState(true),
    [confirmNewPasswordHidden, setConfirmNewPasswordHidden] = useState(true),
    [loading, setLoading] = useState(false),
    // FORM-DATA
    [oldPassword, setOldPassword] = useState(""),
    [newPassword, setNewPassword] = useState(""),
    [confirmNewPassword, setConfirmNewPassword] = useState(""),
    [errors, setErrors] = useState({});

  function updatePassword(e) {
    e.preventDefault();
    setLoading(true);

    // let u = localStorage.getItem("username");
    // if (u) {
    var data = new FormData();
    // data.append("username", u);
    // data.append("old_password", oldPassword);
    data.append("password1", newPassword);
    data.append("password2", confirmNewPassword);

    var config = {
      method: "post",
      url: `${process.env.BASE_URL}user/reset_confirm/${storeData}`,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // alert(response.data.message)
        console.log(response.data, "reset.js line 98");
        router.push("/register/login")
        // getAccessToken();
      })
      .catch(function (error) {
        console.log(error.response.data, "eda");
        if (error.response.data && error.response.data.details) {
          // alert(error.response.data.details)
        }
        setLoading(false);
        setErrors(error.response.data);
      });
    // } else {
    //   alert("Username is not found in the localstorage.");
    //   // router.push({ pathname: "/register/login" })
    //   setLoading(false);
    //   console.log(username, "usernam");
    // }
  }
  return (
    <div>
      <HeadContent
        title="User Detail"
        desc="This is a dummy description that will be changed later."
      />

      <main>
        <p className="text__dark4 font-opensans-semibold mt-4">
          Setup your Password
        </p>
        <div className="row mx-0 justify-content-center">
          <div className="col-8">
            <form action="" onSubmit={updatePassword} className="mt-5">
              {/* <div className="input-group border__light3 border-radius__5 justify-content-between flex-nowrap mb-4">
                                <input 
                                    required
                                    value={oldPassword}
                                    onChange={(e) => setOldPassword(e.target.value)}
                                    type={oldPasswordHidden ? "password" : "text"} 
                                    className="control py-3 px-3 border-0 border-radius__5 w-100" placeholder="Enter Old Password"/>
                                <div className="input-group-append">
                                    <span
                                        onClick={() => setOldPasswordHidden(!oldPasswordHidden)} 
                                        className={"btn p-0 input-group-text bg-transparent border-0 px-3 mdi " + (oldPasswordHidden ? "mdi-eye-off-outline" : "mdi-eye-outline")}
                                    ></span>
                                </div>                                    
                            </div>     */}

              <div className="mb-4">
                <div className="input-group border__light3 border-radius__5 justify-content-between flex-nowrap">
                  <input
                    required
                    value={newPassword}
                    onChange={(e) => setNewPassword(e.target.value)}
                    type={newPasswordHidden ? "password" : "text"}
                    className="control py-3 px-3 border-0 border-radius__5 w-100"
                    placeholder="New Password (minimum 8 characters)"
                  />
                  <div className="input-group-append">
                    <span
                      onClick={() => setNewPasswordHidden(!newPasswordHidden)}
                      className={
                        "btn p-0 input-group-text bg-transparent border-0 px-3 mdi " +
                        (newPasswordHidden
                          ? "mdi-eye-off-outline"
                          : "mdi-eye-outline")
                      }
                    ></span>
                  </div>
                </div>
                {errors && errors.new_password1 ? (
                  <small className="ml-2 w-100 text-left form-text text__danger">
                    {errors.new_password1}
                  </small>
                ) : null}
              </div>

              <div>
                <div className="input-group border__light3 border-radius__5 justify-content-between flex-nowrap">
                  <input
                    required
                    value={confirmNewPassword}
                    onChange={(e) => setConfirmNewPassword(e.target.value)}
                    type={confirmNewPasswordHidden ? "password" : "text"}
                    className="control py-3 px-3 border-0 border-radius__5 w-100"
                    placeholder="Confirm New Password (minimum 8 characters)"
                  />
                  <div className="input-group-append">
                    <span
                      onClick={() =>
                        setConfirmNewPasswordHidden(!confirmNewPasswordHidden)
                      }
                      className={
                        "btn p-0 input-group-text bg-transparent border-0 px-3 mdi " +
                        (confirmNewPasswordHidden
                          ? "mdi-eye-off-outline"
                          : "mdi-eye-outline")
                      }
                    ></span>
                  </div>
                </div>
                {errors && errors.new_password2 ? (
                  <small className="ml-2 w-100 text-left form-text text__danger">
                    {errors.new_password2}
                  </small>
                ) : null}
              </div>

              <button
                disabled={loading}
                className="btn btn-block border-radius__5 bg__green text-white border-0 py-3 my-4"
              >
                {loading ? (
                  <span className="mdi mdi-loading mdi-spin mr-3"></span>
                ) : null}
                <span>Change Password</span>
              </button>
            </form>
          </div>
        </div>
      </main>
    </div>
  );
};
Reset.layout = "LayoutRegistration";
export default Reset;
