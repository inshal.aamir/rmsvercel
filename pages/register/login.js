import Link from "next/link";
import { useState } from "react";
import HeadContent from "../../components/partials/HeadContent";
import "../../components/config.js";
import { connect } from "react-redux";
import { login } from "../../redux/actions/auth";

const Login = ({ loading, login, errors }) => {
  const [tabs] = useState([
      { name: "Admin", key: "admin" },
      { name: "User", key: "user" },
    ]),
    [passwordHidden, setPasswordHidden] = useState(true),
    // FORM-DATA
    [username, setUsername] = useState(""), // aa
    [password, setPassword] = useState(""), // Lala1Lala
    [activeTab, setActiveTab] = useState("admin");

  return (
    <div>
      <HeadContent
        title="Login | EVM"
        desc="This is a dummy description that will be changed later."
      />

      <main>
        <p className="text__dark4 font-opensans-semibold mt-4">Sign in</p>
        <div className="row mx-0 justify-content-center">
          <div className="col-8">
            {/* <div className="row mx-0 mt-4">
                {tabs &&
                    tabs.map((tab, i) => (
                    <button
                        key={i}
                        onClick={() => setActiveTab(tab.key)}
                        className={
                        "btn col-6 bg-transparent bw__2 py-3 border-radius__0 " +
                        (activeTab == tab.key
                            ? "border-bottom__green "
                            : "border-bottom")
                        }
                    >
                        <span className="font-opensans-semibold text__dark4">
                        {tab.name}
                        </span>
                    </button>
                    ))}
                </div> */}
            <form
              onSubmit={(e) => (
                e.preventDefault(), login({ username, password })
              )}
              className="mt-5"
            >
              <div className="input-group mb-4">
                <input
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                  required
                  type="text"
                  className="form-control border__light3 border-radius__5 py-3 px-3"
                  placeholder="Username"
                />
              </div>

              <div className="input-group border__light3 border-radius__5 justify-content-between flex-nowrap">
                <input
                  required
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  type={passwordHidden ? "password" : "text"}
                  className="control py-3 px-3 border-0 border-radius__5 w-100"
                  placeholder="Password (minimum 8 characters)"
                />
                <div className="input-group-append">
                  <span
                    onClick={() => setPasswordHidden(!passwordHidden)}
                    className={
                      "btn p-0 input-group-text bg-transparent border-0 px-3 mdi " +
                      (passwordHidden
                        ? "mdi-eye-off-outline"
                        : "mdi-eye-outline")
                    }
                  ></span>
                </div>
              </div>
              {errors && errors.detail && (
                <p className="w-100 text-left mb-1 mt-2 font-opensans-semibold small text__danger">
                  {errors.detail}
                </p>
              )}
              <button
                disabled={loading}
                className="btn btn-block border-radius__5 bg__green text-white border-0 py-3 my-4"
              >
                {loading ? (
                  <span className="mdi mdi-loading mdi-spin mr-3"></span>
                ) : null}
                <span>Sign in</span>
              </button>
            </form>
            <div className="text-center">
              <span className="text__dark4 mr-1">Dont have an account?</span>

              <Link href="">
                <a className="font-opensans-semibold text__green">Signup</a>
              </Link>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

Login.layout = "LayoutRegistration";

const mapStateToProps = (state) => {
  return {
    users: state.user.list,
    loading: state.site.loading,
    errors: state.site.errors,
  };
};

const mapDispatchToProps = {
  login,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
