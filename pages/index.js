import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import PartWiseResultBarChart from "../components/chartjs/bar-charts/PartyWiseResultBarChart";
import HeadContent from "../components/partials/HeadContent";

const Home = () => {
  const [axis, setAxis] = useState("y");
  return (
    <div>
      <HeadContent
        title="Party Position"
        desc="This is a dummy description that will be changed later."
      />

      <main>
        <div className="row mx-0 mt-4 align-items-center">
          <div className="col-7 px-0">
            <div className="border d-inline-block border-color__green border-radius__3">
              <Link href={`/`}>
                <a
                  className={
                    "btn pt-0 pb-1 px-3 h-100 border-radius__2 bg__green text-white"
                  }
                >
                  <span className="small font-opensans-regular">
                    National Assembly
                  </span>
                </a>
              </Link>
              <Link href={`/`}>
                <a
                  className={
                    "btn pt-0 pb-1 px-3 h-100 border-radius__2 text-dark bg-white"
                  }
                >
                  <span className="small font-opensans-regular">
                    Provincial Assembly
                  </span>
                </a>
              </Link>
            </div>
          </div>
        </div>
        <div className="row mx-0 mt-4 mb-5">
          <div className="col-lg-8 pl-0">
            <div className="row mx-0 mb-4">
              <div className="col-lg-3 pr-2 pl-0">
                <div className="bg-white border-radius__8 p-4 text-center box-shadow__light">
                  <p className="mb-2">Total Candidates</p>
                  <h6 className="text__danger font-opensans-semibold">8,800</h6>
                </div>
              </div>
              <div className="col-lg-3 px-2">
                <div className="bg-white border-radius__8 p-4 text-center box-shadow__light">
                  <p className="mb-2">Registered Voters</p>
                  <h6 className="text__danger font-opensans-semibold">
                    105,955,409
                  </h6>
                </div>
              </div>
              <div className="col-lg-3 px-2">
                <div className="bg-white border-radius__8 p-4 text-center box-shadow__light">
                  <p className="mb-2">Average Turnout</p>
                  <h6 className="text__danger font-opensans-semibold">
                    52.58%
                  </h6>
                </div>
              </div>
              <div className="col-lg-3 pl-2 pr-0">
                <div className="bg-white border-radius__8 p-4 text-center box-shadow__light">
                  <p className="mb-2">Result Status</p>
                  <h6 className="text__yellow font-opensans-semibold">
                    Pending
                  </h6>
                </div>
              </div>
            </div>
            <div
              className="bg-white border-radius__5 small pt-3 pb-3 box-shadow__light"
              style={{ height: "88.5vh" }}
            >
              <div className="h-100" style={{ maxHeight: "85vh" }}>
                <div className="d-flex align-items-center justify-content-between pl-4 pr-3 mb-3">
                  <p className="font-opensans-semibold mb-0">
                    Party Wise Results
                  </p>
                  <button
                    onClick={() => setAxis(axis == "x" ? "y" : "x")}
                    className={
                      "btn mdi bg__light1 border-radius__10 mdi-18px transition__3 " +
                      (axis == "y"
                        ? "mdi-align-vertical-bottom"
                        : "mdi-align-horizontal-left")
                    }
                  ></button>
                </div>
                <PartWiseResultBarChart axis={axis} />
              </div>
            </div>
          </div>
          <div className="col-lg-4 px-0 overflow__hidden h-100">
            <div className="overflow__auto bg-white border-radius__5 mb-4 box-shadow__light">
              <h6 className="font-opensans-semibold px-3 pt-4 pb-2">
                Map Representation of constituencies
              </h6>
              <div className="p-5">
                <img src="/assets/img/dashboard-map.PNG" className="w-100" />
              </div>
            </div>
            <div className="mt-3">
              <div className="overflow__auto bg-white border-radius__5 box-shadow__light">
                <h6 className="font-opensans-semibold px-3 pt-4 pb-2">
                  General Seats Distribution
                </h6>
                <div className="p-5">
                  <img
                    src="/assets/img/dashboard-seats.PNG"
                    className="w-100"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};
Home.layout = "L";
export default Home;
