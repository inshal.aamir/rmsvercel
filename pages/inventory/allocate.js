import HeadContent from '../../components/partials/HeadContent.js'
import '../../components/config.js'
import Link from 'next/link'
import GenericDoughnutChart from '../../components/chartjs/doughnut-charts/GenericDoughnutChart.js'
import InventoryLogByProvinceDivision from '../../components/tables/InventoryLogByProvinceDivision.js'

const InventoryAllocate = () => {

    return (
        <div>
            <HeadContent 
                title="Allocate Inventory"
                desc="This is a dummy description that will be changed later." />

            <main className="row mx-0">
                <div className="col-lg-3 px-0 my-4">
                    <div className="bg-white border-radius__5 small pb-3 pt-1">
                        <h6 className="border-bottom py-3 px-3 font-opensans-semibold">Summary of Inventory log</h6>
                        <GenericDoughnutChart />
                    </div>                    
                </div>
                <div className="col-lg-9 my-4 pr-0">
                    <div className="bg-white border-radius__5 small pb-3 pt-1 h-100">
                        <div className="row mx-0 border-bottom mt-1 justify-content-between">
                            <p className="col-lg-12 px-3 mt-1 mb-2 font-opensans-semibold text-muted">Inventory Allocation</p>
                        </div> 
                        <div className="row mx-0 align-items-center mb-3 bg__light8 px-2 pt-3 pb-4">
                            <div className="col-lg-4">
                                <p className="text__dark3 mb-1 px-2 font-opensans-regular">Select Election</p>
                                <select 
                                    className="custom-select border__light3 border-radius__12 px-3" id="slect_election">
                                    <option>Local Government Elections of ICT</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>                                               
                            </div>
                            <div className="col-lg-3 pl-0">
                                <p className="text__dark3 mb-1 px-2 font-opensans-regular">Constituency</p>
                                <select 
                                    className="custom-select border__light3 border-radius__12 px-3" id="constituency">
                                    <option>National Assembly </option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>                                               
                            </div>
                            <div className="col-lg-2 pl-0">
                                <p className="text__dark3 mb-1 px-2 font-opensans-regular">No. of Candidates</p>
                                <input type="text" className="border__light3 border-radius__12 px-3 w-100 py-2" defaultValue={4600} />
                            </div>
                            <div className="col-lg-3 text-right mt-3 pr-5">
                                <Link href="/inventory/dashboard">
                                    <a className="btn bg__green border-radius__12 px-4 py-2">
                                        <span className="small text-white">Allocate Inventory</span>
                                    </a>
                                </Link>

                            </div>

                        </div>                                                 
                    </div>                    
                </div>
                <div className="col-lg-12 bg-white border-radius__5 px-0 mb-5">
                    <h6 className="border-bottom py-3 px-3 font-opensans-semibold">Inventory log by provinces division</h6>
                    <div className="px-3 mb-4">
                        <InventoryLogByProvinceDivision />        
                    </div>
                </div>
            </main>
        </div>
    )
}
InventoryAllocate.layout = "L"
export default InventoryAllocate