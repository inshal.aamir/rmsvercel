import HeadContent from "../../components/partials/HeadContent.js";
import "../../components/config.js";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { inventoryCalculation, numberWithCommas } from "../../js/helpers.js";

const InventoryAllocation = () => {
  const router = useRouter();
  const [inventoryData, setInventoryData] = useState();
  const [inventoryResult, setInventoryResult] = useState();
  const [electionType, setElectionType] = useState("");
  const [consti, setConsti] = useState("");
  const [noCandi, setNoCandi] = useState("");

  useEffect(() => {
    const inventoryData = JSON.parse(localStorage.getItem("inventoryData"));
    setInventoryData(JSON.parse(localStorage.getItem("inventoryData")));

    setElectionType(inventoryData[0]);
    setConsti(inventoryData[1]);
    setNoCandi(inventoryData[2]);

    const inventoryResultGet = inventoryCalculation(inventoryData[5], noCandi);
    setInventoryResult(inventoryResultGet);
  });
  return (
    <>
      {inventoryResult && inventoryResult.length > 3 && (
        <div>
          <HeadContent
            title="Allocate Inventory"
            desc="This is a dummy description that will be changed later."
          />
          <main className="row mx-0">
            <div className="col-lg-12 my-4 px-0">
              <div className="bg-white border-radius__5 small pb-3 pt-1 h-100 px-3">
                <p className="col-lg-12 px-2 mt-3 mb-2 pb-1 font-opensans-semibold text-muted">
                  Inventory Allocation
                </p>
                <div className="row mx-0 align-items-center mb-3 bg__light8 px-2 pt-3 pb-4">
                  <div className="col-lg-4">
                    <p className="text__dark3 mb-1 px-2 font-opensans-regular">
                      Select Election
                    </p>
                    <select
                      className="custom-select border__light3 border-radius__12 px-3"
                      id="slect_election"
                      value={electionType}
                    >
                      <option value="general-elections-2022">
                        General Elections 2022
                      </option>
                      <option value="by-elections">By Elections</option>
                      <option value="local-elections">Local Elections</option>
                      <option value="senate-elections">Senate Elections</option>
                      <option value="local-government elections">
                        Local Government Elections
                      </option>
                    </select>
                  </div>
                  <div className="col-lg-3 pl-0">
                    <p className="text__dark3 mb-1 px-2 font-opensans-regular">
                      Constituency
                    </p>
                    <select
                      className="custom-select border__light3 border-radius__12 px-3"
                      id="constituency"
                      value={consti}
                    >
                      <option value="na-52">NA-52</option>
                      <option value="na-53">NA-53</option>
                      <option value="na-54">NA-54</option>
                    </select>
                  </div>
                  <div className="col-lg-2 pl-0">
                    <p className="text__dark3 mb-1 px-2 font-opensans-regular">
                      No. of Candidates
                    </p>
                    <input
                      type="text"
                      className="border__light3 border-radius__12 px-3 w-100 py-2"
                      defaultValue={4600}
                      value={numberWithCommas(noCandi)}
                    />
                  </div>
                  <div className="col-lg-3 text-right mt-3 pr-5">
                    <Link href="/inventory/dashboard">
                      <a className="btn bg__green border-radius__12 px-4 py-2">
                        <span className="small text-white">
                          Allocate Inventory
                        </span>
                      </a>
                    </Link>
                  </div>
                </div>
                <div className="mx-0 border px-4 py-4 border-radius__5 w-100">
                  <div className="container ml-0">
                    <div className="row mx-0">
                      <div className="col-lg-4 mb-3 pb-2 bg__light9 py-3 px-3 border-radius__5">
                        <p className="text__dark3 mb-2">
                          Total Registered Voters:{" "}
                          <span className="font-opensans-semibold">
                            &nbsp;{" "}
                            {inventoryData &&
                              numberWithCommas(inventoryData[3])}
                          </span>
                        </p>
                        <p className="text__dark3 mb-2">
                          Polling Stations:{" "}
                          <span className="font-opensans-semibold">
                            &nbsp;{" "}
                            {inventoryData &&
                              numberWithCommas(inventoryData[4])}
                          </span>
                        </p>
                        <p className="text__dark3 mb-2">
                          Polling Booths:{" "}
                          <span className="font-opensans-semibold">
                            &nbsp;{" "}
                            {inventoryData &&
                              numberWithCommas(inventoryData[5])}
                          </span>
                        </p>
                      </div>
                    </div>
                    <div className="row mx-0">
                      <div className="col-lg-4 px-0">
                        <div className="row mx-0 bg__light9 py-2 border-left border-right border-color__light9">
                          <p className="col-lg-6 text__grey8 mb-0">Equipment</p>
                          <p className="col-lg-6 text__grey8 mb-0">
                            Units Allocated
                          </p>
                        </div>
                        <div className="row mx-0 border-left border-right">
                          <p className="col-lg-6 font-opensans-semibold mb-0 py-2 border-bottom">
                            CU
                          </p>
                          <p className="col-lg-6 mb-0 py-2 border-bottom">
                            {inventoryResult &&
                              numberWithCommas(inventoryResult[0])}
                          </p>
                          <p className="col-lg-6 font-opensans-semibold mb-0 py-2 border-bottom">
                            BU
                          </p>
                          <p className="col-lg-6 mb-0 py-2 border-bottom">
                            {inventoryResult &&
                              (isNaN(inventoryResult[1])
                                ? JSON.stringify(
                                    numberWithCommas(inventoryResult[1])
                                  )
                                : numberWithCommas(inventoryResult[1]))}
                          </p>
                          <p className="col-lg-6 font-opensans-semibold mb-0 py-2 border-bottom">
                            Printer Box
                          </p>
                          <p className="col-lg-6 mb-0 py-2 border-bottom">
                            {inventoryResult &&
                              numberWithCommas(inventoryResult[2])}
                          </p>
                          {/* <p className="col-lg-6 font-opensans-semibold mb-0 py-2 border-bottom">
                            Printer roll
                          </p> */}
                          {/* <p className="col-lg-6 mb-0 py-2 border-bottom">800</p>
                          <p className="col-lg-6 font-opensans-semibold mb-0 py-2 border-bottom">
                            Data Cables
                          </p> */}
                          {/* <p className="col-lg-6 mb-0 py-2 border-bottom">800</p> */}
                          <p className="col-lg-6 font-opensans-semibold mb-0 py-2 border-bottom">
                            Battery Pack
                          </p>
                          {/* <p className="col-lg-6 mb-0 py-2 border-bottom">800</p>
                          <p className="col-lg-6 font-opensans-semibold mb-0 py-3 border-bottom">
                            Battery Charger
                          </p> */}
                          <p className="col-lg-6 mb-0 py-2 border-bottom">
                            {inventoryResult &&
                              numberWithCommas(inventoryResult[3])}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </main>
        </div>
      )}
    </>
  );
};
InventoryAllocation.layout = "L";
export default InventoryAllocation;
