
import { useState } from 'react'
import HeadContent from '../../../components/partials/HeadContent.js'
import InventoryTable from '../../../components/tables/InventoryTable.js'
import '../../../components/config.js'
import { useRouter } from 'next/router'
import Link from 'next/link'
import InventoryLogChart from '../../../components/chartjs/doughnut-charts/InventoryLogChart.js'
import GenericDoughnutChart from '../../../components/chartjs/doughnut-charts/GenericDoughnutChart.js'
import InventoryAllocationTable from '../../../components/tables/InventoryAllocationTable.js'

const Inventory = () => {
    const router = useRouter(),
        openTab = router.query.inventory
    const [tabs] = useState([
            { name: "All Inventory", key: "all", path: "/inventory/all" },
            { name: "Operational", key: "operational", path: "/inventory/operational" },
            { name: "Faulty", key: "faulty", path: "/inventory/faulty" },
        ])

    const [list, setList] = useState([
        { province: "KPK", noOfPollingStations: "12064", noOfPollingBooths: "8193957", cuAllocated: "12064", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "green" } },
        { province: "FATA", noOfPollingStations: "1884", noOfPollingBooths: "1507901", cuAllocated: "1884", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "blue" } },
        { province: "ICT", noOfPollingStations: "31231", noOfPollingBooths: "407492", cuAllocated: "31231", buAllocated: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__orange mr-2 small"></span>`, printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "green" } },
        { province: "Punjab", noOfPollingStations: "1213", noOfPollingBooths: "9461156", cuAllocated: "1213", buAllocated: "12064", printers: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__red mr-2 small"></span>`, batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "green" } },
        { province: "Sindh", noOfPollingStations: "42432", noOfPollingBooths: "12378192", cuAllocated: "42432", buAllocated: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__red mr-2 small"></span>`, printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "green" } },
        { province: "Balochistan", noOfPollingStations: "12121", noOfPollingBooths: "2486091", cuAllocated: "12064", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "blue" } },
    ])
    return (
        <div>
            <HeadContent 
                title="All Inventory"
                desc="This is a dummy description that will be changed later." />

            <main className="row mx-0">
                <div className="col-lg-3 px-0 my-4">
                    <div className="bg-white border-radius__5  small pb-3 pt-1">
                        <h6 className="border-bottom py-3 px-3 font-opensans-semibold">Summary of Inventory log</h6>
                        <GenericDoughnutChart />
                    </div>

                </div>
                <div className="col-lg-9 pr-0 my-4">
                    <div className="bg-white border-radius__5 small pb-3 pt-1 h-100">
                        <div className="row mx-0 border-bottom my-1 justify-content-between">
                            <div className="col-lg-6 px-0 mt-1">  
                                {tabs && tabs.map((tab, key) => (
                                    <Link key={key} href={tab.path}>
                                        <a
                                             
                                            className={"bg-transparent btn pt-0 pb-1 px-3 h-100 bw__3 border-radius__0 " + (tab.key == openTab ? "border-bottom__green text-dark" : "border-bottom__transparent  text__grey6")}>
                                            <span className="small font-opensans-semibold">{tab.name}</span>
                                        </a> 
                                    </Link>
                                ))}          
                            </div>
                            <div className="col-lg-4 pl-0 d-flex align-items-center justify-content-center">
                                <div className="input-group mr-2 bg-white border-radius__15 border__light">
                                    <div className="input-group-prepend border-0">
                                        <span  
                                            className="input-group-text bg-transparent pl-3 pr-0 border-0">
                                                <img src="/assets/img/icons/search.svg" width="16px" />
                                            </span>
                                    </div>
                                    <input style={{borderRadius: "0px 35px 35px 0px"}} type="text" className="form-control border-0 bg-transparent py-0" placeholder="Search" aria-label="Username" aria-describedby="basic-addon1" />
                                </div>  
                                <button className="btn mdi-tune-vertical mdi py-0 px-1 border-radius__5 py-0 bg-transparent border"></button>                                
                            </div>
                        </div>
                        <InventoryTable />                          
                    </div>                    
                </div>
                <div className="col-lg-12 bg-white border-radius__5 px-0 mb-5">
                    <h6 className="border-bottom py-3 px-3 font-opensans-semibold">Inventory Allocation</h6>
                    <div className="row mx-0 px-4 py-4">
                        <div className="col-lg-7 px-0 row mx-0 align-items-center">
                            <span>Select Election</span>
                            <div className="mx-4">
                                <select className="custom-select border__light3 border-radius__15 px-3" id="gender2">
                                    <option>Local Goverment Elections of ICT</option>
                                    <option value="1">PK-48-Mardan-II</option>
                                    <option value="2">PK-48-Mardan-III</option>
                                </select>
                            </div>      
                            <button className="btn bg__green text-white border-radius__15 px-4 py__10px">Calculate Inventory</button>                             
                        </div>
                        <div className="col-lg-5 px-0 row mx-0">
                            <div className="col-lg-7">
                                <div className="border px-3 py-2 border-radius__5">
                                    <div className="row mx-0 align-items-center mb-1">
                                        <p className="col-7 px-0 font-opensans-semibold small mb-0">Total Polling Stations</p>
                                        <p className="col-5 text-right px-0 font-opensans-semibold text__green mb-0">543216</p>
                                    </div>
                                    <div className="row mx-0 align-items-center">
                                        <p className="col-7 px-0 font-opensans-semibold small mb-0">Total Polling Booths</p>
                                        <p className="col-5 px-0 text-right font-opensans-semibold text__green mb-0">20431313</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-5 px-0">
                                <div className="border h-100 d-flex align-items-center flex-wrap border-radius__5 px-3">
                                    <div>
                                        <p className="font-opensans-semibold small mb-2"><span className="mdi mdi-circle text__red mr-2 small"></span> Fault equipment detected</p>
                                        <p className="font-opensans-semibold small mb-0"><span className="mdi mdi-circle text__orange mr-2 small"></span> Shortage of equipment</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <InventoryAllocationTable />                  
                </div>
            </main>
        </div>
    )
}
Inventory.layout = "L"
export default Inventory