
import HeadContent from '../../components/partials/HeadContent.js'
import '../../components/config.js'
import Link from 'next/link'
import GenericDoughnutChart from '../../components/chartjs/doughnut-charts/GenericDoughnutChart.js'
import InventoryLogByProvinceDivision from '../../components/tables/InventoryLogByProvinceDivision.js'

const InventoryAllocation2 = () => {
    return (
        <div>
            <HeadContent 
                title="Allocate Inventory"
                desc="This is a dummy description that will be changed later." />

            <main className="row mx-0">
                <div className="col-lg-12 my-4 px-0">
                    <div className="bg-white border-radius__5 small pb-3 pt-1 h-100 px-3">
                        <p className="col-lg-12 px-2 mt-3 mb-2 pb-1 font-opensans-semibold text-muted">Inventory Allocation</p>
                        <div className="row mx-0 align-items-center mb-3 bg__light8 px-2 pt-3 pb-4">
                            <div className="col-lg-4">
                                <p className="text__dark3 mb-1 px-2 font-opensans-regular">Select Election</p>
                                <select 
                                    className="custom-select border__light3 border-radius__12 px-3" id="slect_election">
                                    <option value="general-elections-2022">General Elections 2022</option>
                                    <option value="by-elections">By Elections</option>
                                    <option value="local-elections">Local Elections</option>
                                    <option value="senate-elections">Senate Elections</option>
                                    <option value="local-government elections">Local Government Elections</option>
                                </select>                                               
                            </div>
                            <div className="col-lg-3 pl-0">
                                <p className="text__dark3 mb-1 px-2 font-opensans-regular">Constituency</p>
                                <select 
                                    className="custom-select border__light3 border-radius__12 px-3" id="constituency">
                                    <option value="na-260">NA-260</option>
                                    <option value="na-265">NA-265</option>
                                    <option value="na-260">NA-115</option>
                                </select>                                               
                            </div>
                            <div className="col-lg-2 pl-0">
                                <p className="text__dark3 mb-1 px-2 font-opensans-regular">No. of Candidates</p>
                                <input type="text" className="border__light3 border-radius__12 px-3 w-100 py-2" defaultValue={4600} />
                            </div>
                            <div className="col-lg-3 text-right mt-3 pr-5">
                                <Link href="/inventory/dashboard">
                                    <a className="btn bg__green border-radius__12 px-4 py-2">
                                        <span className="small text-white">Allocate Inventory</span>
                                    </a>
                                </Link>

                            </div>

                        </div> 
                        <div className="mx-0 border px-4 py-4 border-radius__5 w-100">
                            <div className="container ml-0">
                                <div className="row mx-0 mb-2">
                                    <div className="col-lg-3 pl-2 pr-3 mb-3 row mx-0 align-items-center">
                                        <p className="text__dark3 mb-0 px-1 col-6 px-0 font-opensans-semibold">Polling Stations</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={800} />
                                    </div>                               
                                </div>
                                <div className="row mx-0">
                                    <div className="col-lg-3 pl-2 pr-3 mb-3 row mx-0 align-items-center">
                                        <p className="text__dark3 mb-0 px-1 col-6 px-0 font-opensans-semibold">Polling Booths</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={3100} />
                                    </div>   
                                    <div className="col-lg-3 pl-2 pr-3 text-right mb-3 row mx-0 align-items-center">
                                        <p className="text__primary3 mb-0 px-3 col-4 px-0 font-opensans-semibold">Male</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={1500} />
                                    </div>     
                                    <div className="col-lg-3 pl-2 pr-3 mb-3 row mx-0 align-items-center">
                                        <p className="text__pink mb-0 px-1 col-3 px-0 font-opensans-semibold">Female</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={1500} />
                                    </div>                                 
                                </div>
                                <div className="row mx-0 mt-4 mb-4 pb-4">
                                    <p className="text__dark3 mb-0 px-1 col-2 px-0 font-opensans-semibold">National Assembly</p>
                                    <p className="text__dark3 mb-0 px-1 col-2 px-0 font-opensans-semibold">Punjab</p>
                                    <p className="text__dark3 mb-0 px-1 col-2 px-0 font-opensans-semibold">Balochistan</p>
                                    <p className="text__dark3 mb-0 px-1 col-2 px-0 font-opensans-semibold">KPK</p>
                                    <p className="text__dark3 mb-0 px-1 col-2 px-0 font-opensans-semibold">Sindh</p>                                                           
                                </div>


                                <div className="row mx-0 mb-2">
                                    <div className="col-lg-3 pl-0 pr-3 mb-3 row mx-0 align-items-center">
                                        <p className="text__dark3 mb-0 pr-3 pl-0 col-6 font-opensans-semibold text-right">CU</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={800} />
                                    </div>                               
                                </div>  
                                <div className="row mx-0 mb-2">
                                    <div className="col-lg-3 pl-0 pr-3 mb-3 row mx-0 align-items-center">
                                        <p className="text__dark3 mb-0 pr-3 pl-0 col-6 font-opensans-semibold text-right">BU</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={800} />
                                    </div>                               
                                </div>    
                                <div className="row mx-0 mb-2">
                                    <div className="col-lg-3 pl-0 pr-3 mb-3 row mx-0 align-items-center">
                                        <p className="text__dark3 mb-0 pr-3 pl-0 col-6 font-opensans-semibold text-right">Printer</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={800} />
                                    </div>                               
                                </div> 
                                <div className="row mx-0 mb-2">
                                    <div className="col-lg-3 pl-0 pr-3 mb-3 row mx-0 align-items-center">
                                        <p className="text__dark3 mb-0 pr-3 pl-0 col-6 font-opensans-semibold text-right">Printer roll</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={800} />
                                    </div>                               
                                </div> 
                                <div className="row mx-0 mb-2">
                                    <div className="col-lg-3 pl-0 pr-3 mb-3 row mx-0 align-items-center">
                                        <p className="text__dark3 mb-0  pr-3 pl-0 col-6 font-opensans-semibold text-right">Data Cables</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={800} />
                                    </div>                               
                                </div> 
                                <div className="row mx-0 mb-2">
                                    <div className="col-lg-3 pl-0 pr-3 mb-3 row mx-0 align-items-center">
                                        <p className="text__dark3 mb-0  pr-3 pl-0 col-6 font-opensans-semibold text-right">Battery Base</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={800} />
                                    </div>                               
                                </div> 
                                <div className="row mx-0 mb-2">
                                    <div className="col-lg-3 pl-0 pr-3 mb-3 row mx-0 align-items-center">
                                        <p className="text__dark3 mb-0 pr-3 pl-0 col-6 font-opensans-semibold text-right">Battery Charger</p>
                                        <input type="text" required className="col-6 form-control border__light3 w-100 border-radius__10 py-1 px-3" defaultValue={800} />
                                    </div>                               
                                </div>                         

                            </div>
                        </div>                                                
                    </div>                    
                </div>
            </main>
        </div>
    )
}
InventoryAllocation2.layout = "L"
export default InventoryAllocation2