
import { useState } from 'react'
import HeadContent from '../../components/partials/HeadContent.js'
import InventoryTable from '../../components/tables/InventoryTable.js'
import '../../components/config.js'
import { useRouter } from 'next/router'
import InventoryLogChart from '../../components/chartjs/doughnut-charts/InventoryLogChart.js'
import GenericDoughnutChart from '../../components/chartjs/doughnut-charts/GenericDoughnutChart.js'
import InventoryAllocationTable from '../../components/tables/InventoryAllocationTable.js'
import InventoryLogByProvinceDivision from '../../components/tables/InventoryLogByProvinceDivision.js'
import SimpleDropdown from '../../components/widgets/SimpleDropdown.js'
import InventoryAllocationModal from '../../components/modals/InventoryAllocationModal.js'
import GenericLineChart from '../../components/chartjs/line-charts/GenericLineChart.js'

const InventoryDashboard = () => {
    const router = useRouter(),
        openTab = router.query.inventory
    const [tabs] = useState([
            { name: "All Inventory", key: "all", path: "/inventory/all" },
            { name: "Operational", key: "operational", path: "/inventory/operational" },
            { name: "Faulty", key: "faulty", path: "/inventory/faulty" },
        ])

    const [list, setList] = useState([
        { province: "Control Unit", noOfPollingStations: "12064", noOfPollingBooths: "8193957", cuAllocated: "12064", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "green" } },
        { province: "Ballot Unit", noOfPollingStations: "1884", noOfPollingBooths: "1507901", cuAllocated: "1884", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "blue" } },
        { province: "Printer", noOfPollingStations: "31231", noOfPollingBooths: "407492", cuAllocated: "31231", buAllocated: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__orange mr-2 small"></span>`, printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "green" } },
        { province: "Printer roll", noOfPollingStations: "1213", noOfPollingBooths: "9461156", cuAllocated: "1213", buAllocated: "12064", printers: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__red mr-2 small"></span>`, batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "green" } },
        { province: "Data Cables", noOfPollingStations: "42432", noOfPollingBooths: "12378192", cuAllocated: "42432", buAllocated: `&nbsp; &nbsp; &nbsp; 12064 <span class="mdi mdi-circle text__red mr-2 small"></span>`, printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Pending", color: "green" } },
        { province: "Battery Base", noOfPollingStations: "12121", noOfPollingBooths: "2486091", cuAllocated: "12064", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "blue" } },
        { province: "Battery Charger", noOfPollingStations: "12121", noOfPollingBooths: "2486091", cuAllocated: "12064", buAllocated: "12064", printers: "12064", batteryUnit: "12064", charger: "12064", status: { name: "Completed", color: "blue" } },
    ])
    return (
        <div>
            <InventoryAllocationModal />

            <HeadContent 
                title="Allocate Inventory"
                desc="This is a dummy description that will be changed later." />

            <main className="row mx-0">
                <div className="row mx-0 col-lg-12 px-0 mt-3 ">
                    <h4 className="col-5 my-0 px-0 font-opensans-semibold align-self-end">INVENTORY DASHBOARD</h4>
                    <div className="col-7 text-right px-0">

                            <button className="btn bg-white border__light2 border-radius__12 px-3 py-1 mx-2" disabled>
                                <span className="d-flex align-items-center py-1">
                                    <span className="small text-dark font-opensans-regular mr-1">Export</span>
                                    {/* <span className="mdi mdi-download"></span> */}
                                </span>
                            </button>   
                            <button className="btn bg-white border__light2 border-radius__12 px-3 py-1 mx-2" disabled>
                                <span className="d-flex align-items-center py-1">
                                    <span className="small text-dark font-opensans-regular mr-1">Import</span>
                                    {/* <span className="mdi mdi-upload"></span> */}
                                </span>
                            </button>        
                            <button className="btn bg__green border-radius__12 px-4 py-2 mx-2" data-toggle="modal" data-target="#inventoryAllocationModal">
                                <span className="small text-white">Allocate Inventory</span>
                            </button>
                    </div>
                </div>  

                <div className="col-lg-6 px-0 my-4">
                    <div className="bg-white border-radius__5 small pb-3 pt-1">
                        <p className="border-bottom py-3 px-3 font-opensans-semibold">Summary of Inventory log</p>
                        <GenericDoughnutChart />
                    </div>                    
                </div>
                <div className="col-lg-6 my-4 pr-0">
                    <div className="bg-white border-radius__5 small pb-3 pt-1 h-100">
                        <p className="border-bottom py-3 px-3 w-100 font-opensans-semibold">Summary of Inventory log</p>
                        <GenericLineChart />
                    </div>                    
                </div>
                <div className="col-lg-4 my-4 pr-0 d-none">
                    <div className="bg-white border-radius__5 small pb-3 pt-1 h-100">
                        <p className="border-bottom py-3 px-3 w-100 font-opensans-semibold">Summary of Inventory log</p>
                    </div>                    
                </div>
                <div className="col-lg-12 bg-white border-radius__5 px-0 mb-5">
                    <h6 className="border-bottom py-3 px-3 font-opensans-semibold">Inventory log by provinces division</h6>
                    <div className="px-3 mb-4">
                        <InventoryLogByProvinceDivision />        
                    </div>
                </div>
            </main>
        </div>
    )
}
InventoryDashboard.layout = "L"
export default InventoryDashboard