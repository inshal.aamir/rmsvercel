import React,{useEffect} from "react"
import { useRouter } from 'next/router'

export default function Index() {

  const router = useRouter()

  useEffect(()=>{
    router.push({ pathname: "/inventory/dashboard" })
  },[])

  return (
    <div>
    <div>Loading...</div>
    </div>
  )
}

Index.layout = 'LayoutNone'
