
import Link from 'next/link'
import { useEffect, useState } from 'react'
import HeadContent from '../../../components/partials/HeadContent'
import { useRouter } from 'next/router'
import axios from 'axios'
import '../../../components/config.js'
import Breadcrumbs from '../../../components/widgets/Breadcrumbs'
import SearchSuggestionDropdown from '../../../components/widgets/SearchSuggestionDropdown'
import { updateObjectInArray } from '../../../js/helpers'

// REDUX
import { connect } from "react-redux"
import { createUser, setPermissions, setErrors } from '../../../redux/actions/user'
import CreateNewUserForm from '../../../components/forms/user/CreateNewUserForm'

const CreateNewUser = (props) => {

    const { permissions, roles, createUser, setPermissions, errors, setErrors } = props
    
    var FormData = require('form-data')
    
    const router = useRouter()

    const [username, setUsername] = useState(""),
        [firstName, setFirstName] = useState(""),
        [lastName, setLastName] = useState(""),
        [designation, setDesignation] = useState(""),
        [phone, setPhone] = useState(""),
        [email, setEmail] = useState(""),
        [cnic, setCnic] = useState(""),
        [password, setPassword] = useState(""),
        [profile, setProfile] = useState(""),
        // [permissions, setPermissions] = useState({}),
        [permissionList, setPermissionList] = useState([]),

        [preview, setPreview] = useState(null),
        [passwordHidden, setPasswordHidden] = useState(true),
        [loading, setLoading] = useState(false)
    

    function showPreview(event) {
        if(event.target.files.length > 0) {
            let { files } = event.target
            var src = URL.createObjectURL(files[0])

            setProfile(files[0])
            setPreview(src)
        }        
    }

    useEffect(() => errors || !errors ? setLoading(false) : null , [errors])

    function submitFormData(e) {
        e.preventDefault()
        // setLoading(true)

        // Check Validations
        const usernameRegex = /^[a-z0-9_.]+$/
        
        var data = new FormData();
        data.append('username', username)
        data.append('password', password)
        data.append('password2', password)
        data.append('email', email)
        data.append('first_name', firstName)
        data.append('last_name', lastName)
        data.append('cnic', cnic)
        data.append("permissions", JSON.stringify(permissions.filter(p => p.checked)))
        data.append("roles", JSON.stringify(roles.map(r => { if (r.checked) return r.role }).filter(role => role)))
        data.append("profilePicture", profile) 

        createUser(data)

    }

    return (
        <div>
            <HeadContent 
                title="User Detail"
                desc="This is a dummy description that will be changed later." />

            <main>
                <Breadcrumbs
                    links={[
                        { name: "Home", key: "/" },
                        { name: "User Management", key: "/user-management" },
                        { name: "Create new user", key: "javascript:void(0)" },
                    ]}
                />
                <CreateNewUserForm />

            </main>
        </div>
    )
}

const mapStateToProps = state => {
    return { 
        permissions: state.user.permissions,
        roles: state.user.roles,
        errors: state.user.errors
    }
}

const mapDispatchToProps = {
    createUser, setPermissions, setErrors
}

CreateNewUser.layout = "L"
export default connect(mapStateToProps, mapDispatchToProps)(CreateNewUser)