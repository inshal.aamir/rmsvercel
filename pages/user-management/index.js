import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import HeadContent from "../../components/partials/HeadContent";
import SimpleDropdown from "../../components/widgets/SimpleDropdown";
import "../../components/config.js";

// REDUX
import { connect } from "react-redux";
import { getUsers, setUser } from "../../redux/actions/user";
import DeleteUserModal from "../../components/modals/DeleteUserModal";
import { route } from "next/dist/server/router";
import { exportCsv, printScreen } from "../../js/helpers";

const UserManagement = (props) => {
  const { users, getUsers, setUser } = props;

  const [loading, setLoading] = useState(true),
    [pageno, setPageno] = useState(1),
    router = useRouter(),
    path = router.asPath.split("?")[1] || "page_number=1",
    userData =
      typeof window !== "undefined"
        ? JSON.parse(localStorage.getItem("access_user"))
        : null;

  const [search, setSearch] = useState([]);

  // [list, setList] = useState([])

  useEffect(async () => {
    setPageno(parseInt(path.split("=")[1]));
    await getUsers({ path });

    if (router.query) {
      const check = users.filter(
        (user) => user.username === router.query.username
      );
      console.log(check, "check user");

      if (check.length > 0) {
        setUser(check[0]);
        router.push(`/user-management/${router.query.username}/view`);
      }
    }
    setLoading(false);
    console.log(users, "userss");
    // users.length == 0 ? getUsers({ path }) : null
  }, []);
  return (
    console.log(search, "main seatrch"),
    (
      <div>
        <DeleteUserModal />
        <HeadContent
          title="User Management"
          desc="This is a dummy description that will be changed later."
        />

        {userData && (
          <main>
            <div className="row mx-0 my-4 align-items-center">
              <div className="col-5">
                <div
                  className="input-group mr-4 bg-white border-radius__15 border__light"
                  style={{ width: "308px" }}
                >
                  <div className="input-group-prepend border-0">
                    <span className="input-group-text bg-transparent pl-3 pr-1 border-0">
                      <img src="/assets/img/icons/search.svg" width="16px" />
                    </span>
                  </div>
                  <input
                    style={{ borderRadius: "0px 35px 35px 0px" }}
                    type="text"
                    className="form-control border-0 bg-transparent py-2"
                    placeholder="Search"
                    onChange={(e) => {
                      if (e.target.value) {
                        const searchUser = users.filter(
                          (val) =>
                            val.first_name.includes(e.target.value) ||
                            val.last_name.includes(e.target.value)
                        );
                        // console.log(searchUser, "seach user");
                        // console.log(users, " user");
                        setSearch(searchUser);
                      } else {
                        setSearch([]);
                      }
                    }}
                  />
                </div>
              </div>
              <div className="col-7 text-right">
                <div className="d-inline-block">
                  <button
                    type="button"
                    className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2"
                    // data-toggle="dropdown"
                    // aria-haspopup="true"
                    // aria-expanded="false"
                    onClick={() => {
                      console.log("export being clicked");
                      let userObject = [];
                      userObject.push(Object.keys(users[0]));
                      users.map((user) => {
                        userObject.push(Object.values(user));
                      });

                      exportCsv(userObject);
                    }}
                  >
                    <span className="d-flex align-items-center py-1">
                      <span className="small text-dark font-opensans-regular mr-1">
                        Export
                      </span>
                      {/* <span className="mdi mdi-chevron-down"></span> */}
                    </span>
                  </button>
                </div>
                <Link href="">
                  <a className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2">
                    <span
                      className="d-flex align-items-center py-1"
                      onClick={() => {
                        window.print();
                      }}
                    >
                      <span className="small text-dark font-opensans-regular">
                        Print
                      </span>
                      <img
                        src="/assets/img/icons/printer.svg"
                        width="15px"
                        className="ml-2"
                      />
                    </span>
                  </a>
                </Link>
                {userData.is_superuser ||
                (!userData.is_superuser &&
                  userData.user_permissions.filter(
                    (p) => p.codename == "user_edit"
                  ).length > 0) ? (
                  <Link href="/user-management/create-new-user">
                    <a
                      onClick={() => setUser(null)}
                      className="btn bg__green border-radius__15 px-3 py-2 mx-2"
                    >
                      <span className="d-flex align-items-center py-1">
                        <span className="small text-white">Add New User</span>
                        <img
                          src="/assets/img/icons/create-folder.svg"
                          width="15px"
                          className="ml-2"
                        />
                      </span>
                    </a>
                  </Link>
                ) : null}
              </div>
            </div>
            {loading ? (
              <p className="d-flex p-5 text-center w-100 align-items-center justify-content-center my-5">
                <span className="mdi mdi-loading mdi-spin mdi-24px text__green mr-3"></span>
                <span className="text-muted small">Loading...</span>
              </p>
            ) : !loading && users.length > 0 ? (
              <>
                <table className="w-100 bg-white border-radius__5 small">
                  <thead className="border-bottom">
                    <tr>
                      <th
                        className="pb-3 pt-4 pl-4"
                        style={{ minWidth: "75px" }}
                      >
                        ID
                      </th>
                      <th className="pb-3 pt-4" style={{ minWidth: "120px" }}>
                        Name
                      </th>
                      <th className="pb-3 pt-4" style={{ minWidth: "120px" }}>
                        Designation
                      </th>
                      <th className="pb-3 pt-4" style={{ minWidth: "120px" }}>
                        Roles
                      </th>
                      <th className="pb-3 pt-4">Email ID</th>
                      <th className="pb-3 pt-4">CNIC</th>
                      <th className="pb-3 pt-4">Created on</th>
                      <th className="pb-3 pt-4">Updated on</th>
                      <th className="pb-3 pt-4"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {!(search.length > 0)
                      ? users &&
                        users.map((user, key) => (
                          <tr key={`${key}userlist`} className="border-bottom">
                            <td className="py__10px pl-4">{user.id}</td>
                            <td className="py__10px text-capitalize">
                              {user.first_name} {user.last_name}
                            </td>
                            <td className="py__10px">
                              {user.designation || "No Designation"}
                            </td>
                            <td className="py__10px">
                              {user.role &&
                                user.role.map((role, key_) =>
                                  (key_ < 2 && user.role.length == 2) ||
                                  key_ == 0 ? (
                                    <p
                                      key={key_ + "designation"}
                                      className="d-inline-block bg__green3 text__green2 mr-2 px-3 mb-0 font-weight-bold small border-radius__24 py-1"
                                    >
                                      {role.role}
                                    </p>
                                  ) : key_ == 1 && user.role.length > 2 ? (
                                    <button className="btn p-0 text__green2 bg-transparent">
                                      <span className="small font-weight-bold">
                                        +{user.role.length - 1} more
                                      </span>
                                    </button>
                                  ) : null
                                )}
                            </td>
                            <td className="py__10px">{user.email}</td>
                            <td className="py__10px">{user.cnic}</td>
                            <td className="py__10px">
                              {user.created_at.split("T")[0]}
                            </td>
                            <td className="py__10px">
                              {user.updated_at.split("T")[0]}
                            </td>
                            {/* <td className="py__10px">
                                          {
                                              user.user_permissions && user.user_permissions.length > 0 ?
                                              user.user_permissions.map((permission, key) => (
                                                  <span key={key}>{permission.name}, </span>
                                              ))
                                              : "No Permissions Assigned!"
                                          }
                                          </td> */}
                            <td className="py__10px d-flex justify-content-center">
                              {userData.is_superuser ? (
                                <>
                                  <Link
                                    href={`/user-management/${user.username}/edit`}
                                  >
                                    <a
                                      onClick={() => setUser(user)}
                                      className="btn p-0 mr-3 bg-transparent border px-2 pb-1 border-radius__5"
                                    >
                                      <img
                                        src="/assets/img/icons/edit.svg"
                                        width="15px"
                                      />
                                    </a>
                                  </Link>

                                  <Link
                                    href={`/user-management/${user.username}/view`}
                                  >
                                    <a
                                      onClick={() => setUser(user)}
                                      className="btn p-0 mr-3 bg-transparent border px-2 pb-1 border-radius__5"
                                    >
                                      <img
                                        src="/assets/img/icons/eye.svg"
                                        width="15px"
                                      />
                                    </a>
                                  </Link>

                                  <button
                                    type="button"
                                    onClick={() => setUser(user)}
                                    data-toggle="modal"
                                    data-target="#deleteUserModal"
                                    className="btn p-0 bg-transparent border px-2 pb-1 border-radius__5"
                                  >
                                    <img
                                      src="/assets/img/icons/delete.svg"
                                      width="15px"
                                    />
                                  </button>
                                </>
                              ) : (
                                userData.user_permissions &&
                                userData.user_permissions.map((val, index) => {
                                  if (val.codename.includes("user")) {
                                    if (
                                      userData.user_permissions.filter((p) =>
                                        p.codename.includes("edit")
                                      ).length == 0 &&
                                      val.codename.includes("view")
                                    ) {
                                      return (
                                        <Link
                                          href={`/user-management/${user.username}/view`}
                                        >
                                          <a
                                            onClick={() => setUser(user)}
                                            className="btn p-0 mr-3 bg-transparent border px-2 pb-1 border-radius__5"
                                          >
                                            <img
                                              src="/assets/img/icons/eye.svg"
                                              width="15px"
                                            />
                                          </a>
                                        </Link>
                                      );
                                    } else if (val.codename.includes("edit")) {
                                      return (
                                        <>
                                          <Link
                                            href={`/user-management/${user.username}/edit`}
                                          >
                                            <a
                                              onClick={() => setUser(user)}
                                              className="btn p-0 mr-3 bg-transparent border px-2 pb-1 border-radius__5"
                                            >
                                              <img
                                                src="/assets/img/icons/edit.svg"
                                                width="15px"
                                              />
                                            </a>
                                          </Link>

                                          <Link
                                            href={`/user-management/${user.username}/view`}
                                          >
                                            <a
                                              onClick={() => setUser(user)}
                                              className="btn p-0 mr-3 bg-transparent border px-2 pb-1 border-radius__5"
                                            >
                                              <img
                                                src="/assets/img/icons/eye.svg"
                                                width="15px"
                                              />
                                            </a>
                                          </Link>

                                          <button
                                            type="button"
                                            onClick={() => setUser(user)}
                                            data-toggle="modal"
                                            data-target="#deleteUserModal"
                                            className="btn p-0 bg-transparent border px-2 pb-1 border-radius__5"
                                          >
                                            <img
                                              src="/assets/img/icons/delete.svg"
                                              width="15px"
                                            />
                                          </button>
                                        </>
                                      );
                                    }
                                  }
                                })
                              )}
                            </td>
                          </tr>
                        ))
                      : search &&
                        search.map((user, key) => (
                          <tr key={`${key}userlist`} className="border-bottom">
                            <td className="py__10px pl-4">{user.id}</td>
                            <td className="py__10px text-capitalize">
                              {user.first_name} {user.last_name}
                            </td>
                            <td className="py__10px">
                              {user.designation || "No Designation"}
                            </td>
                            <td className="py__10px">
                              {user.role &&
                                user.role.map((role, key_) =>
                                  (key_ < 2 && user.role.length == 2) ||
                                  key_ == 0 ? (
                                    <p
                                      key={key_ + "designation"}
                                      className="d-inline-block bg__green3 text__green2 mr-2 px-3 mb-0 font-weight-bold small border-radius__24 py-1"
                                    >
                                      {role.role}
                                    </p>
                                  ) : key_ == 1 && user.role.length > 2 ? (
                                    <button className="btn p-0 text__green2 bg-transparent">
                                      <span className="small font-weight-bold">
                                        +{user.role.length - 1} more
                                      </span>
                                    </button>
                                  ) : null
                                )}
                            </td>
                            <td className="py__10px">{user.email}</td>
                            <td className="py__10px">{user.cnic}</td>
                            <td className="py__10px">
                              {user.created_at.split("T")[0]}
                            </td>
                            <td className="py__10px">
                              {user.updated_at.split("T")[0]}
                            </td>
                            {/* <td className="py__10px">
                                          {
                                              user.user_permissions && user.user_permissions.length > 0 ?
                                              user.user_permissions.map((permission, key) => (
                                                  <span key={key}>{permission.name}, </span>
                                              ))
                                              : "No Permissions Assigned!"
                                          }
                                          </td> */}
                            <td className="py__10px d-flex justify-content-center">
                              {userData.is_superuser ? (
                                <>
                                  <Link
                                    href={`/user-management/${user.username}/edit`}
                                  >
                                    <a
                                      onClick={() => setUser(user)}
                                      className="btn p-0 mr-3 bg-transparent border px-2 pb-1 border-radius__5"
                                    >
                                      <img
                                        src="/assets/img/icons/edit.svg"
                                        width="15px"
                                      />
                                    </a>
                                  </Link>

                                  <Link
                                    href={`/user-management/${user.username}/view`}
                                  >
                                    <a
                                      onClick={() => setUser(user)}
                                      className="btn p-0 mr-3 bg-transparent border px-2 pb-1 border-radius__5"
                                    >
                                      <img
                                        src="/assets/img/icons/eye.svg"
                                        width="15px"
                                      />
                                    </a>
                                  </Link>

                                  <button
                                    type="button"
                                    onClick={() => setUser(user)}
                                    data-toggle="modal"
                                    data-target="#deleteUserModal"
                                    className="btn p-0 bg-transparent border px-2 pb-1 border-radius__5"
                                  >
                                    <img
                                      src="/assets/img/icons/delete.svg"
                                      width="15px"
                                    />
                                  </button>
                                </>
                              ) : (
                                userData.user_permissions &&
                                userData.user_permissions.map((val, index) => {
                                  if (val.codename.includes("user")) {
                                    if (
                                      userData.user_permissions.filter((p) =>
                                        p.codename.includes("edit")
                                      ).length == 0 &&
                                      val.codename.includes("view")
                                    ) {
                                      return (
                                        <Link
                                          href={`/user-management/${user.username}/view`}
                                        >
                                          <a
                                            onClick={() => setUser(user)}
                                            className="btn p-0 mr-3 bg-transparent border px-2 pb-1 border-radius__5"
                                          >
                                            <img
                                              src="/assets/img/icons/eye.svg"
                                              width="15px"
                                            />
                                          </a>
                                        </Link>
                                      );
                                    } else if (val.codename.includes("edit")) {
                                      return (
                                        <>
                                          <Link
                                            href={`/user-management/${user.username}/edit`}
                                          >
                                            <a
                                              onClick={() => setUser(user)}
                                              className="btn p-0 mr-3 bg-transparent border px-2 pb-1 border-radius__5"
                                            >
                                              <img
                                                src="/assets/img/icons/edit.svg"
                                                width="15px"
                                              />
                                            </a>
                                          </Link>

                                          <Link
                                            href={`/user-management/${user.username}/view`}
                                          >
                                            <a
                                              onClick={() => setUser(user)}
                                              className="btn p-0 mr-3 bg-transparent border px-2 pb-1 border-radius__5"
                                            >
                                              <img
                                                src="/assets/img/icons/eye.svg"
                                                width="15px"
                                              />
                                            </a>
                                          </Link>

                                          <button
                                            type="button"
                                            onClick={() => setUser(user)}
                                            data-toggle="modal"
                                            data-target="#deleteUserModal"
                                            className="btn p-0 bg-transparent border px-2 pb-1 border-radius__5"
                                          >
                                            <img
                                              src="/assets/img/icons/delete.svg"
                                              width="15px"
                                            />
                                          </button>
                                        </>
                                      );
                                    }
                                  }
                                })
                              )}
                            </td>
                          </tr>
                        ))}
                    <tr>
                      <td colSpan={8} className="py-3 text-right">
                        <div className="d-none align-items-center justify-content-end mr-4 py-2">
                          <Link
                            href={
                              pageno > 1
                                ? `/user-management?page_number=${pageno - 1}`
                                : "/user-management"
                            }
                          >
                            <a className="btn mdi mdi-arrow-left text-white bg__dark2 border-radius__5 mr-2"></a>
                          </Link>
                          <button className="btn bg-transparent text__green">
                            <span className="small font-weight-bold">1</span>
                          </button>
                          <button className="btn bg-transparent text-dark">
                            <span className="small font-weight-bold">2</span>
                          </button>
                          <button className="btn bg-transparent text-dark">
                            <span className="small font-weight-bold">3</span>
                          </button>
                          <button className="btn bg-transparent text-dark">
                            <span className="small font-weight-bold">4</span>
                          </button>
                          <button className="btn bg-transparent text-dark">
                            <span className="small font-weight-bold">5</span>
                          </button>
                          <p className="text-dark mb-0 mx-1">...</p>
                          <button className="btn bg-transparent text-dark">
                            <span className="small font-weight-bold">10</span>
                          </button>
                          <Link
                            href={`/user-management?page_number=${pageno + 1}`}
                          >
                            <a className="btn mdi mdi-arrow-right text-white bg__green border-radius__5 ml-2"></a>
                          </Link>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                {/* <div className="w-100 text-right my-4">
    
                              <Link href={pageno > 1 ? `/user-management?page_number=${pageno-1}` : '/user-management' }>
                                      <a className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2">
                                          <span className="d-flex align-items-center py-1">
                                              <span className="mdi mdi-chevron-left text-dark mr-1"></span>
                                              <span className="small text-dark font-opensans-regular">Prev</span>
                                          </span>
                                      </a>
                              </Link>                            
                              <Link href={`/user-management?page_number=${pageno+1}`}>
                                      <a className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2">
                                          <span className="d-flex align-items-center py-1">
                                              <span className="small text-dark font-opensans-regular">Next</span>
                                              <span className="mdi mdi-chevron-right text-dark ml-1"></span>
                                          </span>
                                      </a>
                              </Link>                            
                          </div>                    */}
              </>
            ) : (
              <p className="p-5 text-center w-100 my-5">
                <span className="text-muted small">No Users Found!</span>
              </p>
            )}
          </main>
        )}
      </div>
    )
  );
};
const mapStateToProps = (state) => {
  return { users: state.user.list };
};

const mapDispatchToProps = {
  getUsers,
  setUser,
};

UserManagement.layout = "L";
export default connect(mapStateToProps, mapDispatchToProps)(UserManagement);
