
import Image from 'next/image'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import HeadContent from '../../../components/partials/HeadContent'
import { useRouter } from 'next/router'
import '../../../components/config.js'
import Breadcrumbs from '../../../components/widgets/Breadcrumbs'

import DeleteUserModal from '../../../components/modals/DeleteUserModal'

// REDUX
import { connect } from "react-redux"
import { getRoles, setPermissions, updateUser } from '../../../redux/actions/user'
import RoleSuggestionDropdown from '../../../components/dropdowns/RoleSuggestionDropdown'
import PermissionAssociatedWithRole from '../../../components/forms/user/components/PermissionAssociatedWithRole'
import ApiResponseModal from '../../../components/modals/ApiResponseModal'
import { setErrors } from '../../../redux/actions/site'

const EditUser = (props) => {
    

    const { user, permissions, roles, errors, updateUser, getRoles, message, loading, setErrors } = props,
        router = useRouter(),
        [firstName, setFirstName] = useState("Khan"),
        [lastName, setLastName] = useState("test"),
        [designation, setDesignation] = useState(""), 
        [email, setEmail] = useState("test@gmail.com"),
        [cnic, setCnic] = useState("17101-5368966-3"),
        [password, setPassword] = useState("pakistan1234"), 
        [profile, setProfile] = useState(""),
        [createAt, setCreatedAt] = useState(""),

        [preview, setPreview] = useState(""),
        [passwordHidden, setPasswordHidden] = useState(true),
        [errors1, setErrors1] = useState(false),
       
        
        FormData = require('form-data')

   
    

    
    useEffect(() => {
        // if (!user) return
        if (!user) router.push({ pathname: "/user-management" })
        setFirstName(user.first_name)
        setLastName(user.last_name)
        setEmail(user.email)
        setCnic(user.cnic)
        setDesignation(user.designation)
        setCreatedAt(user.created_at.split("T")[0])
        user.profilePicture ? setPreview(`${process.env.BASE_URL}${user.profilePicture.slice(1)}`) : null

        setErrors(null)

    }, [user])

    useEffect(() => getRoles(user), [user])
    useEffect(() => message ? window.$("#apiResponseModal").modal() : null , [message])
    
    function showPreview(event) {
        if(event.target.files.length > 0){
            let { files } = event.target
            var src = URL.createObjectURL(files[0])

            setProfile(files[0])
            setPreview(src)
        }        
    }
    
    
    function submitFormData(e) {
       
        e.preventDefault()

        if (cnic.length<13){
            setErrors1(true)
            return
        }

        var data = new FormData();  
        data.append('email', email)
        data.append('first_name', firstName)
        data.append('last_name', lastName)
        data.append('cnic', cnic)
        data.append("permissions", JSON.stringify(permissions.filter(p => p.checked)))
        data.append("roles", JSON.stringify(roles.map(r => { if (r.checked) return r.role }).filter(role => role)))
        data.append("profilePicture", profile) 
        data.append("username", user.username) 
        data.append("designation", designation)

        updateUser(data)
        
        
    }
    const checks = () => {
      
    }
    return (
        <div>
            <DeleteUserModal />
            <ApiResponseModal message={message} />
            <HeadContent 
                title="User Detail"
                desc="This is a dummy description that will be changed later." />

            <main>
                <Breadcrumbs 
                    links={[
                        { name: "Home", key: "/" },
                        { name: "User Management", key: "/user-management" },
                        { name: "User Detail", key: "javascript:void(0)" },
                    ]}
                />
                <form onSubmit={submitFormData} className="w-100 bg-white border-radius__5 pt-4 mt-3 mb-5">
                {/* <form className="w-100 bg-white border-radius__5 pt-4 mt-3 mb-5"> */}
                    <div className="container px-0 ml-0 my-4">
                        <div className="row mx-0">
                            <div className="col-3">
                                <div className="upload-btn-wrapper p-2">
                                    <button type="button" className="btn bg-white btn__icon__lg bg__light4 ml-3 p-0 box-shadow__light overflow__hidden">
                                        {preview ? 
                                           <img src={preview} width="100%" />
                                        : 
                                            <img src="/assets/img/icons/user_light.svg" width="100px" />
                                        }
                                    </button>                            
                                    <input
                                        onChange={showPreview} 
                                        type="file" name="myfile" accept="image/*" />
                                    <p className="small text-center mt-3 text__grey4">Upload Photo</p>
                                </div>                                 
                            </div>
                            <div className="col-8">
                                <h2 className="font-opensans-semibold mt-4 mb-4 pb-2 ml-3">{firstName } {lastName}</h2>

                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Designation</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group">
                                            <input 
                                                value={designation} 
                                                onChange={(e) => setDesignation(e.target.value)}
                                                required
                                                type="text" className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="Enter Designation"/>
                                        </div>                                        
                                    </div>
                                    {errors && errors.designation ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.designation}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Full Name</p>
                                    <div className="col-lg-3 pl-0">
                                        <div className="input-group">
                                            <input 
                                                type="text" 
                                                value={firstName} 
                                                onChange={(e) => setFirstName(e.target.value)}
                                                required
                                                className="form-control border__light3 border-radius__12 py-2 px-3" placeholder="First Name"/>
                                        </div>                                      
                                    </div>
                                    <div className="col-lg-3 pl-1 pr-0">
                                        <div className="input-group">
                                            <input 
                                                type="text" 
                                                value={lastName} 
                                                onChange={(e) => setLastName(e.target.value)}
                                                required
                                                className="form-control border__light3 border-radius__12 py-2 px-3" placeholder="Last Name"/>
                                        </div>                                      
                                    </div>
                                    {errors && (errors.first_name ||  errors.last_name) ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.first_name[0] ||  errors.last_name[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>                                
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Roles</p>
                                    <div className="col-lg-6 px-0 position-relative">
                                        { roles && roles.length > 0 && <RoleSuggestionDropdown roles={roles ?? ""} /> || <p className="small mb-0 text__grey5 mt-1"><i>Loading roles...</i></p> }    
                                        {errors && errors.roles ? 
                                            <div className="row mx-0 w-100">
                                                <div className="col-6 px-0">
                                                    <small className="ml-2 form-text text__danger">{errors.roles[0]}</small>  
                                                </div>
                                            </div>                                    
                                        : null}

                                    </div>
                                    {/* {errors && errors.username ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.username[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null} */}
                                </div>
                                <div className="row mx-0 align-items-center mb-3 d-none">
                                    <p className="col-lg-3 mb-0 text__dark3">Designation</p>
                                    <div className="col-lg-6 px-0">
                                        <select 
                                            value={designation}
                                            onChange={(e) => setDesignation(e.target.value)}
                                            className="custom-select border__light3 border-radius__15 px-3" id="gender2">
                                            <option>Designer</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                        </select>                                               
                                    </div>
                                </div> 
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Email</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group">
                                            <input 
                                                value={email} 
                                                onChange={(e) => setEmail(e.target.value)}
                                                required
                                                type="email" className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="Email Address"/>
                                        </div>                                        
                                    </div>
                                    {errors && errors.email ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.email}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">CNIC</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group">
                                            <input 
                                                value={cnic} 
                                                onChange={(e) =>
                                                {
                                                    if (e.target.value.length>13){

                                                    }
                                                    else{
                                                        const result = event.target.value.replace(/\D/g, '');
                                                        setCnic(result);
                                                    }
                                                }
                                                }
                                                required
                                                minLength={13}
                                                maxLength={13}
                                                type="text" className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="CNIC"/>
                                        </div>                                        
                                    </div>
                                    {errors && errors.cnic?
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.cnic}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                    {errors1?
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">Please enter a valid CNIC</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>
                                <div className="row mx-0 align-items-center mb-3 d-none">
                                    <p className="col-lg-3 mb-0 text__dark3">Password</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group border__light3 border-radius__15 justify-content-between flex-nowrap">
                                            <input 
                                                value={password} 
                                                onChange={(e) => setPassword(e.target.value)} 
                                                type={passwordHidden ? "password" : "text"} 
                                                className="control py-2 px-3 border-0 border-radius__15 w-100" placeholder="Password"/>
                                                <div className="input-group-append">
                                                    <span
                                                        onClick={() => setPasswordHidden(!passwordHidden)} 
                                                        className={"btn p-0 input-group-text bg-transparent border-0 px-3 mdi " + (passwordHidden ? "mdi-eye-off-outline" : "mdi-eye-outline")}
                                                    ></span>
                                                </div>
                                        </div>   
                                        <div className="w-100 text-right mt-1">
                                            <Link href={"/"}>
                                                <a className="small text-dark font-opensans-semibold">Change password?</a>
                                            </Link>
                                        </div>                                     
                                    </div>
                                    {errors && errors.password ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.password[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Created on</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group border__light3 border-radius__15 justify-content-between flex-nowrap">
                                            <input  
                                                disabled
                                                type="text"
                                                className="control py-2 px-3 border-0 border-radius__15 w-100" defaultValue={createAt} />
                                                {/* <div className="input-group-append">
                                                    <span className={"p-0 input-group-text bg-transparent border-0 px-3 mdi mdi-calendar-blank-outline"}
                                                    ></span>
                                                </div> */}
                                        </div>                                        
                                    </div> 
                                </div>

                                <div className="row mx-0 mb-3 mt-4">
                                    <p className="col-lg-3 mb-0 text__dark3">Permissions</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="w-100 overflow__auto" style={{ maxHeight: "220px" }}>
                                            <PermissionAssociatedWithRole />                                                                                   
                                        </div>    
                                        <button 
                                            type="button"
                                            data-toggle="modal" data-target="#deleteUserModal"
                                            className="btn px-2 py-2 border-radius__15 bg-transparent font-opensans-bold text-dark border mt-3">
                                            <span className="small px-2">Delete User</span>
                                        </button>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="w-100 text-right px-4 py-3 mt-5 box-shadow__inset__light">
                        <Link href="/user-management">
                            <a className="btn px-4 py-2 border-radius__15 bg-transparent text-dark border mx-2">
                                <span className="small px-2">Cancel</span>
                            </a>
                        </Link>  
                        <button
                            // onClick={checks}
                            type="submit"
                            disabled={loading}
                            className="btn px-4 py-2 border-radius__15 bg__green text-white mx-2">
                            {loading ? 
                                <span className="mdi mdi-loading mdi-spin"></span>
                            : null}
                            <span className="small px-2">Save Information</span>
                        </button>

                    </div>
                </form>
            </main>
        </div>
    )
}
const mapStateToProps = state => {
    return { 
        permissions: state.user.permissions,
        roles: state.user.roles,
        user: state.user.data,
        errors: state.site.errors,
        loading: state.site.loading,
        message: state.site.msg
    }
}

const mapDispatchToProps = {
    setPermissions, updateUser, getRoles, setErrors
}

EditUser.layout = "L"
export default connect(mapStateToProps, mapDispatchToProps)(EditUser)