
import Image from 'next/image'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import HeadContent from '../../../components/partials/HeadContent'
import  { Redirect, useHistory } from 'react-router-dom'
import { useRouter } from 'next/router'
import axios from 'axios'
import '../../../components/config.js'
import Breadcrumbs from '../../../components/widgets/Breadcrumbs'

import SearchSuggestionDropdown from '../../../components/widgets/SearchSuggestionDropdown'
import DeleteUserModal from '../../../components/modals/DeleteUserModal'

// REDUX
import { connect } from "react-redux"
import { getRoles, setPermissions, updateUser } from '../../../redux/actions/user'
import { updateObjectInArray } from '../../../js/helpers'
import RoleSuggestionDropdown from '../../../components/dropdowns/RoleSuggestionDropdown'
import PermissionAssociatedWithRole from '../../../components/forms/user/components/PermissionAssociatedWithRole'

const ViewUser = (props) => {

    const { user, permissions, roles, setPermissions, errors, updateUser, getRoles } = props,
        router = useRouter()

    
    
    var FormData = require('form-data')
    

    const [firstName, setFirstName] = useState("Khan"),
        [lastName, setLastName] = useState("test"),
        [designation, setDesignation] = useState(""), 
        [email, setEmail] = useState("test@gmail.com"),
        [cnic, setCnic] = useState("17101-5368966-3"),
        [password, setPassword] = useState("pakistan1234"), 
        [profile, setProfile] = useState(""),
        [createAt, setCreatedAt] = useState(""),

        [preview, setPreview] = useState(""),
        [passwordHidden, setPasswordHidden] = useState(true),
        [loading, setLoading] = useState(false)

    
    useEffect(() => getRoles(user), [user])
    useEffect(() => {
        // if (!user) return
        if (!user) router.push({ pathname: "/user-management" })
        else{
        setFirstName(user.first_name)
        setLastName(user.last_name)
        setEmail(user.email)
        setCnic(user.cnic)
        setCreatedAt(user.created_at.split("T")[0])
        user.profilePicture ? setPreview(`${process.env.BASE_URL}${user.profilePicture.slice(1)}`) : null
        }
    }, [user])
    
    function showPreview(event) {
        if(event.target.files.length > 0){
            let { files } = event.target
            var src = URL.createObjectURL(files[0])

            setProfile(files[0])
            setPreview(src)
        }        
    }
    
    
    function submitFormData(e) {
        e.preventDefault()
        // setLoading(true) 

        var data = new FormData();  
        data.append('email', email)
        data.append('first_name', firstName)
        data.append('last_name', lastName)
        data.append('cnic', cnic)
        data.append("permissions", JSON.stringify(permissions.filter(p => p.checked)))
        data.append("roles", JSON.stringify(roles.map(r => { if (r.checked) return r.role }).filter(role => role)))
        data.append("profilePicture", profile) 
        data.append("username", user.username)

        updateUser(data)
        
    }
    return (
        <div>
            <DeleteUserModal />
            <HeadContent 
                title="User Detail"
                desc="This is a dummy description that will be changed later." />

            <main>
                <Breadcrumbs 
                    links={[
                        { name: "Home", key: "/" },
                        { name: "User Management", key: "/user-management" },
                        { name: "User Detail", key: "javascript:void(0)" },
                    ]}
                />
                <form action="" onSubmit={submitFormData} className="w-100 bg-white border-radius__5 pt-4 mt-3 mb-5">
                    <div className="container px-0 ml-0 my-4">
                        <div className="row mx-0">
                            <div className="col-3">
                                <div className="upload-btn-wrapper p-2">
                                    <button type="button" className="btn bg-white btn__icon__lg bg__light4 ml-3 p-0 box-shadow__light overflow__hidden">
                                        {preview ? 
                                           <img src={preview} width="100%" />
                                        : 
                                            <img src="/assets/img/icons/user_light.svg" width="100px" />
                                        }
                                    </button>                            
                                    <input
                                        onChange={showPreview} 
                                        type="file" name="myfile" accept="image/*" disabled />
                                    <p className="small text-center mt-3 text__grey4">Upload Photo</p>
                                </div>                                 
                            </div>
                            <div className="col-8">
                                <h2 className="font-opensans-semibold mt-4 mb-4 pb-2 ml-3">{firstName } {lastName}</h2>

                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Full Name</p>
                                    <div className="col-lg-3 pl-0">
                                        <div className="input-group">
                                            <input 
                                                type="text" 
                                                value={firstName} 
                                                onChange={(e) => setFirstName(e.target.value)}
                                                required
                                                disabled
                                                className="form-control border__light3 border-radius__12 py-2 px-3" placeholder="First Name"/>
                                        </div>                                      
                                    </div>
                                    <div className="col-lg-3 pl-1 pr-0">
                                        <div className="input-group">
                                            <input 
                                                type="text" 
                                                value={lastName} 
                                                onChange={(e) => setLastName(e.target.value)}
                                                required
                                                disabled
                                                className="form-control border__light3 border-radius__12 py-2 px-3" placeholder="Last Name"/>
                                        </div>                                      
                                    </div>
                                    {errors && (errors.first_name ||  errors.last_name) ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.first_name[0] ||  errors.last_name[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>                                
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Roles(s)</p>
                                    <div className="col-lg-6 px-0 position-relative">
                                        { roles && roles.length > 0 && <RoleSuggestionDropdown disabled={true} roles={roles ?? ""} /> || <p className="small mb-0 text__grey5 mt-1"><i>Loading roles...</i></p> }    
                                        {errors && errors.roles ? 
                                            <div className="row mx-0 w-100">
                                                <div className="col-6 px-0">
                                                    <small className="ml-2 form-text text__danger">{errors.roles[0]}</small>  
                                                </div>
                                            </div>                                    
                                        : null}

                                    </div>
                                    {/* {errors && errors.username ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.username[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null} */}
                                </div>
                                <div className="row mx-0 align-items-center mb-3 d-none">
                                    <p className="col-lg-3 mb-0 text__dark3">Designation</p>
                                    <div className="col-lg-6 px-0">
                                        <select 
                                            value={designation}
                                            onChange={(e) => setDesignation(e.target.value)}
                                            className="custom-select border__light3 border-radius__15 px-3" id="gender2">
                                            <option>Designer</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                        </select>                                               
                                    </div>
                                </div> 
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Email</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group">
                                            <input 
                                                value={email} 
                                                onChange={(e) => setEmail(e.target.value)}
                                                required
                                                disabled
                                                type="email" className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="Email Address"/>
                                        </div>                                        
                                    </div>
                                    {errors && errors.email ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.email[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">CNIC</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group">
                                            <input 
                                                value={cnic} 
                                                onChange={(e) => setCnic(e.target.value)}
                                                required
                                                disabled
                                                type="text" className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="CNIC"/>
                                        </div>                                        
                                    </div>
                                    {errors && errors.cnic ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.cnic[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>
                                <div className="row mx-0 align-items-center mb-3 d-none">
                                    <p className="col-lg-3 mb-0 text__dark3">Password</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group border__light3 border-radius__15 justify-content-between flex-nowrap">
                                            <input 
                                                value={password} 
                                                onChange={(e) => setPassword(e.target.value)} 
                                                type={passwordHidden ? "password" : "text"} 
                                                className="control py-2 px-3 border-0 border-radius__15 w-100" placeholder="Password"/>
                                                <div className="input-group-append">
                                                    <span
                                                        onClick={() => setPasswordHidden(!passwordHidden)} 
                                                        className={"btn p-0 input-group-text bg-transparent border-0 px-3 mdi " + (passwordHidden ? "mdi-eye-off-outline" : "mdi-eye-outline")}
                                                    ></span>
                                                </div>
                                        </div>   
                                        <div className="w-100 text-right mt-1">
                                            <Link href={"/"}>
                                                <a className="small text-dark font-opensans-semibold">Change password?</a>
                                            </Link>
                                        </div>                                     
                                    </div>
                                    {errors && errors.password ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.password[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Created on</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group border__light3 border-radius__15 justify-content-between flex-nowrap">
                                            <input  
                                                disabled
                                                type="text"
                                                className="control py-2 px-3 border-0 border-radius__15 w-100" defaultValue={createAt} />
                                                {/* <div className="input-group-append">
                                                    <span className={"p-0 input-group-text bg-transparent border-0 px-3 mdi mdi-calendar-blank-outline"}
                                                    ></span>
                                                </div> */}
                                        </div>                                        
                                    </div> 
                                </div>

                                <div className="row mx-0 mb-5 pb-5 mt-4">
                                    <p className="col-lg-3 mb-0 text__dark3">Permissions</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="w-100 overflow__auto" style={{ maxHeight: "220px" }}>
                                            <PermissionAssociatedWithRole />                                                                                    
                                        </div>                                           
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </form>
            </main>
        </div>
    )
}
const mapStateToProps = state => {
    return { 
        permissions: state.user.permissions,
        roles: state.user.roles,
        errors: state.user.errors,
        user: state.user.data
    }
}

const mapDispatchToProps = {
    setPermissions, updateUser, getRoles
}

ViewUser.layout = "L"
export default connect(mapStateToProps, mapDispatchToProps)(ViewUser)