
import Image from 'next/image'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import HeadContent from '../../../components/partials/HeadContent'
import  { Redirect } from 'react-router-dom'
import { useRouter } from 'next/router'
import axios from 'axios'
import '../../../components/config.js'
import Breadcrumbs from '../../../components/widgets/Breadcrumbs'

import SearchSuggestionDropdown from '../../../components/widgets/SearchSuggestionDropdown'

const ViewUser = () => {
    
    var FormData = require('form-data')
    
    const router = useRouter()

    const [username, setUsername] = useState("Kamran"),
        [firstName, setFirstName] = useState("Khan"),
        [lastName, setLastName] = useState("test"),
        [designation, setDesignation] = useState(""),
        [phone, setPhone] = useState("0314-9888489"),
        [email, setEmail] = useState("test@gmail.com"),
        [cnic, setCnic] = useState("17101-5368966-3"),
        [password, setPassword] = useState("pakistan1234"),
        [permissions, setPermissions] = useState({}),
        [permissionList, setPermissionList] = useState([]),

        [preview, setPreview] = useState("/assets/img/post-profile3.png"),
        [passwordHidden, setPasswordHidden] = useState(true),
        [loading, setLoading] = useState(false),
        [loadingPermissions, setLoadingPermissions] = useState(true),
        [uid] = useState(router.query.uid)
    
    const [errors, setErrors] = useState({})
    
    useEffect(() => {
        getPermissions()
    }, [])

    function getPermissions() {
        var config = {
            method: 'get',
            url: `${process.env.BASE_URL}user/get-permissions/?page_number=1`,
            headers: {}
        }

        axios(config)
        .then(function (response) {
            if (response.data && response.data.length > 0) {
                setPermissionList(response.data)
            }
            setLoadingPermissions(false)
            console.log(response.data, "permissions") 
        })
        .catch(function (error) {
            console.log(error)
            setLoadingPermissions(false)
        })        
    }

    function showPreview(event) {
        if(event.target.files.length > 0){
            var src = URL.createObjectURL(event.target.files[0]);
            setPreview(src)
        }        
    }
    function filterPermissions() {
        let filtered = []
        for (const prop in permissions) {
            permissions[prop] ? filtered.push(prop) : null
        }
        return filtered
    }
    function createNewUser(e) {
        e.preventDefault()
        // setLoading(true)
        setErrors({})

        // Check Validations
        const usernameRegex = /^[a-z0-9_.]+$/

        // if (!usernameRegex.test(username) && )
        // console.log(usernameRegex.test(username))
        // return
        
        var data = new FormData();
        data.append('username', username);
        data.append('password', password);
        data.append('password2', password);
        data.append('email', email);
        data.append('first_name', firstName);
        data.append('last_name', lastName);
        filterPermissions().forEach(permission => data.append("permissions[]", permission))
        
        var config = {
            method: 'post',
            url: `${process.env.BASE_URL}user/add-user/`,
            headers: { 
                'Authorization': localStorage.getItem("access_token"), 
            },
            data: {
                username, 
                password,
                password2: password,
                email,
                first_name: firstName,
                last_name: lastName,
                permissions: filterPermissions()
            }
        };
        
        axios(config)
        .then(function (response) {
            console.log
            setLoading(false)
            router.push({ pathname: "/user-management" })
        })
        .catch((error) => {
            if (error && error.response && error.response.data) {
                let err = error.response.data
                if (err.messages && err.messages.length > 0) {
                    let msg = err.messages[0]["message"]
                    alert (msg)
                } else {
                    setErrors(error.response.data)
                }
            }
            setLoading(false)
        })    
    }

    return (
        <div>
            <HeadContent 
                title="User Detail"
                desc="This is a dummy description that will be changed later." />

            <main>
                <Breadcrumbs 
                    links={[
                        { name: "Home", key: "/" },
                        { name: "User Management", key: "/user-management" },
                        { name: "User Detail", key: "javascript:void(0)" },
                    ]}
                />
                <form action="" onSubmit={createNewUser} className="w-100 bg-white border-radius__5 pt-4 mt-3 mb-5">
                    <div className="container px-0 ml-0 my-4 pb-5">
                        <div className="row mx-0 pb-5">
                            <div className="col-3">
                                <div className="upload-btn-wrapper p-2">
                                    <button type="button" className="btn bg-white btn__icon__lg bg__light4 ml-3 p-0 box-shadow__light overflow__hidden">
                                        {preview ? 
                                           <img src={preview} width="100%" />
                                        : 
                                            <img src="/assets/img/icons/user_light.svg" width="100px" />
                                        }
                                    </button>                            
                                    <input
                                        onChange={showPreview} 
                                        type="file" name="myfile" accept="image/*" />
                                    <p className="small text-center mt-3 text__grey4">Upload Photo</p>
                                </div>                                 
                            </div>
                            <div className="col-8">
                                <div className="d-flex align-items-center mt-4 mb-4">
                                    <h2 className="font-opensans-semibold ml-3 mb-0 mr-3 mb-1">Akram Waqas</h2>
                                    <Link href={`/user-management/${uid}/edit`}>
                                        <a className="btn px-2 py-1 border-radius__15 bg-transparent font-opensans-bold text-dark border">
                                            <span className="small px-2">Edit</span>
                                        </a>
                                    </Link>  
                                    
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Designation(s)</p>
                                    <div className="col-lg-6 px-0 position-relative">
                                        <SearchSuggestionDropdown 
                                            list={[
                                                { name: "Role 1", code: "role_1", checked: true },
                                                { name: "Role 2", code: "role_2", checked: true },
                                                { name: "Role 3", code: "role_3", checked: false },
                                                { name: "Role 4", code: "role_4", checked: false },
                                                { name: "Role 5", code: "role_5", checked: false },
                                            ]}    
                                            disabled={true}  
                                        />                                        
                                    </div>
                                    {/* {errors && errors.username ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.username[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null} */}
                                </div>
                                <div className="row mx-0 align-items-center mb-3 d-none">
                                    <p className="col-lg-3 mb-0 text__dark3">Designation</p>
                                    <div className="col-lg-6 px-0">
                                        <select 
                                            value={designation}
                                            onChange={(e) => setDesignation(e.target.value)}
                                            className="custom-select border__light3 border-radius__15 px-3" id="gender2">
                                            <option>Designer</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                        </select>                                               
                                    </div>
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Phone</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group">
                                            <input 
                                                value={phone} 
                                                onChange={(e) => setPhone(e.target.value)}
                                                required
                                                type="text" className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="Phone"/>
                                        </div>                                        
                                    </div>
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Email</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group">
                                            <input 
                                                value={email} 
                                                onChange={(e) => setEmail(e.target.value)}
                                                required
                                                type="email" className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="Email Address"/>
                                        </div>                                        
                                    </div>
                                    {errors && errors.email ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.email[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">CNIC</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group">
                                            <input 
                                                value={cnic} 
                                                onChange={(e) => setCnic(e.target.value)}
                                                required
                                                type="text" className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="CNIC"/>
                                        </div>                                        
                                    </div>
                                    {errors && errors.cnic ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.cnic[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Password</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group border__light3 border-radius__15 justify-content-between flex-nowrap">
                                            <input 
                                                value={password} 
                                                onChange={(e) => setPassword(e.target.value)}
                                                required
                                                type={passwordHidden ? "password" : "text"} 
                                                className="control py-2 px-3 border-0 border-radius__15 w-100" placeholder="Password"/>
                                                <div className="input-group-append">
                                                    <span
                                                        onClick={() => setPasswordHidden(!passwordHidden)} 
                                                        className={"btn p-0 input-group-text bg-transparent border-0 px-3 mdi " + (passwordHidden ? "mdi-eye-off-outline" : "mdi-eye-outline")}
                                                    ></span>
                                                </div>
                                        </div>                                    
                                    </div>
                                    {errors && errors.password ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.password[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>
                                <div className="row mx-0 align-items-center mb-3">
                                    <p className="col-lg-3 mb-0 text__dark3">Created on</p>
                                    <div className="col-lg-6 px-0">
                                        <div className="input-group border__light3 border-radius__15 justify-content-between flex-nowrap">
                                            <input 
                                                required
                                                type="text"
                                                className="control py-2 px-3 border-0 border-radius__15 w-100" defaultValue={"25 Dec 2021, 1:00 PM"} />
                                                <div className="input-group-append">
                                                    <span className={"btn p-0 input-group-text bg-transparent border-0 px-3 mdi mdi-calendar-blank-outline"}
                                                    ></span>
                                                </div>
                                        </div>                                        
                                    </div>
                                    {errors && errors.password ? 
                                        <div className="row mx-0 w-100">
                                            <div className="col-3"></div>
                                            <div className="col-6 px-0">
                                                <small className="ml-2 form-text text__danger">{errors.password[0]}</small>  
                                            </div>
                                        </div>                                    
                                    : null}
                                </div>

                                <div className="row mx-0 mb-3 mt-4">
                                    <p className="col-lg-3 mb-0 text__dark3">Permissions</p>
                                    <div className="col-lg-6 px-0">
                                        {!loadingPermissions ? 
                                            permissionList && permissionList.slice(0, 2).map((permission, key) => (
                                                <div key={key} className="mb-3">
                                                    <input
                                                        onChange={(e) => setPermissions({
                                                            ...permissions,
                                                            [permission.codename]: e.target.checked
                                                        })} 
                                                        type="checkbox" id={`permission__${key}`} name="radio-group" />
                                                    <label htmlFor={`permission__${key}`}><span></span> {permission.name}</label>
                                                </div>    
                                            ))
                                        :
                                            <p className="small">Loading Permissions...</p>
                                        }                                                                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>
    )
}
ViewUser.layout = "L"
export default ViewUser