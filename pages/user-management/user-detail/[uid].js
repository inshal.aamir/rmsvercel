
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'
import HeadContent from '../../../components/partials/HeadContent'

export default function UserDetail() {
    return (
        <div>
            <HeadContent 
                title="User Detail"
                desc="This is a dummy description that will be changed later." />

            <main>
                <div className="w-100">
                    <button className="btn px-0 py-0">
                        <span className="small">Home</span>
                    </button>
                    /
                    <button className="btn px-0 py-0">
                        <span className="small">Home</span>
                    </button>
                    /
                    <button className="btn px-0 py-0">
                        <span className="small">Home</span>
                    </button>
                    
                </div>
            </main>
        </div>
    )
}