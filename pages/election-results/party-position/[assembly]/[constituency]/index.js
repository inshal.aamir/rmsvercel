
import dynamic from 'next/dynamic'
import Link from 'next/link'
import { useRouter } from 'next/router'
import PartyPositionDoughnutChart from '../../../../../components/chartjs/doughnut-charts/PartyPositionDoughnutChart.js'
import '../../../../../components/config.js'
import HeadContent from '../../../../../components/partials/HeadContent.js'
import CandidatesListTable from '../../../../../components/tables/CandidatesListTable.js'

const IndividualConstituencyMapView = dynamic(() => import('../../../../../components/maps/IndividualConstituencyMapView'), { ssr: false })

const Constituency = () => {
    const router = useRouter(),
        { constituency } = router.query
    return (
        <div>
            <HeadContent 
                title="Party Position"
                desc="This is a dummy description that will be changed later." />

            <main>
                
                <div className="row mx-0 mt-4 align-items-center">
                    <div className="col-7 px-0">
                        <h4 className="font-opensans-bold my-2">{constituency ? constituency.toUpperCase() : "Null"} ELECTION 2022 RESULTS</h4>
                    </div>
                    <div className="col-5 text-right">
                        <button className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2" disabled>
                            <span className="d-flex align-items-center py-1">
                                <span className="small text-dark font-opensans-regular">Export</span>
                                <img src="/assets/img/icons/download.svg" width="15px" className="ml-2" />
                            </span>
                        </button>
                        <button className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2" disabled>
                            <span className="d-flex align-items-center py-1">
                                <span className="small text-dark font-opensans-regular">Print</span>
                                <img src="/assets/img/icons/printer.svg" width="15px" className="ml-2" />
                            </span>
                        </button>
                    </div>
                </div>
                <div className="row mx-0 mt-4 mb-5">
                    <div className="col-lg-8 pl-0">
                        <CandidatesListTable />         
                    </div>
                    <div className="col-lg-4 px-0">
                        {/* <PartyPositionGraph /> */}
                        <PartyPositionDoughnutChart />
                        <div className="mt-3">
                            <IndividualConstituencyMapView />
                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}
Constituency.layout = "L"
export default Constituency