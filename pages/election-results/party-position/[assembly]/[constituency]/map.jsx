
import dynamic from 'next/dynamic'
import Link from 'next/link'
import { useRouter } from 'next/router'
import PartyPositionDoughnutChart from '../../../../../components/chartjs/doughnut-charts/PartyPositionDoughnutChart.js'
import '../../../../../components/config.js'
import HeadContent from '../../../../../components/partials/HeadContent.js'

const IndividualConstituencyWideMapView = dynamic(() => import('../../../../../components/maps/IndividualConstituencyWideMapView'), { ssr: false })

const Constituency = () => {
    const router = useRouter(),
        { constituency } = router.query
    return (
        <div>
            <HeadContent 
                title="Party Position"
                desc="This is a dummy description that will be changed later." />

            <main className="container-fluid px-0">
                
                <div className="row mx-0 mt-4 align-items-center">
                    <div className="col-7 px-0">
                        <h4 className="font-opensans-bold my-2">{constituency ? constituency.toUpperCase() : "Null"} MAP VIEW</h4>
                    </div>
                    <div className="col-5 text-right">
                        <Link href="">
                                <a className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2 d-none">
                                    <span className="d-flex align-items-center py-1">
                                        <span className="small text-dark font-opensans-regular">Export</span>
                                        <img src="/assets/img/icons/download.svg" width="15px" className="ml-2" />
                                    </span>
                                </a>
                        </Link> 
                        <Link href="">
                                <a className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2 d-none">
                                    <span className="d-flex align-items-center py-1">
                                        <span className="small text-dark font-opensans-regular">Print</span>
                                        <img src="/assets/img/icons/printer.svg" width="15px" className="ml-2" />
                                    </span>
                                </a>
                        </Link>                        
                    </div>
                </div>
                <div className="row mx-0 mt-4 mb-5 ">
                    <div className="col-lg-3 px-0 bg-white text-uppercase overflow__auto" style={{ maxHeight: "800px" }}>
                        <div className="w-100 py-4 bg__green mb-3"></div>
                        <div className="border-bottom px-2 py-2">
                            <div className="px-4 pt-3 active union__council">
                                <p className="font-opensans-semibold pl-1 mb-1"> UC-43 | NA-54</p>
                                <p className="d-flex align-items-center"><span className="mdi mdi-24px mdi-gamepad-circle-outline text__orange mr-2"></span> UC-43 (i-10 2&3) nA-54 isb</p>
                            </div>
                        </div>
                        <div className="border-bottom px-2 py-2">
                            <div className="px-4 pt-3">
                                <p className="font-opensans-semibold pl-1 mb-1"> uC-42 | nA-54</p>
                                <p className="d-flex align-items-center"><span className="mdi mdi-24px mdi-gamepad-circle-outline text__green mr-2"></span> uC-42 (i-10 1&4) nA-54 isb</p>
                            </div>
                        </div>  
                        <div className="border-bottom px-2 py-2">
                            <div className="px-4 pt-3">
                                <p className="font-opensans-semibold pl-1 mb-1"> uC-29 | nA-54</p>
                                <p className="d-flex align-items-center"><span className="mdi mdi-24px mdi-gamepad-circle-outline text__blue mr-2"></span> uC-29 (F-10 & 11) nA-54 isb</p>
                            </div>
                        </div>  
                        <div className="border-bottom px-2 py-2">
                            <div className="px-4 pt-3">
                                <p className="font-opensans-semibold pl-1 mb-1"> uC-38 | nA-54</p>
                                <p className="d-flex align-items-center"><span className="mdi mdi-24px mdi-gamepad-circle-outline text__yellow mr-2"></span> uC-38 (G-11) nA-54 isb</p>
                            </div>
                        </div>  
                        <div className="border-bottom px-2 py-2">
                            <div className="px-4 pt-3">
                                <p className="font-opensans-semibold pl-1 mb-1"> uC-36 | nA-54</p>
                                <p className="d-flex align-items-center"><span className="mdi mdi-24px mdi-gamepad-circle-outline text__red mr-2"></span> uC-36 (G-10 3&4) nA-54 isb</p>
                            </div>
                        </div>   
                        <div className="border-bottom px-2 py-2">
                            <div className="px-4 pt-3">
                                <p className="font-opensans-semibold pl-1 mb-1"> uC-43 | nA-54</p>
                                <p className="d-flex align-items-center"><span className="mdi mdi-24px mdi-gamepad-circle-outline text__primary mr-2"></span> uC-43 (i-10 2&3) nA-54 isb</p>
                            </div>
                        </div>      
                        <div className="border-bottom px-2 py-2">
                            <div className="px-4 pt-3">
                                <p className="font-opensans-semibold pl-1 mb-1"> uC-37 | nA-54</p>
                                <p className="d-flex align-items-center"><span className="mdi mdi-24px mdi-gamepad-circle-outline text__pink mr-2"></span> uC-43 (G-10 1&2) nA-54 isb</p>
                            </div>
                        </div>    
                    </div>
                    <div className="col-lg-9 px-0">
                        <IndividualConstituencyWideMapView />
                    </div>
                </div>
            </main>
        </div>
    )
}
Constituency.layout = "L"
export default Constituency