import dynamic from "next/dynamic";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import "../../../../components/config.js";
// import PartyPositionMayorMap from '../../../../components/maps/PartyPositionMayorMap.js'
import HeadContent from "../../../../components/partials/HeadContent.js";
import ConstituencyWithTopResult from "../../../../components/tables/ConstituencyWithTopResult.js";

import { connect } from "react-redux";
import PollingStationsList from "../../../../components/list/PollingStationsList.jsx";
import WinningPoliticalPartiesInConstituenciesList from "../../../../components/list/WinningPoliticalPartiesInConstituenciesList.jsx";
import axios from "axios";
// import ConstituenciesResultMapView from "../../../../components/maps/ConstituenciesMapView";
// import PollingStationsResultMapView from "../../../../components/maps/PollingStationsResultMapView";

const ConstituenciesResultMapView = dynamic(
  () => import("../../../../components/maps/ConstituenciesResultMapView.jsx"),
  { ssr: false }
);
// const ConstituenciesResultMapView = import("../../../../components/maps/ConstituenciesResultMapView.jsx");
const PollingStationsResultMapView = dynamic(
  () => import("../../../../components/maps/PollingStationsResultMapView.jsx"),
  { ssr: false }
);

const PartyPosition = ({ latlng }) => {
  // useEffect(()=>{
  //     axios.get()
  // })

  const [tabs] = useState([
      { name: "National Assembly", key: "national" },
      { name: "Provincial Assembly", key: "provincial" },
    ]),
    router = useRouter(),
    { assembly, view = "table", province, constituency } = router.query;

  return (
    <div>
      <HeadContent
        title="Party Position"
        desc="This is a dummy description that will be changed later."
      />

      <main>
        <h4 className="font-opensans-bold my-2">
          PARTY POSITION GENERAL ELECTIONS 2022
        </h4>

        <div className="row mx-0 my-4 align-items-center">
          <div className="col-5 px-0">
            <div className="border d-inline-block border-color__green border-radius__3">
              {tabs &&
                tabs.map((tab, key) => (
                  <Link
                    href={`/election-results/party-position/${tab.key}?view=${view}`}
                    key={key}
                  >
                    <a
                      className={
                        "btn pt-0 pb-1 px-3 h-100 border-radius__2 " +
                        (tab.key == assembly
                          ? "bg__green text-white"
                          : "text-dark bg-white")
                      }
                    >
                      <span className="small font-opensans-regular">
                        {tab.name}
                      </span>
                    </a>
                  </Link>
                ))}
            </div>
          </div>
          <div className="col-7 px-0 text-right">
            <Link href="/election-results/party-position/create-new-constituency">
              <a className="btn bg__green border-radius__15 px-3 py-2 mx-2 d-none">
                <span className="d-flex align-items-center py-1">
                  <span className="small text-white">Add New Constituency</span>
                  <img
                    src="/assets/img/icons/create-folder.svg"
                    width="15px"
                    className="ml-2"
                  />
                </span>
              </a>
            </Link>
            <Link
              href={
                `/election-results/party-position/${assembly}?view=${
                  view == "map" ? "table" : "map"
                }` + (province ? `&province=${province}` : "")
              }
            >
              <a className="btn bg__green border-radius__15 px-3 py-2 mx-2">
                <span className="d-flex align-items-center py-1">
                  <img
                    src={`/assets/img/icons/${
                      view == "map" ? "tableview" : "map"
                    }.svg`}
                    width="22px"
                    className="mr-2"
                  />
                  <span className="small text-white">
                    {view == "map" ? "Table" : "Map"} View
                  </span>
                </span>
              </a>
            </Link>
            <button
              className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2"
              disabled
            >
              <span className="d-flex align-items-center py-1">
                <span className="small text-dark font-opensans-regular">
                  Export
                </span>
                <img
                  src="/assets/img/icons/download.svg"
                  width="15px"
                  className="ml-2"
                />
              </span>
            </button>
            <button
              className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2"
              disabled
            >
              <span className="d-flex align-items-center py-1">
                <span className="small text-dark font-opensans-regular">
                  Print
                </span>
                <img
                  src="/assets/img/icons/printer.svg"
                  width="15px"
                  className="ml-2"
                />
              </span>
            </button>
          </div>
        </div>
        {view == "map" ? (
          <div className="row mx-0 mb-4">
            <div className="col-lg-8 pl-0 pr-1 h-100">
              <div className="overflow__auto bg-white border-radius__5 h-100 box-shadow__light">
                {constituency && latlng ? (
                  <PollingStationsResultMapView />
                  
                ) : (
                  <ConstituenciesResultMapView />
                  
                )}
              </div>
            </div>
            <div className="col-lg-4 pr-0">
              {constituency && latlng ? (
                <PollingStationsList />
                
              ) : (
                <WinningPoliticalPartiesInConstituenciesList />
                
              )}
            </div>
          </div>
        ) : (
          <div className="bg-white border-radius__5 small pt-4 pb-3 mb-5">
            <ConstituencyWithTopResult />
          </div>
        )}
      </main>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    latlng: state.site.latlng,
  };
};

const mapDispatchToProps = {};

PartyPosition.layout = "L";
export default connect(mapStateToProps, mapDispatchToProps)(PartyPosition);
