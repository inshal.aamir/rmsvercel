
import Image from 'next/image'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import HeadContent from '../../../components/partials/HeadContent'
import  { Redirect } from 'react-router-dom'
import { useRouter } from 'next/router'
import axios from 'axios'
import '../../../components/config.js'
import Breadcrumbs from '../../../components/widgets/Breadcrumbs'

const CreateNewConstituency = () => {
    
    var FormData = require('form-data')
    
    const router = useRouter()

    const [NAConstituencyNumber, setNAConstituencyNumber] = useState("NA Constituency Number"),
        [NAConstituencyName, setNAConstituencyName] = useState("NA Constitiency Name"),
        [NAMaleVoters, setNAMaleVoters] = useState("NA Male Voters"),
        [NAFemaleVoters, setNAFemaleVoters] = useState("NA Female Voters"),
        [NABarcode, setNABarcode] = useState("NA Barcode"),
        [PAConstituencyNumber, setPAConstituencyNumber] = useState("PA Constituency Number"),
        [PAConstituencyName, setPAConstituencyName] = useState("PA Constituency Name"),
        [PAMaleVoters, setPAMaleVoters] = useState("PA Male Voters"),
        [PAFemaleVoters, setPAFemaleVoters] = useState("PA Female Voters"),
        [PABarcode, setPABarcode] = useState("PA Barcode"),
        
        [NACandidateList, setNACandidateList] = useState([]),
        [PACandidateList, setPACandidateList] = useState([]),

        [preview, setPreview] = useState(null),
        [loading, setLoading] = useState(false)
    
    const [errors, setErrors] = useState({
        username: null,
        email: null,
        password: null,
        email: null
    })
    function addCandidatesList(candidates) {
        console.log(candidates, "canidates....")

        candidates.forEach(async (candidate) => {

            var data = new FormData();            
            data.append("name", candidate.name)
            data.append("constituency", candidate.constituency)
            data.append("image", candidate.file)


            var config = {
                method: 'post',
                url: `${process.env.BASE_URL}api/config/store-image`,
                headers: { 
                    'Authorization': localStorage.getItem("access_token"), 
                },
                data
            };
            
            let res = await axios(config)
            console.log(res, "candiate response...")
            // .then(function (response) {
            //     if (response && response.status == 200) {
            //         console.log(response, "candidate response add...")
            //     }
            //     console.log(response)
            // })
            // .catch((error) => {
            //     console.log(error.response, "error.response")
            //     if (error && error.response && error.response.data) {
            //         let err = error.response.data
            //         if (err.messages && err.messages.length > 0) {
            //             let msg = err.messages[0]["message"]
            //             alert (msg)
            //         } else {
            //             setErrors(error.response.data)
            //         }
            //     }
            // })              
        })

        setLoading(false)
        alert("Configuration Added Successfully")
    }
    function createNewConstituency(e) {
        e.preventDefault()
        setLoading(true)
        setErrors({})
        
        var data = new FormData();
        data.append('na_contituency_num', NAConstituencyNumber);
        data.append('na_contituency_name', NAConstituencyName);
        data.append('na_male_voters', NAMaleVoters);
        data.append('na_female_voters', NAFemaleVoters);
        data.append('pa_contituency_name', PAConstituencyName);
        data.append('pa_contituency_num', PAConstituencyNumber);
        data.append('pa_male_voters', PAMaleVoters);
        data.append('pa_female_voters', PAFemaleVoters);
        data.append('pa_barcode', PABarcode);
        data.append('na_barcode', NABarcode);
        
        var config = {
            method: 'post',
            url: `${process.env.BASE_URL}api/config/`,
            headers: { 
                'Authorization': localStorage.getItem("access_token"), 
            },
            data
        };
        
        axios(config)
        .then(function (response) {
            if (response && response.status == 200) {
                addCandidatesList(NACandidateList.concat(PACandidateList))

            } else {                
                setLoading(false)
            }
        })
        .catch((error) => {
            console.log(error.response, "error.response")
            if (error && error.response && error.response.data) {
                let err = error.response.data
                if (err.messages && err.messages.length > 0) {
                    let msg = err.messages[0]["message"]
                    alert (msg)
                } else {
                    setErrors(error.response.data)
                }
            }
            setLoading(false)
        })    
    }

    return (
        <div>
            <HeadContent 
                title="User Detail"
                desc="This is a dummy description that will be changed later." />

            <main>
                <Breadcrumbs />
                <form action="" onSubmit={createNewConstituency} className="w-100 bg-white border-radius__5 pt-4 mt-3 mb-5">
                    <h6 className="border-bottom px-3 pb-2 font-opensans-bold text__grey4">CONFIGURATION FORM</h6>
                    <div className="container px-0 my-4">
                        <div className="row mx-0">
                            <div className="col-12 mt-5">
                                <div className="row mx-0 align-items-center mb-4 pb-2 d-none">
                                    <div className="col-lg-6 px-5">
                                        <p className="mb-1 small text__dark3 px-1 font-opensans-semibold">Election Date</p>
                                        <div className="input-group">
                                            <input 
                                                type="text"  
                                                className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="mm / dd / yyyy"/>
                                        </div>                                      
                                        {/* <small className="ml-2 form-text text__danger">Username is invalid</small> */}
                                    </div>
                                </div>
                                
                                <div className="row mx-0 align-items-center mb-4 pb-2">
                                    <div className="col-lg-6 px-5">
                                        <p className="mb-1 text__dark3 px-1 font-opensans-semibold small">National Election Constituency</p>
                                        <div className="input-group">
                                            <input 
                                                type="text" 
                                                value={NAConstituencyName} 
                                                onChange={(e) => setNAConstituencyName(e.target.value)}
                                                required
                                                className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="NA Constituency Name"/>
                                        </div>                                      
                                        {/* <small className="ml-2 form-text text__danger">Username is invalid</small> */}
                                    </div>
                                    <div className="col-lg-6 px-5">
                                        <p className="mb-1 text__dark3 px-1 font-opensans-semibold small">Provincical Election Constituency</p>
                                        <div className="input-group">
                                            <input 
                                                type="text" 
                                                value={PAConstituencyName} 
                                                onChange={(e) => setPAConstituencyName(e.target.value)}
                                                required
                                                className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="PA Constituency Name"/>
                                        </div>                                      
                                        {/* <small className="ml-2 form-text text__danger">Username is invalid</small> */}
                                    </div>
                                </div>
                                
                                <div className="row mx-0 align-items-center  mb-4 pb-2">
                                    <div className="col-lg-6 px-5">
                                        <p className="mb-1 text__dark3 px-1 font-opensans-semibold small">National Constituency Number</p>
                                        <div className="input-group">
                                            <input 
                                                type="text" 
                                                value={NAConstituencyNumber} 
                                                onChange={(e) => setNAConstituencyNumber(e.target.value)}
                                                required
                                                className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="Enter Number Here NA"/>
                                        </div>                                      
                                        {/* <small className="ml-2 form-text text__danger">Username is invalid</small> */}
                                    </div>
                                    <div className="col-lg-6 px-5">
                                        <p className="mb-1 text__dark3 px-1 font-opensans-semibold small">Provincial Constituency Number</p>
                                        <div className="input-group">
                                            <input 
                                                type="text" 
                                                value={PAConstituencyNumber} 
                                                onChange={(e) => setPAConstituencyNumber(e.target.value)}
                                                required
                                                className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="Enter Number Here PA"/>
                                        </div>                                      
                                        {/* <small className="ml-2 form-text text__danger">Username is invalid</small> */}
                                    </div>
                                </div>

                                <div className="row mx-0 align-items-center mb-4 pb-3">
                                    <div className="col-lg-6 px-5">
                                        <div className="row mx-0 align-items-center">
                                            <div className="col-lg-6 pl-0">
                                                <p className="mb-1 text__dark3 px-1 font-opensans-semibold small">Male Voters</p>
                                                <div className="input-group">
                                                    <input 
                                                        type="text" 
                                                        value={NAMaleVoters} 
                                                        onChange={(e) => setNAMaleVoters(e.target.value)}
                                                        required
                                                        className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="NA-Male"/>
                                                </div>                                      
                                                {/* <small className="ml-2 form-text text__danger">Username is invalid</small> */}
                                            </div>
                                            <div className="col-lg-6 pr-0">
                                                <p className="mb-1 text__dark3 px-1 font-opensans-semibold small">Female Voters</p>
                                                <div className="input-group">
                                                    <input 
                                                        type="text" 
                                                        value={NAFemaleVoters} 
                                                        onChange={(e) => setNAFemaleVoters(e.target.value)}
                                                        required
                                                        className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="NA-Female"/>
                                                </div>                                      
                                                {/* <small className="ml-2 form-text text__danger">Username is invalid</small> */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 px-5">
                                        <div className="row mx-0 align-items-center">
                                            <div className="col-lg-6 pl-0">
                                                <p className="mb-1 text__dark3 px-1 font-opensans-semibold small">Male Voters</p>
                                                <div className="input-group">
                                                    <input 
                                                        type="text" 
                                                        value={PAMaleVoters} 
                                                        onChange={(e) => setPAMaleVoters(e.target.value)}
                                                        required
                                                        className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="PA-Male"/>
                                                </div>                                      
                                                {/* <small className="ml-2 form-text text__danger">Username is invalid</small> */}
                                            </div>
                                            <div className="col-lg-6 pr-0">
                                                <p className="mb-1 text__dark3 px-1 font-opensans-semibold small">Female Voters</p>
                                                <div className="input-group">
                                                    <input 
                                                        type="text" 
                                                        value={PAFemaleVoters} 
                                                        onChange={(e) => setPAFemaleVoters(e.target.value)}
                                                        required
                                                        className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="PA-Female"/>
                                                </div>                                      
                                                {/* <small className="ml-2 form-text text__danger">Username is invalid</small> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mx-0 align-items-center mb-4 pb-2">
                                    <div className="col-lg-6 text-center px-4 align-self-start">
                                        <div className={"text-left " + (NACandidateList.length > 0 ? "border-top pt-4 " : "")}>
                                            {NACandidateList && NACandidateList.map((candidate, index) => (
                                                <div key={index} className="row mx-0 align-items-center mb-3 hover__zoom__1">
                                                    <div className="input-group col-10 pr-0">
                                                        <div className="input-group-prepend position-relative zoom__0 transition__1">
                                                            <span
                                                                onClick={() => (setNACandidateList(NACandidateList => NACandidateList.filter((_, i) => i !== index)))} 
                                                                style={{ left: "-32px"}}
                                                                className="input-group-text bg-white text__danger mdi mdi-close-circle mt-1 btn p-0 bg-transparent border-0 mdi-24px position-absolute"></span>
                                                        </div>                                                        
                                                        <input 
                                                            type="text" 
                                                            value={candidate.name} 
                                                            required
                                                            onChange={(e) => {
                                                                let newCandidateList = [...NACandidateList]
                                                                newCandidateList[index]["name"] = e.target.value
                                                                setNACandidateList(newCandidateList)
                                                            }}
                                                            
                                                            className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="Enter NA Candidate Name"/>
                                                    </div>                                      
                                                    <div className="upload-btn-wrapper col-2 text-right">
                                                        <button type="button" className="btn bg-white ml-3  p-0 overflow__hidden" style={{ width: "50px"}}>
                                                            {candidate.image ? 
                                                            <img src={candidate.image} width="100%" className="border p-1 border-radius__5 border__light bg__light" />
                                                            : 
                                                                <span className="mdi mdi-image-plus mdi-24px text__grey4"></span>
                                                            }
                                                        </button>                            
                                                        <input
                                                            title="Add Candidate Logo"
                                                            onChange={(e) => {
                                                                if(e.target.files.length > 0){
                                                                    var src = URL.createObjectURL(e.target.files[0]);
                                                                    let newCandidateList = [...NACandidateList]
                                                                    newCandidateList[index]["image"] = src
                                                                    newCandidateList[index]["file"] = e.target.files[0]
                                                                    setNACandidateList(newCandidateList)
                                                                }                                                                    
                                                            }}
                                                            type="file" name="myfile" accept="image/*" />
                                                    </div>      
                                                </div>
                                            ))}
                                        </div>
                                        <button 
                                            onClick={() => 
                                                setNACandidateList([...NACandidateList, {
                                                    image: null,
                                                    name: "",
                                                    constituency: "NA"
                                                }])
                                            }
                                            type="button" className="btn px-4 py-0 border-radius__15 bg-transparent text__green mx-2 w-100 mt-4">
                                            <span className="mdi mdi-plus"></span>
                                            <span className="px-2 font-opensans-semibold">Add New NA Candidate</span>
                                        </button>                                        
                                    </div>
                                    <div className="col-lg-6 text-center align-self-start px-4">
                                        <div className={"text-left " + (PACandidateList.length > 0 ? "border-top pt-4 " : "")}>
                                            {PACandidateList && PACandidateList.map((candidate, index) => (
                                                <div key={index} className="row mx-0 align-items-center mb-3 hover__zoom__1">
                                                    <div className="input-group col-10 pr-0">
                                                        <div className="input-group-prepend position-relative zoom__0 transition__1">
                                                            <span
                                                                onClick={() => (setPACandidateList(PACandidateList => PACandidateList.filter((_, i) => i !== index)))} 
                                                                style={{ left: "-32px"}}
                                                                className="input-group-text bg-white text__danger mdi mdi-close-circle mt-1 btn p-0 bg-transparent border-0 mdi-24px position-absolute"></span>
                                                        </div>    
                                                        <input 
                                                            type="text" 
                                                            value={candidate.name} 
                                                            required
                                                            onChange={(e) => {
                                                                let newCandidateList = [...PACandidateList]
                                                                newCandidateList[index]["name"] = e.target.value
                                                                setPACandidateList(newCandidateList)
                                                            }}
                                                            className="form-control border__light3 border-radius__15 py-2 px-3" placeholder="Enter PA Candidate Name"/>
                                                    </div>                                      
                                                    <div className="upload-btn-wrapper col-2 text-right">
                                                        <button type="button" className="btn bg-white ml-3  p-0 overflow__hidden" style={{ width: "50px"}}>
                                                            {candidate.image ? 
                                                            <img src={candidate.image} width="100%" className="border p-1 border-radius__5 border__light bg__light" />
                                                            : 
                                                                <span className="mdi mdi-image-plus mdi-24px text__grey4"></span>
                                                            }
                                                        </button>                            
                                                        <input
                                                            title="Add Candidate Logo"
                                                            onChange={(e) => {
                                                                if(e.target.files.length > 0){
                                                                    var src = URL.createObjectURL(e.target.files[0]);
                                                                    let newCandidateList = [...PACandidateList]
                                                                    newCandidateList[index]["image"] = src
                                                                    newCandidateList[index]["file"] = e.target.files[0]
                                                                    setPACandidateList(newCandidateList)
                                                                }                                                                    
                                                            }}
                                                            type="file" name="myfile" accept="image/*" />
                                                    </div>      
                                                </div>
                                            ))}
                                        </div>
                                        <button
                                            onClick={() => 
                                                setPACandidateList([...PACandidateList, {
                                                    image: null,
                                                    name: "",
                                                    constituency: "PA"
                                                }])
                                            } 
                                            type="button" className="btn px-4 py-0 border-radius__15 bg-transparent text__danger mx-2 mt-4 w-100">
                                            <span className="mdi mdi-plus"></span>
                                            <span className="px-2 font-opensans-semibold">Add New PA Candidate</span>
                                        </button>                                        
                                    </div>
                                </div>

                                {/* <div className="row mx-0 align-items-center  mb-4 pb-2 pt-2 d-none">
                                    <div className="col-lg-6 text-center">
                                        <div className="upload-btn-wrapper p-2">
                                            <button type="button" className="btn bg-white btn__icon__md bg__light4 ml-3 p-0 box-shadow__light overflow__hidden">
                                                {preview ? 
                                                <img src={preview} width="100%" />
                                                : 
                                                    <span className="mdi mdi-upload mdi-36px text__grey4"></span>
                                                }
                                            </button>                            
                                            <input
                                                onChange={showPreview} 
                                                type="file" name="myfile" accept="image/*" />
                                            <p className="small text-center mt-3 text__grey4 font-opensans-semibold">Upload National Assembly Icons</p>
                                        </div> 
                                    </div>
                                    <div className="col-lg-6 text-center">
                                        <div className="upload-btn-wrapper p-2">
                                            <button type="button" className="btn bg-white btn__icon__md bg__light4 ml-3 p-0 box-shadow__light overflow__hidden">
                                                {preview ? 
                                                    <img src={preview} width="100%" />
                                                : 
                                                    <span className="mdi mdi-upload mdi-36px text__grey4"></span>
                                                }
                                            </button>                            
                                            <input
                                                onChange={showPreview} 
                                                type="file" name="myfile" accept="image/*" />
                                            <p className="small text-center mt-3 text__grey4 font-opensans-semibold">Upload Provincial Assembly File</p>
                                        </div> 
                                    </div>
                                </div> */}

                            </div>
                        </div>
                    </div>
                    <div className="w-100 text-right px-4 py-3 mt-5 box-shadow__inset__light">
                        <Link href="/user-management">
                            <a className="btn px-4 py-2 border-radius__15 bg__danger text-white mx-2">
                                <span className="small px-2">Delete</span>
                            </a>
                        </Link>  
                        <button 
                            disabled={loading}
                            className="btn px-4 py-2 border-radius__15 bg__green text-white mx-2">
                            {loading ? 
                                <span className="mdi mdi-loading mdi-spin"></span>
                            : null}
                            <span className="small px-2">Create Constituency</span>
                        </button>

                    </div>
                </form>
            </main>
        </div>
    )
}
CreateNewConstituency.layout = "L"
export default CreateNewConstituency