import { useState } from "react";
import "../../components/config.js";
import HeadContent from "../../components/partials/HeadContent.js";
import PollingSchemeSummaryTable from "../../components/tables/PollingSchemeSummaryTable.js";
import PollingSchemeTable from "../../components/tables/PollingSchemeTable.js";
import { numberWithCommas } from "../../js/helpers.js";

const PollingScheme = () => {
  const [summaryOfPollingScheme] = useState([
    {
      province: "KP",
      maleVoters: "8193957",
      femaleVoters: "8193957",
      totalVoters: "8193957",
      malePollingStations: "4024",
      femalePollingStations: "4342",
      combinedPollingStations: "4563",
      totalPollingStations: "12064",
      malePollingBooths: "8193957",
      femalePollingBooths: "8193957",
      totalPollingBooths: "8193957",
    },
    {
      province: "FATA",
      maleVoters: "8193957",
      femaleVoters: "8193957",
      totalVoters: "8193957",
      malePollingStations: "4024",
      femalePollingStations: "4342",
      combinedPollingStations: "4563",
      totalPollingStations: "12064",
      malePollingBooths: "8193957",
      femalePollingBooths: "8193957",
      totalPollingBooths: "8193957",
    },
    {
      province: "ICT",
      maleVoters: "8193957",
      femaleVoters: "8193957",
      totalVoters: "8193957",
      malePollingStations: "4024",
      femalePollingStations: "4342",
      combinedPollingStations: "4563",
      totalPollingStations: "12064",
      malePollingBooths: "8193957",
      femalePollingBooths: "8193957",
      totalPollingBooths: "8193957",
    },
    {
      province: "Punjab",
      maleVoters: "8193957",
      femaleVoters: "8193957",
      totalVoters: "8193957",
      malePollingStations: "4024",
      femalePollingStations: "4342",
      combinedPollingStations: "4563",
      totalPollingStations: "12064",
      malePollingBooths: "8193957",
      femalePollingBooths: "8193957",
      totalPollingBooths: "8193957",
    },
    {
      province: "Sindh",
      maleVoters: "8193957",
      femaleVoters: "8193957",
      totalVoters: "8193957",
      malePollingStations: "4024",
      femalePollingStations: "4342",
      combinedPollingStations: "4563",
      totalPollingStations: "12064",
      malePollingBooths: "8193957",
      femalePollingBooths: "8193957",
      totalPollingBooths: "8193957",
    },
    {
      province: "Balochistan",
      maleVoters: "8193957",
      femaleVoters: "8193957",
      totalVoters: "8193957",
      malePollingStations: "4024",
      femalePollingStations: "4342",
      combinedPollingStations: "4563",
      totalPollingStations: "12064",
      malePollingBooths: "8193957",
      femalePollingBooths: "8193957",
      totalPollingBooths: "8193957",
    },
  ]);
  return (
    <div>
      <HeadContent
        title="Polling Scheme"
        desc="This is a dummy description that will be changed later."
      />

      <main>
        <h4 className="font-opensans-semibold my-1">
          POLLING SCHEME GENERAL ELECTION 2022
        </h4>
        <PollingSchemeSummaryTable />
        <PollingSchemeTable />
      </main>
    </div>
  );
};
PollingScheme.layout = "L";
export default PollingScheme;
