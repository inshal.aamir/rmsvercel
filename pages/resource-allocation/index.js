import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import HeadContent from "../../components/partials/HeadContent";
import SimpleDropdown from "../../components/widgets/SimpleDropdown";
import "../../components/config.js";
import axios from "axios";
import ResourceAllocationModal from "../../components/modals/ResourceAllocationModal";
import Pagination from "../../components/widgets/Pagination";

const Inventory = () => {
  const [list, setList] = useState([
      {
        id: "001",
        presidingOfficer: "Abid Iqbal",
        NumberAndNameOfConstituency: "PK-48-Mardan-I",
        pollingStationAllocated: "Govt.Primary School, Mian Essa (P)",
        role: "Returning Officer",
        status: { name: "Allocated", color: "green" },
      },
      {
        id: "002",
        presidingOfficer: "Jamal Sarwar",
        NumberAndNameOfConstituency: "PK-48-Mardan-I",
        pollingStationAllocated: "Govt.Higher Secondary School, No.1 ...",
        role: "Returning Officer",
        status: { name: "In Pending", color: "orange" },
      },
      {
        id: "003",
        presidingOfficer: "Mehk Arshad",
        NumberAndNameOfConstituency: "PK-48-Mardan-I",
        pollingStationAllocated:
          "Govt.Girls Primary School, Serai Minagano... ",
        role: "Returning Officer",
        status: { name: "Allocated", color: "green" },
      },
      {
        id: "004",
        presidingOfficer: "Abdullah",
        NumberAndNameOfConstituency: "PK-48-Mardan-I",
        pollingStationAllocated: "Govt.Primary School, Mosafar Kalay",
        role: "Returning Officer",
        status: { name: "Allocated", color: "green" },
      },
      {
        id: "005",
        presidingOfficer: "Yonus Zafar",
        NumberAndNameOfConstituency: "PK-48-Mardan-I",
        pollingStationAllocated: "Govt.Girls Primary School, Shekhano Abad...",
        role: "Returning Officer",
        status: { name: "In Pending", color: "orange" },
      },
      {
        id: "006",
        presidingOfficer: "Anis Ahmad",
        NumberAndNameOfConstituency: "PK-48-Mardan-I",
        pollingStationAllocated: "Govt.Primary School, Arif Khan Killi",
        role: "Returning Officer",
        status: { name: "In Pending", color: "orange" },
      },
      {
        id: "007",
        presidingOfficer: "Imran Akhtar",
        NumberAndNameOfConstituency: "PK-48-Mardan-I",
        pollingStationAllocated: "Govt.Primary School, Tazagram (P) Male",
        role: "Returning Officer",
        status: { name: "Allocated", color: "green" },
      },
    ]),
    [loading, setLoading] = useState(false),
    [pageno, setPageno] = useState(1),
    router = useRouter();
  const [search, setSearch] = useState([]);

  function getUsersList() {
    let path = router.asPath.split("?")[1] || "page_number=1";
    setPageno(parseInt(path.split("=")[1]));

    var config = {
      method: "get",
      url: `http://192.168.22.200:9001/api/get-users/?${path}`,
      headers: {
        Authorization: localStorage.getItem("access_token"),
      },
    };

    axios(config)
      .then(function (response) {
        if (response.data && response.data.length > 0) {
          setList(response.data);
          setLoading(false);
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.detail) {
          alert(error.response.data.detail);
        } else {
          alert("No More Records found... Redireting to Page 1");
          router.push({ pathname: `/user-management` });
        }
      });
  }

  return (
    <div>
      <HeadContent
        title="Inventory"
        desc="This is a dummy description that will be changed later."
      />

      <main>
        {/* Modals */}
        <ResourceAllocationModal />
        <div className="row mx-0 my-4 align-items-center">
          <div className="col-5">
            <div
              className="input-group mr-4 bg-white border-radius__15 border__light"
              style={{ width: "308px" }}
            >
              <div className="input-group-prepend border-0">
                <span className="input-group-text bg-transparent pl-3 pr-1 border-0">
                  <img src="/assets/img/icons/search.svg" width="16px" />
                </span>
              </div>
              <input
                style={{ borderRadius: "0px 35px 35px 0px" }}
                type="text"
                className="form-control border-0 bg-transparent py-2"
                placeholder="Search"
                aria-label="Username"
                aria-describedby="basic-addon1"
                onChange={(e) => {
                  if (e.target.value) {
                    const searchUser = users.filter((val) =>
                      val.presidingOfficer.includes(e.target.value)
                    );
                    // console.log(searchUser, "seach user");
                    // console.log(users, " user");
                    setSearch(searchUser);
                  } else {
                    setSearch([]);
                  }
                }}
              />
            </div>
          </div>
          <div className="col-7 text-right">
            <div className="dropdown d-inline-block">
              <button
                className="btn bg-white border__light2 border-radius__15 px-3 py-1 mx-2"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                disabled
              >
                <span className="d-flex align-items-center py-2">
                  <span className="small text-dark font-opensans-regular mr-1">
                    Import
                  </span>
                  {/* <span className="mdi mdi-chevron-down"></span> */}
                </span>
              </button>
              <SimpleDropdown
                list={[
                  { title: "Save Post", className: "text__dark border-bottom" },
                  { title: "Unfollow", className: "text__dark border-bottom" },
                  { title: "Report", className: "text__danger border-bottom" },
                  {
                    title: "Copy Post Link",
                    className: "text__dark border-bottom",
                  },
                  { title: "Cancel", className: "text__dark" },
                ]}
              />
            </div>
            <button
              className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2"
              disabled
            >
              <span className="d-flex align-items-center py-1">
                <span className="small text-dark font-opensans-regular">
                  Print
                </span>
                <img
                  src="/assets/img/icons/printer.svg"
                  width="15px"
                  className="ml-2"
                />
              </span>
            </button>
            <Link href="/user-management/create-new-user">
              <a className="btn bg__green border-radius__15 px-3 py-2 mx-2">
                <span className="d-flex align-items-center py-1">
                  <span className="small text-white">Add New Officer</span>
                  <img
                    src="/assets/img/icons/create-folder.svg"
                    width="15px"
                    className="ml-2"
                  />
                </span>
              </a>
            </Link>
          </div>
        </div>
        {loading ? (
          <p className="d-flex p-5 text-center w-100 align-items-center justify-content-center my-5">
            <span className="mdi mdi-loading mdi-spin mdi-24px text__green mr-3"></span>
            <span className="text-muted small">Loading...</span>
          </p>
        ) : !loading && list.length > 0 ? (
          <>
            <table className="w-100 bg-white border-radius__5 small">
              <thead className="border-bottom">
                <tr>
                  <th className="pb-3 pt-4 pl-4">ID</th>
                  <th className="pb-3 pt-4">Presiding Officer</th>
                  <th className="pb-3 pt-4">No. and Name of Constituency</th>
                  <th className="pb-3 pt-4">Polling Station Allocated</th>
                  <th className="pb-3 pt-4">Role</th>
                  <th className="pb-3 pt-4 text-center">Status</th>
                  <th className="pb-3 pt-4 px-4 text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                {!(search.length > 0)
                  ? list &&
                    list.map((item, key) => (
                      <tr key={key} className="border-bottom">
                        <td className="py__10px pl-4">{item.id}</td>
                        <td className="py__10px">{item.presidingOfficer}</td>
                        <td className="py__10px">
                          {item.NumberAndNameOfConstituency}
                        </td>
                        <td className="py__10px">
                          {item.pollingStationAllocated}
                        </td>
                        <td className="py__10px">{item.role}</td>
                        <td
                          className={
                            "py__10px text-center font-opensans-medium text__" +
                            item.status.color
                          }
                        >
                          {item.status.name}
                        </td>
                        <td className="py__10px px-4 d-flex justify-content-center align-items-center">
                          <Link href={`${router.pathname}/${item.id}`}>
                            <a className="btn mx-1 p-0 bg-transparent text-dark">
                              <span className="small font-opensans-regular">
                                View
                              </span>
                            </a>
                          </Link>
                          <span className="text__grey3">|</span>
                          <Link href={`${router.pathname}/edit/${item.id}`}>
                            <a className="btn mx-1 p-0 bg-transparent text-dark">
                              <span className="small font-opensans-regular">
                                Edit
                              </span>
                            </a>
                          </Link>
                          <span className="text__grey3">|</span>
                          <button
                            className="btn mx-1 p-0 bg-transparent"
                            data-toggle="modal"
                            data-target="#resourceAllocationModal"
                          >
                            <span className="small font-opensans-regular">
                              Allocate
                            </span>
                          </button>
                        </td>
                      </tr>
                    ))
                  : list &&
                    list.map((item, key) => (
                      <tr key={key} className="border-bottom">
                        <td className="py__10px pl-4">{item.id}</td>
                        <td className="py__10px">{item.presidingOfficer}</td>
                        <td className="py__10px">
                          {item.NumberAndNameOfConstituency}
                        </td>
                        <td className="py__10px">
                          {item.pollingStationAllocated}
                        </td>
                        <td className="py__10px">{item.role}</td>
                        <td
                          className={
                            "py__10px text-center font-opensans-medium text__" +
                            item.status.color
                          }
                        >
                          {item.status.name}
                        </td>
                        <td className="py__10px px-4 d-flex justify-content-center align-items-center">
                          <Link href={`${router.pathname}/${item.id}`}>
                            <a className="btn mx-1 p-0 bg-transparent text-dark">
                              <span className="small font-opensans-regular">
                                View
                              </span>
                            </a>
                          </Link>
                          <span className="text__grey3">|</span>
                          <Link href={`${router.pathname}/edit/${item.id}`}>
                            <a className="btn mx-1 p-0 bg-transparent text-dark">
                              <span className="small font-opensans-regular">
                                Edit
                              </span>
                            </a>
                          </Link>
                          <span className="text__grey3">|</span>
                          <button
                            className="btn mx-1 p-0 bg-transparent"
                            data-toggle="modal"
                            data-target="#resourceAllocationModal"
                          >
                            <span className="small font-opensans-regular">
                              Allocate
                            </span>
                          </button>
                        </td>
                      </tr>
                    ))}
                <Pagination />
              </tbody>
            </table>
          </>
        ) : (
          <p className="p-5 text-center w-100 my-5">
            <span className="text-muted small">No Users Found!</span>
          </p>
        )}
      </main>
    </div>
  );
};
Inventory.layout = "L";
export default Inventory;
