
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import HeadContent from '../../../components/partials/HeadContent'
import SimpleDropdown from '../../../components/widgets/SimpleDropdown'
import '../../../components/config.js'
import axios from 'axios'
import ResourceAllocationDetailTable from '../../../components/tables/ResourceAllocationDetailTable'

const ResourceAllocationEdit = () => {
    const [list, setList] = useState([
        { id: "001", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "002", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "003", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "004", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "005", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "006", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "007", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
        { id: "008", controlUnit: "CU-001", balloutUnit: "BU-001", printers: "P-001", printerRolls: "PR-001", dataCables: "D-001", batteryCover: "Cover-001", batteryCharger: "Charger-001" },
    ]),
        [loading, setLoading] = useState(false),
        [pageno, setPageno] = useState(1),
        router = useRouter()

    function getUsersList() {
        let path = router.asPath.split("?")[1] || "page_number=1"
        setPageno(parseInt(path.split("=")[1]))

        var config = {
          method: 'get',
          url: `http://192.168.22.200:9001/api/get-users/?${path}`,
          headers: { 
            'Authorization': localStorage.getItem("access_token"), 
          },
        };
        
        axios(config)
        .then(function (response) {
            if (response.data && response.data.length > 0) {
                setList(response.data)
                setLoading(false)
            }
        })
        .catch((error) => {
            if (error.response && error.response.data.detail) {
                alert(error.response.data.detail)
            } else {     
                alert("No More Records found... Redireting to Page 1")           
                router.push({ pathname: `/user-management` })
            }
        })    

    }

    useEffect(() => {
    }, [])
    return (
        <div>
            <HeadContent 
                title="Inventory"
                desc="This is a dummy description that will be changed later." />

            <main>
                <div className="row mx-0 my-4 align-items-center">
                    <div className="col-7">
                        <h4 className="text-dark font-opensans-semibold mb-0 text-uppercase">Presiding Officer  -  Abid Iqbal (PK-48-Mardan-I)</h4>
                    </div>
                    <div className="col-5 text-right">

                            <div className="dropdown d-inline-block">
                                <button className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2" disabled  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span className="d-flex align-items-center py-1">
                                        <span className="small text-dark font-opensans-regular mr-1">Export</span>
                                        <span className="mdi mdi-chevron-down d-none"></span>
                                    </span>
                                </button>                             
                                <SimpleDropdown 
                                    list={
                                        [
                                            { title: "Save Post", className: "text__dark border-bottom" },
                                            { title: "Unfollow", className: "text__dark border-bottom" },
                                            { title: "Report", className: "text__danger border-bottom" },
                                            { title: "Copy Post Link", className: "text__dark border-bottom" },
                                            { title: "Cancel", className: "text__dark" },
                                        ]
                                    }    
                                />
                            </div>    
                                <button className="btn bg-white border__light2 border-radius__15 px-3 py-2 mx-2" disabled>
                                    <span className="d-flex align-items-center py-1">
                                        <span className="small text-dark font-opensans-regular">Print</span>
                                        <img src="/assets/img/icons/printer.svg" width="15px" className="ml-2" />
                                    </span>
                                </button>
                    </div>
                </div>
                <div className="border row mx-0  bg-white border-radius__5 mb-5">
                    <div className="col-lg-4 pl-4 py-4 overflow__auto ">
                        <div className="px-4" style={{ minWidth: "500px" }}>
                            <div className="mb-4">
                                <p className="text__dark3 mb-1 px-2 font-opensans-medium small">Constituency</p>
                                <div className="w-75">
                                    <select 
                                        className="custom-select border__light3 border-radius__15 px-3" id="gender2">
                                        <option>PK-48-Mardan-I</option>
                                        <option value="1">PK-48-Mardan-II</option>
                                        <option value="2">PK-48-Mardan-III</option>
                                    </select>
                                </div>                         
                            </div>
                            <div>
                                <p className="text__dark3 mb-1 px-2 font-opensans-medium small">Polling Station Allocated</p>
                                <div className="w-75">
                                    <select 
                                        className="custom-select border__light3 border-radius__15 px-3" id="gender2">
                                        <option>Govt.Primary School, Mian Essa </option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>                    
                            </div>
                            <div className="border my-5 px-3 pt-4 pb-5 border-radius__5">
                                <h6 className="font-opensans-semibold mb-4 px-2">Resource Allocation</h6>
                                <div className="row mx-0">
                                    <div className="col-4 px-2 mb-3">
                                        <p className="text__dark3 mb-1 px-1 font-opensans-medium small">Control Unit</p>
                                        <div className="input-group px-1">
                                            <input 
                                                type="text" 
                                                required
                                                className="form-control border__light3 border-radius__10 py-2 px-3" placeholder="25" />
                                        </div>                         
                                    </div>
                                    <div className="col-4 px-2 mb-3">
                                        <p className="text__dark3 mb-1 px-2 font-opensans-medium small">Ballot Unit</p>
                                        <div className="input-group px-1">
                                            <input 
                                                type="text" 
                                                required
                                                className="form-control border__light3 border-radius__10 py-2 px-3" placeholder="25" />
                                        </div>                         
                                    </div>
                                    <div className="col-4 px-2 mb-3">
                                        <p className="text__dark3 mb-1 px-2 font-opensans-medium small">Printers ssssss</p>
                                        <div className="input-group px-1">
                                            <input 
                                                type="text" 
                                                required
                                                className="form-control border__light3 border-radius__10 py-2 px-3" placeholder="25" />
                                        </div>                         
                                    </div>
                                    <div className="col-4 px-2 mb-3">
                                        <p className="text__dark3 mb-1 px-2 font-opensans-medium small">Printer Rolls</p>
                                        <div className="input-group px-1">
                                            <input 
                                                type="text" 
                                                required
                                                className="form-control border__light3 border-radius__10 py-2 px-3" placeholder="25" />
                                        </div>                         
                                    </div>
                                    <div className="col-4 px-2 mb-3">
                                        <p className="text__dark3 mb-1 px-2 font-opensans-medium small">Data Cables</p>
                                        <div className="input-group px-1">
                                            <input 
                                                type="text" 
                                                required
                                                className="form-control border__light3 border-radius__10 py-2 px-3" placeholder="25" />
                                        </div>                         
                                    </div>
                                    <div className="col-4 px-2 mb-3">
                                        <p className="text__dark3 mb-1 px-2 font-opensans-medium small">Battery Cover</p>
                                        <div className="input-group px-1">
                                            <input 
                                                type="text" 
                                                required
                                                className="form-control border__light3 border-radius__10 py-2 px-3" placeholder="25" />
                                        </div>                         
                                    </div>
                                    <div className="col-4 px-2 mb-3">
                                        <p className="text__dark3 mb-1 px-2 font-opensans-medium small">Battery Charger</p>
                                        <div className="input-group px-1">
                                            <input 
                                                type="text" 
                                                required
                                                className="form-control border__light3 border-radius__10 py-2 px-3" placeholder="25" />
                                        </div>                         
                                    </div>
                                    <div className="col-12 px-2 mb-3 mt-4">
                                        <button className="btn bg__dark3 border-radius__4 text__grey5 btn-block d-flex w-100 align-items-center justify-content-center">
                                            <span className="mdi mdi-plus mdi-24px mr-2"></span>
                                            <span className="font-opensans-semibold">Add another item</span>
                                        </button>   
                                        <div className="row mx-0 mt-4">
                                            <div className="col-6 px-0">
                                                <button className="btn bg-transparent text__green mr-3 border-radius__10 px-2 py-2 font-opensans-semibold">Allocate items equally</button>
                                            </div>
                                            <div className="col-6 px-0">
                                                <button className="btn text__grey5 bg-transparent font-opensans-semibold  border-radius__10 px-2 py-2">Total Allocation &nbsp; 100%</button>
                                            </div>
                                        </div>                                     
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="col-lg-8 pl-5">
                        {loading ?
                            <p className="d-flex p-5 text-center w-100 align-items-center justify-content-center my-5">
                                <span className="mdi mdi-loading mdi-spin mdi-24px text__green mr-3"></span>
                                <span className="text-muted small">Loading...</span>
                            </p> 
                        : !loading && list.length > 0 ?
                            <>
                                <ResourceAllocationDetailTable />               
                            </>
                        : 
                            <p className="p-5 text-center w-100 my-5">
                                <span className="text-muted small">No Users Found!</span>
                            </p> 
                        }
                    </div>

                    {/* footer */}
                    <div className="col-12 w-100 text-right px-4 py-3 mt-5 box-shadow__inset__light">
                        <Link href={`/resource-allocation`}>
                            <a className="btn px-4 py-2 border-radius__15 bg-white text-dark mx-2 border">
                                <span className="small px-2">Cancel</span>
                            </a>
                        </Link>  
                        <button 
                            disabled={loading}
                            className="btn px-4 py-2 border-radius__15 bg__green text-white mx-2">
                            {loading ? 
                                <span className="mdi mdi-loading mdi-spin"></span>
                            : null}
                            <span className="small px-2">Save Information</span>
                        </button>

                    </div>                        
                </div>
            </main>
        </div>
    )
}
ResourceAllocationEdit.layout = "L"
export default ResourceAllocationEdit