import Layout from "../components/layout/layout";
import LayoutRegistration from "../components/layout/LayoutRegistration";
import { wrapper } from "../redux/store";
import { Helmet } from "react-helmet";

// import CSS
import "../public/assets/style/bg.css";
import "../public/assets/style/fontsize.css";
import "../public/assets/style/letterspacing.css";
import "../public/assets/style/lineheight.css";
import "../public/assets/style/fontfamily.css";
import "../public/assets/style/textcolor.css";
import "../public/assets/style/button.css";
import "../public/assets/style/shadow.css";
import "../public/assets/style/banner.css";
import "../public/assets/style/body.css";
import "../public/assets/style/form.css";
import "../public/assets/style/border.css";
import "../public/assets/style/round-profile-pic.css";
import "../public/assets/style/overflow.css";
import "../public/assets/style/scrollbar.css";
import "../public/assets/style/lib/swiper.css";
import "../public/assets/style/pulse-button.css";
import "../public/assets/style/padding.css";
import "../public/assets/style/dropdown.css";
import "../public/assets/style/hover.css";
import "../public/assets/style/textprops.css";
import "../public/assets/style/width.css";
import "../public/assets/style/checkboxv2.css";
import "../public/assets/style/cursor.css";
import "../public/assets/style/transition.css";
import "../public/assets/style/zindex.css";
import "../public/assets/style/mediaqueries/lg-queries.css";
import "../public/assets/style/toggle-switch.css";
import "../public/assets/style/resize.css";
import "../public/assets/style/height.css";
import "../public/assets/style/margin.css";
import "../public/assets/style/mdi.css";
import "../public/assets/style/transform.css";
import "../public/assets/style/leaflet.css";
import "../public/assets/style/active.css";
import '../styles/nprogress.css'

import { useState, useEffect } from "react";
import Router, { useRouter } from "next/router";
import "../components/config.js";
import LayoutNone from "../components/layout/LayoutNone";
import LoadingScreen from "../components/widgets/LoadingScreen";
import ApiResponseModal from "../components/modals/ApiResponseModal";
import { connect } from "react-redux";
import { authenticateUser } from "../redux/actions/auth";
import { setRedirectPath } from "../redux/actions/site";
import Script from "next/script";
import NProgress from "nprogress"
import Head from "next/head"

const layouts = {
    L: Layout,
    LayoutRegistration: LayoutRegistration,
    LayoutNone: LayoutNone,
};

// function Loading(){
//     const router= useRouter();
//     const [loading, setLoading] = useState(false);
//     useEffect(()=>{
//         const handleStart= (url) => (url!==router.asPath) && setLoading(true)
//         const handleComplete= (url) => (url===router.asPath) && setLoading(false)

//         router.events.on('routeChangeStart', handleStart)
//         router.events.on('routeChangeComplete', handleComplete)
//         router.events.on('routeChangeError', handleComplete)

//         return () => {
//             router.events.off('routeChangeStart', handleStart)
//             router.events.off('routeChangeComplete', handleComplete)
//             router.events.off('routeChangeError', handleComplete)
//         }
//     })
//     return loading && (
//         <div style={{zIndex:50}}>
//             Loading...
//         </div>
//     )
// }

Router.events.on("routeChangeStart", NProgress.start);
Router.events.on("routeChangeError", NProgress.done);
Router.events.on("routeChangeComplete", NProgress.done);

function MyApp({ Component, pageProps, authenticateUser, loading2, redirect }) {
    const router = useRouter(), Layout = layouts[Component.layout] || ( ({children})  => { 
        return <>{children}</>}) , { asPath } = router
    

    useEffect(() => authenticateUser(), [])
    useEffect(() => {
        if (redirect && location.pathname != redirect) {
            router.push({ pathname: redirect })
            setRedirectPath(null)
        }

    }, [redirect])
    
    if (loading2) 
        return (
            <LoadingScreen />
        )    
    else 
        return (
            <>
            <Head>
               <link
                rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs    /nprogress/0.2.0/nprogress.min.css"
            />
            </Head>
                <Layout>
                    
                    {/* MODALS */}
                    <ApiResponseModal />

                    <Component {...pageProps} key={router.asPath} />    

                </Layout>         
                {/* EXTERNAL SCRIPS */}
                <Script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js" type="text/javascript"></Script>
                <Script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" type="text/javascript"></Script>
                <Script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" type="text/javascript"></Script>            
            </>
        )
}


const mapStateToProps = state => {
    return { 
        loading2: state.site.loading2,
        redirect: state.site.redirect
    }
}

const mapDispatchToProps = {
    authenticateUser, setRedirectPath
}

export default wrapper.withRedux(connect(mapStateToProps, mapDispatchToProps)(MyApp))






    // function getAccessToken() {
    //     let refresh = localStorage.getItem("refresh_token");

    //     if (refresh) {
    //     var config = {
    //         method: "post",
    //         url: `${process.env.BASE_URL}api/token/refresh/`,
    //         headers: {
    //         "Content-Type": "application/json",
    //         },
    //         data: { refresh },
    //     };

    //     axios(config)
    //         .then(async (response) => {
    //         if (response.data && response.data.access) {
    //             await localStorage.setItem(
    //             "access_token",
    //             `ems ${response.data.access}`
    //             );
    //         }
    //         setTimeout(() => setLoading(false), 999);
    //         })
    //         .catch((error) => {
    //         if (error.response && error.response.status == 401) {
    //             // alert(error.response.data.detail, "please see _app.js line 79")
    //             router.push({ pathname: "/register/login" });
    //         }
    //         setTimeout(() => setLoading(false), 999);
    //         console.log(error.response, "error.response");
    //         });
    //     } else {
    //     setTimeout(() => setLoading(false), 999);
    //     }
    // }
    // useEffect(() => {
    //     let accessUser = localStorage.getItem("access_user");
    //     if (accessUser) {
    //     let authUser = JSON.parse(accessUser);
    //     setUser(JSON.parse(accessUser));
    //     getAccessToken();

    //     // !authUser.is_superuser
    //     //     ? router.push({ pathname: "/register/login" })
    //     //     : null;
    //     } else {
    //     // router.push({ pathname: "/register/login" })
    //     setTimeout(() => setLoading(false), 999);
    //     }
    // }, [])